![euclid logo](data/images/euclid.png) euclid
=============================================

euclid is a C++ template library for interfacing with computational geometry library. The main goal is to interface with specialized libraries for Geographic Information Software (GIS) software. euclid currently interfaces with [geos](https://libgeos.org/), [GDAL](https://gdal.org/) and [boost.geometry](https://www.boost.org/).

euclid’s goal is not to provide an implementation of a computational geometry library. Instead, Euclid defines interfaces to switch easily between libraries with minimal change to the code. The idea came from a project we had to convert from GDAL to GEOS due for performance reasons. The goal of euclid was to make another potential transition easier.

euclid also includes `extra` algorithms, which are not readilly available in other libraries.

Interface status
----------------

Primary interfaces:
* GEOS: it is the most complete interface
* Boost.Geometry: in progress, the implementation support the geometry features, and only a limited set of functions

Secondary interfaces:
* GDAL: this interface is available mainly for converting to/from GDAL geometry, it should not be used as the primary interface.
* PROJ: this interface is used for transforming geometries between SRID
* eigen3: this interface is used to convert simple geometries to eigen3 vectors
* fmt: this interface is used to display geometries with libfmt

Reference
---------

The line_string_fitting implements the `simplifyBorders` algorithm presented in [[1]][ref1], which is currently submitted to the [Applied Soft Computing](https://www.sciencedirect.com/journal/applied-soft-computing/) journal.

When citing, use the following references:

[[1]][ref1] M. Wzorek, C. Berger, P. Doherty, Polygon Area Decomposition using a Compactness Metric. arXiv preprint arXiv:2110.04043 (2021).

[ref1]: <https://arxiv.org/abs/2110.04043> "[1]"
