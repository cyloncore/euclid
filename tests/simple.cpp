#include "config.h"

#include <euclid/simple>

#include "catch.hpp"

TEST_CASE("test simple segment intersection", "[simple][segment][intersection]")
{
  euclid::simple::segment seg1({268, -6}, {254, 18.5});
  euclid::simple::segment seg2({180, -5}, {294, -3});

  euclid::simple::point ip1, ip2;
  CHECK(euclid::simple::intersection(seg1, seg2, &ip2));
  CHECK(euclid::simple::intersection(seg2, seg1, &ip1));
  CHECK(ip1 == ip2);
  CHECK(ip1 == euclid::simple::point(266.56079404466498772, -3.481389578163771592));

  euclid::simple::segment seg3({-92, -246}, {-92, -240});
  euclid::simple::segment seg4({-90, -243}, {-129, -251});
  CHECK(euclid::simple::intersection(seg3, seg4, &ip2));
  CHECK(euclid::simple::intersection(seg4, seg3, &ip1));
  CHECK(ip1 == ip2);
  CHECK(ip1 == euclid::simple::point(-92, -243.4102564102564088));

  euclid::simple::segment seg5({-91.654320739660391837, -245.76495662845064771},
                               {-91.654320739660391837, -240.44432525248251409});
  euclid::simple::segment seg6({-90.214704990328414169, -242.88551402309599325},
                               {-128.89874037143732721, -251.08558800441880976});
  CHECK(euclid::simple::intersection(seg5, seg6, &ip2));
  CHECK(euclid::simple::intersection(seg6, seg5, &ip1));
  CHECK(ip1 == ip2);
  CHECK(ip1 == euclid::simple::point(-91.65432073966044868, -243.19067752356590972));

  euclid::simple::segment seg7({-245.76495662845064771, -91.654320739660391837},
                               {-240.44432525248251409, -91.654320739660391837});
  euclid::simple::segment seg8({-242.88551402309599325, -90.214704990328414169},
                               {-251.08558800441880976, -128.89874037143732721});
  CHECK(euclid::simple::intersection(seg7, seg8, &ip2));
  CHECK(euclid::simple::intersection(seg8, seg7, &ip1));
  CHECK(ip1 == ip2);
  CHECK(ip1 == euclid::simple::point(-243.19067752356590972, -91.65432073966044868));

  euclid::simple::segment seg9({15.465225268856324, 58.432944497143843},
                               {15.473934142393354, 58.436734996704395});
  euclid::simple::segment seg10({15.474234330112184, 58.436545291967164},
                                {15.474403287745517, 58.433222517615995});
  CHECK(not euclid::simple::intersection(seg9, seg10, &ip2, 0.001));
  CHECK(not euclid::simple::intersection(seg10, seg9, &ip1, 0.001));
}

TEST_CASE("test simple segment overlaps", "[simple][segment][overlaps]")
{
  CHECK(not euclid::simple::overlaps({{0, 0}, {10, 10}}, {{3, 2}, {5, 4}}));
  CHECK(not euclid::simple::overlaps({{0, 0}, {10, 10}}, {{20, 20}, {30, 30}}));
  CHECK(not euclid::simple::overlaps({{0, 0}, {10, 10}}, {{10, 10}, {30, 30}}));
  CHECK(euclid::simple::overlaps({{0, 0}, {10, 10}}, {{3, 3}, {5, 5}}));
  CHECK(euclid::simple::overlaps({{0, 0}, {10, 10}}, {{20, 20}, {5, 5}}));
  CHECK(euclid::simple::overlaps({{0, 0}, {0, 10}}, {{0, 3}, {0, 5}}));
  CHECK(not euclid::simple::overlaps({{0, 0}, {0, 10}}, {{1, 3}, {1, 5}}));
  CHECK(not euclid::simple::overlaps({{532486.97, 6464814.41}, {532351.18, 6464895.78}},
                                     {{532238.06, 6464670.53}, {532351.18, 6464895.78}}));

  CHECK(euclid::simple::overlaps({{538601.488464741618372, 6469872.366358293220401},
                                  {538909.993753024609759, 6470057.433069659397006}},
                                 {{538909.993008211022243, 6470057.432622807100415},
                                  {538812.999483049265109, 6469999.243065892718732}}));
  CHECK(euclid::simple::overlaps({{538909.993753024609759, 6470057.433069659397006},
                                  {539063.141735346056521, 6469848.778018999844790}},
                                 {{538909.993008211022243, 6470057.432622807100415},
                                  {538959.887748752022162, 6469989.455457101576030}}));

  // adj_tolerance
  CHECK(euclid::simple::overlaps({{532351.18, 6464895.78}, {532110.14, 6464415.79}},
                                 {{532238.06, 6464670.53}, {532351.18, 6464895.78}}));
  CHECK(euclid::simple::overlaps({{537042.74, 6479091.97}, {536367.17, 6479012.67}},
                                 {{536872.89, 6479072.04}, {536901.71, 6479075.42}}));
}

TEST_CASE("test simple point", "[simple][point]")
{
  CHECK(euclid::simple::point().is_valid());
  CHECK(not euclid::simple::point::invalid().is_valid());
}
