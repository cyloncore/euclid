#include "systems.h"

#include <euclid/fmt>
#include <euclid/geometry>

#include "catch.hpp"
#include "geometries.h"

TEMPLATE_TEST_CASE("test fmt::format", "[fmt][format]", euclid::geos)
{
#include "common_using.h"
  CHECK(fmt::format("{}", geometries::line_string_1())
        == "LINESTRING (0.00 1.00, 2.00 3.00, 4.00 5.00)");
  CHECK(fmt::format("{:#3}", geometries::line_string_1())
        == "LINESTRING Z (0.00 1.00 2.00, 2.00 3.00 4.00, 4.00 5.00 6.00)");
  CHECK(fmt::format("{:.4}", geometries::line_string_1())
        == "LINESTRING (0.0000 1.0000, 2.0000 3.0000, 4.0000 5.0000)");
  CHECK(fmt::format("{:.4#3}", geometries::line_string_1())
        == "LINESTRING Z (0.0000 1.0000 2.0000, 2.0000 3.0000 4.0000, 4.0000 5.0000 6.0000)");
  CHECK(fmt::format("{:.12}", geometries::line_string_1())
        == "LINESTRING (0.000000000000 1.000000000000, 2.000000000000 3.000000000000, "
           "4.000000000000 5.000000000000)");
}
