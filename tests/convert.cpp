#include "systems.h"
#include <euclid/convert>

#include <euclid/io>

#include "catch.hpp"
#include "geometries.h"

#define CHECK_CONVERSION_GEOMETRIES(_NAME_)                                                        \
  CHECK(geometries_destination::_NAME_().dimensions()                                              \
        == euclid::convert<destination_system>(geometries_source::_NAME_()).dimensions());         \
  CHECK(euclid::io::to_wkt(geometries_destination::_NAME_())                                       \
        == euclid::io::to_wkt(euclid::convert<destination_system>(geometries_source::_NAME_())))

template<typename source_system, typename destination_system>
void test_conversion()
{
  using geometries_source = euclid_tests::geometries<source_system>;
  using geometries_destination = euclid_tests::geometries<destination_system>;

  // point
  CHECK(typename destination_system::point(1, 2, 3)
        == euclid::convert<destination_system>(typename source_system::point(1, 2, 3)));
  CHECK(euclid::convert<destination_system>(typename source_system::point(1, 2, 3)).dimensions()
        == 3);
  CHECK(typename destination_system::point(1, 2)
        == euclid::convert<destination_system>(typename source_system::point(1, 2)));
  CHECK(euclid::convert<destination_system>(typename source_system::point(1, 2)).dimensions() == 2);

  // line string
  CHECK_CONVERSION_GEOMETRIES(line_string_1);

  // linear ring
  CHECK_CONVERSION_GEOMETRIES(linear_ring_valid_1);
  CHECK_CONVERSION_GEOMETRIES(linear_ring_valid_2);
  CHECK_CONVERSION_GEOMETRIES(linear_ring_valid_3);

  // polygon
  CHECK_CONVERSION_GEOMETRIES(polygon_1);
  CHECK_CONVERSION_GEOMETRIES(polygon_2);
  CHECK_CONVERSION_GEOMETRIES(polygon_3);

  // collections
  CHECK_CONVERSION_GEOMETRIES(multi_point_1);
  CHECK_CONVERSION_GEOMETRIES(multi_line_string_1);
  CHECK_CONVERSION_GEOMETRIES(multi_polygon_1);
  //   CHECK_CONVERSION_GEOMETRIES(collection_1);
}

TEMPLATE_TEST_CASE("test conversion", "[conversion]" EUCLID_TEST_SYSTEMS)
{
#ifdef HAVE_GEOS
  SECTION("GEOS") { test_conversion<TestType, euclid::geos>(); }
#endif
#ifdef HAVE_BOOST
  SECTION("BOOST") { test_conversion<TestType, euclid::boost>(); }
#endif
#ifdef HAVE_GDAL
  SECTION("GDAL") { test_conversion<TestType, euclid::gdal>(); }
#endif
}
