#include "systems.h"

#include "catch.hpp"
#include "geometries.h"

TEMPLATE_TEST_CASE("test multi_point", "[multi_point]" EUCLID_TEST_SYSTEMS)
{
#include "common_using.h"

  multi_point_type mp1 = geometries::multi_point_1();
  CHECK(euclid::io::to_wkt(mp1) == wkt<system>::mp1);
  CHECK(mp1.dimensions() == 2);

  typename multi_point_type::iterator it = mp1.begin();
  CHECK(it == mp1.begin());
  CHECK(it != mp1.end());
  euclid::simple::point point = *it;
  CHECK(point.x() == 0.0);
  CHECK(point.y() == 1.0);
  CHECK(std::isnan(point.z()));
  ++it;
  point = *it;
  CHECK(point.x() == 1.0);
  CHECK(point.y() == 2.0);
  CHECK(std::isnan(point.z()));
  ++it;
  point = *it;
  CHECK(point.x() == 2.0);
  CHECK(point.y() == 3.0);
  CHECK(std::isnan(point.z()));
  ++it;
  CHECK(it == mp1.end());

  multi_geometry_type cop1 = geometries::collection_only_poiny_1();
  CHECK(euclid::io::to_wkt(cop1) == wkt<system>::cop1);
  // multi_point_type mp2 = euclid::geometry::cast<multi_point_type>(cop1);
  // REQUIRE_FALSE ( mp2.is_null() );
  // CHECK( euclid::io::to_wkt(mp2) == wkt<system>::cop1_mp );
  multi_point_type mp2_b = euclid::geometry::cast<multi_point_type>(geometry_type(cop1));
  CHECK(euclid::io::to_wkt(mp2_b) == wkt<system>::cop1_mp);
}
