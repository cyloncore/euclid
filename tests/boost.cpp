#include <euclid/boost>
#include <euclid/io>
#include <euclid/ops>

#include "catch.hpp"

TEST_CASE("test polygon with boost interface", "boost_polygon")
{
  euclid::boost::polygon::builder builder;
  builder.shell().add_point(0.0, 1.0, 2.0).add_point(2.0, 3.0, 4.0).add_point(4.0, 5.0, 6.0);
  euclid::boost::polygon polygon1 = builder.create();
  euclid::boost::geometry geometry1 = polygon1;

  CHECK(polygon1.interior_rings().size() == 0);
  CHECK(euclid::io::to_wkt(polygon1, {2, 3}) == "POLYGON((0 1 2,2 3 4,4 5 6,0 1 2))");

  builder.add_point(6.0, 7.0, 8.0);
  euclid::boost::polygon polygon2 = builder.create();
  geometry1 = polygon2;

  CHECK(polygon2.interior_rings().size() == 0);
  CHECK(euclid::io::to_wkt(polygon2) == "POLYGON((0 1 2,2 3 4,4 5 6,6 7 8,0 1 2))");

  euclid::boost::polygon polygon3
    = euclid::io::from_wkt<euclid::boost::polygon>("POLYGON ((0 1 2, 2 3 4, 4 5 6, 6 7 8, 0 1 2))");
  CHECK(euclid::io::to_wkt(polygon3) == "POLYGON((0 1 2,2 3 4,4 5 6,6 7 8,0 1 2))");

  // Create polygon for testing ops
  euclid::boost::polygon::builder builder4;
  builder4.shell().add_point(0.0, 1.0).add_point(1.0, 1.0).add_point(1.0, 0.0).add_point(0.0, 0.0);
  euclid::boost::polygon polygon4 = builder4.create();

  euclid::boost::polygon::builder builder5;
  builder5.shell().add_point(0.5, 1.5).add_point(1.5, 1.5).add_point(1.5, 0.5).add_point(0.5, 0.5);
  euclid::boost::polygon polygon5 = builder5.create();

  // Test area
  //   CHECK( euclid::ops::area(polygon4) == 1.0 );

  // Test intersection
  //   euclid::boost::geometry intersection_45 = euclid::ops::intersection(polygon4, polygon5);
  //   CHECK( euclid::io::to_wkt(intersection_45, {2, 2}) == "POLYGON ((0.50 1.00, 1.00 1.00, 1.00
  //   0.50, 0.50 0.50, 0.50 1.00))");
}
