#include "systems.h"

#include <euclid/format>
#include <euclid/geometry>

#include "catch.hpp"
#include "geometries.h"

TEMPLATE_TEST_CASE("test fmt::format", "[fmt][format]", euclid::geos)
{
#include "common_using.h"

#if GEOS_VERSION_MAJOR > 3 || (GEOS_VERSION_MAJOR == 3 && GEOS_VERSION_MINOR >= 12)
  CHECK(std::format("{}", geometries::line_string_1()) == "LINESTRING (0 1, 2 3, 4 5)");
  CHECK(std::format("{:#3}", geometries::line_string_1()) == "LINESTRING Z (0 1 2, 2 3 4, 4 5 6)");
  CHECK(std::format("{:.4}", geometries::line_string_1()) == "LINESTRING (0 1, 2 3, 4 5)");
  CHECK(std::format("{:.4#3}", geometries::line_string_1())
        == "LINESTRING Z (0 1 2, 2 3 4, 4 5 6)");
  CHECK(std::format("{:.12}", geometries::line_string_1()) == "LINESTRING (0 1, 2 3, 4 5)");
#else
  CHECK(std::format("{}", geometries::line_string_1())
        == "LINESTRING (0.00 1.00, 2.00 3.00, 4.00 5.00)");
  CHECK(std::format("{:#3}", geometries::line_string_1())
        == "LINESTRING Z (0.00 1.00 2.00, 2.00 3.00 4.00, 4.00 5.00 6.00)");
  CHECK(std::format("{:.4}", geometries::line_string_1())
        == "LINESTRING (0.0000 1.0000, 2.0000 3.0000, 4.0000 5.0000)");
  CHECK(std::format("{:.4#3}", geometries::line_string_1())
        == "LINESTRING Z (0.0000 1.0000 2.0000, 2.0000 3.0000 4.0000, 4.0000 5.0000 6.0000)");
  CHECK(std::format("{:.12}", geometries::line_string_1())
        == "LINESTRING (0.000000000000 1.000000000000, 2.000000000000 3.000000000000, "
           "4.000000000000 5.000000000000)");
#endif

  CHECK(std::format("{}", geometries::line_string_2())
        == "LINESTRING (0.01 1.01, 2.01 3.01, 4.01 5.01)");
  CHECK(std::format("{:#3}", geometries::line_string_2())
        == "LINESTRING Z (0.01 1.01 2.01, 2.01 3.01 4.01, 4.01 5.01 6.01)");
  CHECK(std::format("{:.4}", geometries::line_string_2())
        == "LINESTRING (0.0123 1.0123, 2.0123 3.0123, 4.0123 5.0123)");
  CHECK(std::format("{:.4#3}", geometries::line_string_2())
        == "LINESTRING Z (0.0123 1.0123 2.0123, 2.0123 3.0123 4.0123, 4.0123 5.0123 6.0123)");
  CHECK(std::format("{:.12}", geometries::line_string_2())
        == "LINESTRING (0.012345678901 1.012345678901, 2.012345678901 3.012345678901, "
           "4.012345678901 5.012345678901)");
}
