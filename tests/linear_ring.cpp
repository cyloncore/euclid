#include "systems.h"

#include "catch.hpp"
#include "geometries.h"

TEMPLATE_TEST_CASE("test linear_ring", "[linear_ring]" EUCLID_TEST_SYSTEMS)
{
#include "common_using.h"

  linear_ring_type ring1 = linear_ring_builder()
                             .add_point(0.0, 1.0, 2.0)
                             .add_point(2.0, 3.0, 4.0)
                             .add_point(4.0, 5.0, 6.0)
                             .create();
  CHECK(euclid::io::to_wkt(ring1, {2, 3}) == wkt<system>::ring1);
  CHECK(ring1.dimensions() == 3);

  // point_iterator
  euclid::geometry::point_iterator<linear_ring_type> it = ring1.points().begin();
  CHECK(it == ring1.points().begin());
  CHECK(it != ring1.points().end());
  euclid::simple::point point = *it;
  CHECK(point.x() == 0.0);
  CHECK(point.y() == 1.0);
  CHECK(point.z() == 2.0);
  CHECK(it - ring1.points().begin() == 0);
  CHECK(ring1.points().begin() - it == 0);
  ++it;
  point = *it;
  CHECK(point.x() == 2.0);
  CHECK(point.y() == 3.0);
  CHECK(point.z() == 4.0);
  CHECK(it - ring1.points().begin() == 1);
  CHECK(ring1.points().begin() - it == -1);
  ++it;
  point = *it;
  CHECK(point.x() == 4.0);
  CHECK(point.y() == 5.0);
  CHECK(point.z() == 6.0);
  CHECK(it - ring1.points().begin() == 2);
  CHECK(ring1.points().begin() - it == -2);
  ++it;
  point = *it;
  CHECK(point.x() == 0.0);
  CHECK(point.y() == 1.0);
  CHECK(point.z() == 2.0);
  CHECK(it - ring1.points().begin() == 3);
  CHECK(ring1.points().begin() - it == -3);
  ++it;
  CHECK(it == ring1.points().end());
  CHECK(it - ring1.points().begin() == 4);
  CHECK(ring1.points().begin() - it == -4);
  --it;
  point = *it;
  CHECK(point.x() == 0.0);
  CHECK(point.y() == 1.0);
  CHECK(point.z() == 2.0);

  // segment_iterator
  euclid::geometry::segment_iterator<linear_ring_type> its = ring1.segments().begin();

  CHECK(its == ring1.segments().begin());
  CHECK(its != ring1.segments().end());

  euclid::simple::segment seg = *its;
  CHECK(seg.first().x() == 0.0);
  CHECK(seg.first().y() == 1.0);
  CHECK(seg.first().z() == 2.0);
  CHECK(seg.second().x() == 2.0);
  CHECK(seg.second().y() == 3.0);
  CHECK(seg.second().z() == 4.0);
  CHECK(its - ring1.segments().begin() == 0);
  CHECK(ring1.segments().begin() - its == 0);
  ++its;
  seg = *its;
  CHECK(seg.first().x() == 2.0);
  CHECK(seg.first().y() == 3.0);
  CHECK(seg.first().z() == 4.0);
  CHECK(seg.second().x() == 4.0);
  CHECK(seg.second().y() == 5.0);
  CHECK(seg.second().z() == 6.0);
  CHECK(its - ring1.segments().begin() == 1);
  CHECK(ring1.segments().begin() - its == -1);
  ++its;
  seg = *its;
  CHECK(seg.first().x() == 4.0);
  CHECK(seg.first().y() == 5.0);
  CHECK(seg.first().z() == 6.0);
  CHECK(seg.second().x() == 0.0);
  CHECK(seg.second().y() == 1.0);
  CHECK(seg.second().z() == 2.0);
  CHECK(its - ring1.segments().begin() == 2);
  CHECK(ring1.segments().begin() - its == -2);
  ++its;
  CHECK(its != ring1.segments().begin());
  CHECK(its == ring1.segments().end());

  std::vector<euclid::simple::segment> ring1_segments = ring1.segments();
  CHECK(ring1_segments.size() == 3);

  euclid::geometry::segment_circular_iterator<system> sci(ring1);
  euclid::geometry::segment_circular_iterator<system> sci_start = sci;
  --sci;
  seg = *sci;
  CHECK(sci != sci_start);
  CHECK(sci.full_circles() == -1);
  CHECK(seg.first().x() == 4.0);
  CHECK(seg.first().y() == 5.0);
  CHECK(seg.first().z() == 6.0);
  CHECK(seg.second().x() == 0.0);
  CHECK(seg.second().y() == 1.0);
  CHECK(seg.second().z() == 2.0);
  CHECK(sci - sci_start == -1);
  CHECK(sci_start - sci == 1);
  ++sci;
  seg = *sci;
  CHECK(sci == sci_start);
  CHECK(sci.full_circles() == 0);
  CHECK(seg.first().x() == 0.0);
  CHECK(seg.first().y() == 1.0);
  CHECK(seg.first().z() == 2.0);
  CHECK(seg.second().x() == 2.0);
  CHECK(seg.second().y() == 3.0);
  CHECK(seg.second().z() == 4.0);
  CHECK(sci - sci_start == 0);
  CHECK(sci_start - sci == 0);
  ++sci;
  seg = *sci;
  CHECK(sci != sci_start);
  CHECK(sci.full_circles() == 0);
  CHECK(seg.first().x() == 2.0);
  CHECK(seg.first().y() == 3.0);
  CHECK(seg.first().z() == 4.0);
  CHECK(seg.second().x() == 4.0);
  CHECK(seg.second().y() == 5.0);
  CHECK(seg.second().z() == 6.0);
  CHECK(sci - sci_start == 1);
  CHECK(sci_start - sci == -1);
  ++sci;
  seg = *sci;
  CHECK(sci != sci_start);
  CHECK(sci.full_circles() == 0);
  CHECK(seg.first().x() == 4.0);
  CHECK(seg.first().y() == 5.0);
  CHECK(seg.first().z() == 6.0);
  CHECK(seg.second().x() == 0.0);
  CHECK(seg.second().y() == 1.0);
  CHECK(seg.second().z() == 2.0);
  CHECK(sci - sci_start == 2);
  CHECK(sci_start - sci == -2);
  ++sci;
  seg = *sci;
  CHECK(sci == sci_start);
  CHECK(sci.full_circles() == 1);
  CHECK(seg.first().x() == 0.0);
  CHECK(seg.first().y() == 1.0);
  CHECK(seg.first().z() == 2.0);
  CHECK(seg.second().x() == 2.0);
  CHECK(seg.second().y() == 3.0);
  CHECK(seg.second().z() == 4.0);
  CHECK(sci - sci_start == 3);
  CHECK(sci_start - sci == -3);

  for(int i = 4; i < 20; ++i)
  {
    ++sci;
    CHECK(sci - sci_start == i);
    CHECK(sci_start - sci == -i);
  }

  euclid::geometry::point_circular_iterator<system> pci(ring1);
  euclid::geometry::point_circular_iterator<system> pci_start = pci;

  --pci;
  point = *pci;
  CHECK(pci == std::prev(pci_start, 1));
  CHECK(pci != pci_start);
  CHECK(pci.full_circles() == -1);
  CHECK(point.x() == 4.0);
  CHECK(point.y() == 5.0);
  CHECK(point.z() == 6.0);
  CHECK(pci - pci_start == -1);
  CHECK(pci_start - pci == 1);
  ++pci;
  point = *pci;
  CHECK(pci == pci_start);
  CHECK(pci.full_circles() == 0);
  CHECK(point.x() == 0.0);
  CHECK(point.y() == 1.0);
  CHECK(point.z() == 2.0);
  CHECK(pci - pci_start == 0);
  CHECK(pci_start - pci == 0);
  ++pci;
  point = *pci;
  CHECK(pci != pci_start);
  CHECK(pci.full_circles() == 0);
  CHECK(point.x() == 2.0);
  CHECK(point.y() == 3.0);
  CHECK(point.z() == 4.0);
  CHECK(pci - pci_start == 1);
  CHECK(pci_start - pci == -1);
  ++pci;
  point = *pci;
  CHECK(pci != pci_start);
  CHECK(pci.full_circles() == 0);
  CHECK(point.x() == 4.0);
  CHECK(point.y() == 5.0);
  CHECK(point.z() == 6.0);
  CHECK(pci - pci_start == 2);
  CHECK(pci_start - pci == -2);
  ++pci;
  point = *pci;
  CHECK(pci == pci_start);
  CHECK(pci.full_circles() == 1);
  CHECK(point.x() == 0.0);
  CHECK(point.y() == 1.0);
  CHECK(point.z() == 2.0);
  CHECK(pci - pci_start == 3);
  CHECK(pci_start - pci == -3);

  for(int i = 4; i < 20; ++i)
  {
    ++pci;
    CHECK(pci - pci_start == i);
    CHECK(pci_start - pci == -i);
  }

  CHECK(geometries::linear_ring_valid_1().dimensions() == 2);
}
