#include "systems.h"

#include "catch.hpp"
#include "geometries.h"

TEMPLATE_TEST_CASE("test point", "[point]" EUCLID_TEST_SYSTEMS)
{
#include "common_using.h"
  point_type point1(1.0, 2.0);
  CHECK(point1.x() == 1.0);
  CHECK(point1.y() == 2.0);
  CHECK(std::isnan(point1.z()));
  CHECK(point1.dimensions() == 2);
  point_type point1a(1.0, 2.0);
  CHECK(point1a.x() == 1.0);
  CHECK(point1a.y() == 2.0);
  CHECK(std::isnan(point1a.z()));
  CHECK(point1a.dimensions() == 2);
  point_type point2(1.0, 2.0, 3.0);
  CHECK(point2.x() == 1.0);
  CHECK(point2.y() == 2.0);
  CHECK(point2.z() == 3.0);
  CHECK(point2.dimensions() == 3);
  point_type point3(1.0, 2.0, 3.0);
  CHECK(point3.x() == 1.0);
  CHECK(point3.y() == 2.0);
  CHECK(point3.z() == 3.0);
  CHECK(point3.dimensions() == 3);
  CHECK(point1 == point1a);
  CHECK(point1 != point2);
  CHECK(point3 == point2);
  CHECK(point3 == point_type(point3));
}
