#include "config.h"

#include <euclid/geos>

#include <euclid/ops>

#include "catch.hpp"
#include "geometries.h"

TEMPLATE_TEST_CASE("test is_valid", "[ops][is_valid]", euclid::geos)
{
#include "common_using.h"
  CHECK(euclid::ops::is_valid(geometries::linear_ring_valid_1()));
  CHECK(not euclid::ops::is_valid(geometries::linear_ring_invalid_1()));
}

TEMPLATE_TEST_CASE("test area", "[ops][is_valid]", euclid::geos)
{
#include "common_using.h"
  CHECK(euclid::ops::area(geometries::linear_ring_valid_1()) == 0.0);
  CHECK(euclid::ops::area(geometries::polygon_1()) == 1.0);
}
