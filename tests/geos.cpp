#include "config.h"

#include <euclid/geos>
#include <euclid/io>
#include <euclid/ops>

#include "catch.hpp"
#include "geometries.h"

TEST_CASE("test polygon with geos interface", "geos_polygon")
{
  using geometries = euclid_tests::geometries<euclid::geos>;

  euclid::geos::polygon::builder builder;
  builder.shell().add_point(0.0, 1.0, 2.0).add_point(2.0, 3.0, 4.0).add_point(4.0, 5.0, 6.0);
  euclid::geos::polygon polygon1 = builder.create();
  euclid::geos::geometry geometry1 = polygon1;

  CHECK(geometry1.is<euclid::geos::polygon>());

  CHECK(polygon1.interior_rings().size() == 0);
#if GEOS_VERSION_MAJOR > 3 || (GEOS_VERSION_MAJOR == 3 && GEOS_VERSION_MINOR >= 12)
  CHECK(euclid::io::to_wkt(polygon1) == "POLYGON ((0 1, 2 3, 4 5, 0 1))");
  CHECK(euclid::io::to_wkt(polygon1, {2, 3}) == "POLYGON Z ((0 1 2, 2 3 4, 4 5 6, 0 1 2))");

  CHECK(euclid::io::to_wkt(geometry1.cast<euclid::geos::polygon>().exterior_ring())
        == "LINEARRING (0 1, 2 3, 4 5, 0 1)");
#else
  CHECK(euclid::io::to_wkt(polygon1) == "POLYGON ((0.00 1.00, 2.00 3.00, 4.00 5.00, 0.00 1.00))");
  CHECK(euclid::io::to_wkt(polygon1, {2, 3})
        == "POLYGON Z ((0.00 1.00 2.00, 2.00 3.00 4.00, 4.00 5.00 6.00, 0.00 1.00 2.00))");

  CHECK(euclid::io::to_wkt(geometry1.cast<euclid::geos::polygon>().exterior_ring())
        == "LINEARRING (0.00 1.00, 2.00 3.00, 4.00 5.00, 0.00 1.00)");
#endif

  builder.add_point(6.0, 7.0, 8.0);
  euclid::geos::polygon polygon2 = builder.create();
  geometry1 = polygon2;

  CHECK(polygon2.interior_rings().size() == 0);
#if GEOS_VERSION_MAJOR > 3 || (GEOS_VERSION_MAJOR == 3 && GEOS_VERSION_MINOR >= 12)
  CHECK(euclid::io::to_wkt(polygon2) == "POLYGON ((0 1, 2 3, 4 5, 6 7, 0 1))");
#else
  CHECK(euclid::io::to_wkt(polygon2)
        == "POLYGON ((0.00 1.00, 2.00 3.00, 4.00 5.00, 6.00 7.00, 0.00 1.00))");
#endif

  euclid::geos::polygon polygon3 = euclid::io::from_wkt<euclid::geos::polygon>(
    "POLYGON ((0.00 1.00, 2.00 3.00, 4.00 5.00, 6.00 7.00, 0.00 1.00))");
#if GEOS_VERSION_MAJOR > 3 || (GEOS_VERSION_MAJOR == 3 && GEOS_VERSION_MINOR >= 12)
  CHECK(euclid::io::to_wkt(polygon3) == "POLYGON ((0 1, 2 3, 4 5, 6 7, 0 1))");
#else
  CHECK(euclid::io::to_wkt(polygon3)
        == "POLYGON ((0.00 1.00, 2.00 3.00, 4.00 5.00, 6.00 7.00, 0.00 1.00))");
#endif

  // Create polygon for testing ops
  euclid::geos::polygon polygon4 = geometries::polygon_1();
  euclid::geos::polygon polygon5 = geometries::polygon_2();

  // Test area
  CHECK(euclid::ops::area(polygon4) == 1.0);

  // Test intersection
  euclid::geos::geometry intersection_45 = euclid::ops::intersection(polygon4, polygon5);
#if GEOS_VERSION_MAJOR > 3 || (GEOS_VERSION_MAJOR == 3 && GEOS_VERSION_MINOR >= 12)
  CHECK(euclid::io::to_wkt(intersection_45, {2, 2})
        == "POLYGON ((1 1, 1 0.5, 0.5 0.5, 0.5 1, 1 1))");
#elif GEOS_VERSION_MAJOR > 3 || (GEOS_VERSION_MAJOR == 3 && GEOS_VERSION_MINOR >= 10)
  CHECK(euclid::io::to_wkt(intersection_45, {2, 2})
        == "POLYGON ((1.00 1.00, 1.00 0.50, 0.50 0.50, 0.50 1.00, 1.00 1.00))");
#elif GEOS_VERSION_MINOR >= 9
  CHECK(euclid::io::to_wkt(intersection_45, {2, 2})
        == "POLYGON ((0.50 0.50, 0.50 1.00, 1.00 1.00, 1.00 0.50, 0.50 0.50))");
#else
  CHECK(euclid::io::to_wkt(intersection_45, {2, 2})
        == "POLYGON ((0.50 1.00, 1.00 1.00, 1.00 0.50, 0.50 0.50, 0.50 1.00))");
#endif

  // Test envelope
  euclid::simple::box polygon4_envelope = euclid::ops::envelope(polygon4);
  CHECK(polygon4_envelope.top_left() == euclid::simple::point(0.0, 0.0));
  CHECK(polygon4_envelope.bottom_right() == euclid::simple::point(1.0, 1.0));
}

#include <euclid/extra/longest_segment>

TEST_CASE("test geos interface and extra", "geos_extra")
{
  // Create polygon for testing extra
  euclid::geos::polygon polygon4 = euclid::geos::polygon::builder()
                                     .add_point(0.0, 1.0)
                                     .add_point(1.0, 1.0)
                                     .add_point(1.0, 0.0)
                                     .add_point(0.0, 0.0)
                                     .create();
  euclid::geos::polygon polygon5 = euclid::geos::polygon::builder()
                                     .add_point(0.0, 2.0)
                                     .add_point(1.0, 1.0)
                                     .add_point(1.0, 0.0)
                                     .add_point(0.0, 0.0)
                                     .create();
  // Test longest_segment
  euclid::simple::segment longest = euclid::extra::longest_segment(polygon5.exterior_ring());
  CHECK(longest.first() == euclid::simple::point(0.0, 0.0));
  CHECK(longest.second() == euclid::simple::point(0.0, 2.0));
  longest = euclid::extra::longest_segment(polygon4.exterior_ring());
  CHECK(longest.first() == euclid::simple::point(0.0, 1.0));
  CHECK(longest.second() == euclid::simple::point(1.0, 1.0));
}
