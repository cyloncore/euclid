#include "config.h"

#include "catch.hpp"

#include <euclid/proj>
#include <euclid/simple>

TEST_CASE("test proj interface")
{
  euclid::simple::point pt(58.4949, 15.1017);
  euclid::simple::point r = euclid::proj::transform(pt, "EPSG:4326", "EPSG:32633");
  CHECK(r.x() == Approx(505928.239751));
  CHECK(r.y() == Approx(6483815.54676));
  CHECK(std::isnan(r.z()));
}

TEST_CASE("test proj interface swap")
{
  euclid::simple::point pt(15.1017, 58.4949);
  euclid::simple::point r = euclid::proj::transform(pt, "EPSG:4326", "EPSG:32633", true);
  CHECK(r.x() == Approx(505928.239751));
  CHECK(r.y() == Approx(6483815.54676));
  CHECK(std::isnan(r.z()));
}
