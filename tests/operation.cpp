#include "config.h"

#include "catch.hpp"

#include <euclid/operation>

TEST_CASE("euclid::operation::enable_for_all", "[operation]")
{
  CHECK(euclid::operation::enable_for_all::point);
  CHECK(euclid::operation::enable_for_all::line_string);
  CHECK(euclid::operation::enable_for_all::linear_ring);
  CHECK(euclid::operation::enable_for_all::polygon);
  CHECK(euclid::operation::enable_for_all::multi_geometry);
  CHECK(euclid::operation::enable_for_all::multi_point);
  CHECK(euclid::operation::enable_for_all::multi_line_string);
  CHECK(euclid::operation::enable_for_all::multi_polygon);
}

TEST_CASE("euclid::operation::details::contains_v", "[operation]")
{
  CHECK(euclid::operation::details::contains_v<euclid::geometry::point, euclid::geometry::point>);
  CHECK(not euclid::operation::details::contains_v<euclid::geometry::line_string,
                                                   euclid::geometry::point>);
  CHECK(not euclid::operation::details::contains_v<euclid::geometry::linear_ring,
                                                   euclid::geometry::point>);
  CHECK(
    not euclid::operation::details::contains_v<euclid::geometry::polygon, euclid::geometry::point>);

  CHECK(euclid::operation::details::contains_v<euclid::geometry::point, euclid::geometry::point,
                                               euclid::geometry::line_string>);
  CHECK(
    euclid::operation::details::contains_v<euclid::geometry::line_string, euclid::geometry::point,
                                           euclid::geometry::line_string>);
  CHECK(not euclid::operation::details::contains_v<
        euclid::geometry::linear_ring, euclid::geometry::point, euclid::geometry::line_string>);
  CHECK(
    not euclid::operation::details::contains_v<euclid::geometry::polygon, euclid::geometry::point,
                                               euclid::geometry::line_string>);

  CHECK(euclid::operation::details::contains_v<euclid::geometry::point, euclid::geometry::point,
                                               euclid::geometry::line_string,
                                               euclid::geometry::linear_ring>);
  CHECK(
    euclid::operation::details::contains_v<euclid::geometry::line_string, euclid::geometry::point,
                                           euclid::geometry::line_string,
                                           euclid::geometry::linear_ring>);
  CHECK(
    euclid::operation::details::contains_v<euclid::geometry::linear_ring, euclid::geometry::point,
                                           euclid::geometry::line_string,
                                           euclid::geometry::linear_ring>);
  CHECK(
    not euclid::operation::details::contains_v<euclid::geometry::polygon, euclid::geometry::point,
                                               euclid::geometry::line_string,
                                               euclid::geometry::linear_ring>);
}

template<template<typename> class TestType>
void test_enable_for_one()
{
  CHECK(euclid::operation::enable_for<TestType>::point
        == euclid::operation::details::is_same_v<TestType, euclid::geometry::point>);
  CHECK(euclid::operation::enable_for<TestType>::line_string
        == euclid::operation::details::is_same_v<TestType, euclid::geometry::line_string>);
  CHECK(euclid::operation::enable_for<TestType>::linear_ring
        == euclid::operation::details::is_same_v<TestType, euclid::geometry::linear_ring>);
  CHECK(euclid::operation::enable_for<TestType>::polygon
        == euclid::operation::details::is_same_v<TestType, euclid::geometry::polygon>);
  CHECK(euclid::operation::enable_for<TestType>::multi_geometry
        == euclid::operation::details::is_same_v<TestType, euclid::geometry::multi_geometry>);
  CHECK(euclid::operation::enable_for<TestType>::multi_point
        == euclid::operation::details::is_same_v<TestType, euclid::geometry::multi_point>);
  CHECK(euclid::operation::enable_for<TestType>::multi_line_string
        == euclid::operation::details::is_same_v<TestType, euclid::geometry::multi_line_string>);
  CHECK(euclid::operation::enable_for<TestType>::multi_polygon
        == euclid::operation::details::is_same_v<TestType, euclid::geometry::multi_polygon>);
};

TEST_CASE("euclid::operation::enable_for one", "[operation]")
{
  SECTION("point") { test_enable_for_one<euclid::geometry::point>(); }
  SECTION("line_string") { test_enable_for_one<euclid::geometry::line_string>(); }
  SECTION("linear_ring") { test_enable_for_one<euclid::geometry::linear_ring>(); }
  SECTION("polygon") { test_enable_for_one<euclid::geometry::polygon>(); }
  SECTION("multi_geometry") { test_enable_for_one<euclid::geometry::multi_geometry>(); }
  SECTION("multi_point") { test_enable_for_one<euclid::geometry::multi_point>(); }
  SECTION("multi_line_string") { test_enable_for_one<euclid::geometry::multi_line_string>(); }
  SECTION("multi_polygon") { test_enable_for_one<euclid::geometry::multi_polygon>(); }
}
