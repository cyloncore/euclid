#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"

using system = TestType;
using geometry_type = typename system::geometry;
using point_type = typename system::point;
using line_string_type = typename system::line_string;
using linear_ring_type = typename system::linear_ring;
using linear_ring_builder = typename linear_ring_type::builder;
using polygon_type = typename system::polygon;
using geometries = euclid_tests::geometries<system>;
using multi_point_type = typename system::multi_point;
using multi_polygon_type = typename system::multi_polygon;
using multi_geometry_type = typename system::multi_geometry;

#pragma GCC diagnostic pop
