#include "config.h"

namespace
{
  template<typename _system_>
  struct wkt;
}

#undef EUCLID_HAS_ANY_SYSTEM

// Boost
#ifdef EUCLID_HAVE_BOOST
#undef EUCLID_HAS_ANY_SYSTEM
#define EUCLID_HAS_ANY_SYSTEM

#include <euclid/boost>
#define EUCLID_BOOST_SYSTEM , euclid::boost
namespace
{
  template<typename _system_>
  struct wkt;
  template<>
  struct wkt<euclid::boost>
  {
    static constexpr const char* line_string1 = "LINESTRING(0 1 2,2 3 4,4 5 6)";
    static constexpr const char* rline_string1 = "LINESTRING(4 5 6,2 3 4,0 1 2)";
    static constexpr const char* line_string2 = "LINESTRING(0 1,3 4)";
    static constexpr const char* line_string3 = "LINESTRING(0 1,3 4)";
    static constexpr const char* ring1 = "POLYGON((0 1 2,2 3 4,4 5 6,0 1 2))";
    static constexpr const char* mp1 = "MULTIPOINT((0 1),(1 2),(2 3))";
  };
} // namespace
#else
#define EUCLID_BOOST_SYSTEM
#endif

// GEOS
#ifdef EUCLID_HAVE_GEOS
#undef EUCLID_HAS_ANY_SYSTEM
#define EUCLID_HAS_ANY_SYSTEM

#include <euclid/geos>
#define EUCLID_GEOS_SYSTEM , euclid::geos
namespace
{
  template<>
  struct wkt<euclid::geos>
  {
#if GEOS_VERSION_MAJOR > 3 || (GEOS_VERSION_MAJOR == 3 && GEOS_VERSION_MINOR >= 12)
    static constexpr const char* ring1 = "LINEARRING Z (0 1 2, 2 3 4, 4 5 6, 0 1 2)";
    static constexpr const char* rlinear_ring_valid_1
      = "LINEARRING (0 0, 0 1, 0.5 0.51, 1 1, 1 0, 0.5 0.49, 0 0)";
    static constexpr const char* line_string1 = "LINESTRING Z (0 1 2, 2 3 4, 4 5 6)";
    static constexpr const char* rline_string1 = "LINESTRING Z (4 5 6, 2 3 4, 0 1 2)";
    static constexpr const char* line_string2 = "LINESTRING (0 1, 3 4)";
    static constexpr const char* line_string3 = "LINESTRING (0 1, 3 4)";
    static constexpr const char* mp1 = "MULTIPOINT ((0 1), (1 2), (2 3))";
    static constexpr const char* cop1 = "GEOMETRYCOLLECTION (POINT (1 2), POINT (2 3))";
    static constexpr const char* cop1_mp = "MULTIPOINT ((1 2), (2 3))";
#else
    static constexpr const char* ring1
      = "LINEARRING Z (0.00 1.00 2.00, 2.00 3.00 4.00, 4.00 5.00 6.00, 0.00 1.00 2.00)";
    static constexpr const char* rlinear_ring_valid_1
      = "LINEARRING (0.00 0.00, 0.00 1.00, 0.50 0.51, 1.00 1.00, 1.00 0.00, 0.50 0.49, 0.00 0.00)";
    static constexpr const char* line_string1
      = "LINESTRING Z (0.00 1.00 2.00, 2.00 3.00 4.00, 4.00 5.00 6.00)";
    static constexpr const char* rline_string1
      = "LINESTRING Z (4.00 5.00 6.00, 2.00 3.00 4.00, 0.00 1.00 2.00)";
    static constexpr const char* line_string2 = "LINESTRING (0.00 1.00, 3.00 4.00)";
    static constexpr const char* line_string3 = "LINESTRING (0.00 1.00, 3.00 4.00)";
    static constexpr const char* mp1 = "MULTIPOINT (0.00 1.00, 1.00 2.00, 2.00 3.00)";
    static constexpr const char* cop1 = "GEOMETRYCOLLECTION (POINT (1.00 2.00), POINT (2.00 3.00))";
    static constexpr const char* cop1_mp = "MULTIPOINT (1.00 2.00, 2.00 3.00)";
#endif
  };
} // namespace
#else
#define EUCLID_GEOS_SYSTEM
#endif

// GDAL
#ifdef EUCLID_HAVE_GDAL
#undef EUCLID_HAS_ANY_SYSTEM
#define EUCLID_HAS_ANY_SYSTEM

#include <euclid/gdal>
#define EUCLID_GDAL_SYSTEM , euclid::gdal
namespace
{
  template<typename _system_>
  struct wkt;
  template<>
  struct wkt<euclid::gdal>
  {
    static constexpr const char* ring1 = "LINEARRING (0 1 2,2 3 4,4 5 6,0 1 2)";
    static constexpr const char* line_string1 = "LINESTRING (0 1 2,2 3 4,4 5 6)";
    static constexpr const char* line_string2 = "LINESTRING (0 1,3 4)";
    static constexpr const char* line_string3 = "LINESTRING (0 1,3 4)";
    static constexpr const char* mp1 = "MULTIPOINT (0 1,1 2,2 3)";
    static constexpr const char* cop1 = "GEOMETRYCOLLECTION (POINT (1 2),POINT (2 3))";
    static constexpr const char* cop1_mp = "MULTIPOINT (1 2,2 3)";
  };
} // namespace
#else
#define EUCLID_GDAL_SYSTEM
#endif

#define EUCLID_TEST_SYSTEMS EUCLID_BOOST_SYSTEM EUCLID_GEOS_SYSTEM EUCLID_GDAL_SYSTEM

#ifndef EUCLID_HAS_ANY_SYSTEM
#error No systems is available for testing
#endif
