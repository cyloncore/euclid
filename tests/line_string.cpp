#include "systems.h"

#include "catch.hpp"
#include "geometries.h"

TEMPLATE_TEST_CASE("test line_string", "[line_string]" EUCLID_TEST_SYSTEMS)
{
#include "common_using.h"

  line_string_type ls1 = geometries::line_string_1();
  CHECK(euclid::io::to_wkt(ls1, {2, 3}) == wkt<system>::line_string1);
  CHECK(ls1.dimensions() == 3);

  euclid::geometry::point_iterator<line_string_type> it = ls1.points().begin();
  CHECK(it == ls1.points().begin());
  CHECK(it != ls1.points().end());
  euclid::simple::point point = *it;
  CHECK(point.x() == 0.0);
  CHECK(point.y() == 1.0);
  CHECK(point.z() == 2.0);
  ++it;
  point = *it;
  CHECK(point.x() == 2.0);
  CHECK(point.y() == 3.0);
  CHECK(point.z() == 4.0);
  ++it;
  point = *it;
  CHECK(point.x() == 4.0);
  CHECK(point.y() == 5.0);
  CHECK(point.z() == 6.0);
  ++it;
  CHECK(it == ls1.points().end());

  euclid::geometry::segment_iterator<line_string_type> its = ls1.segments().begin();

  CHECK(its == ls1.segments().begin());
  CHECK(its != ls1.segments().end());

  euclid::simple::segment seg = *its;
  CHECK(seg.first().x() == 0.0);
  CHECK(seg.first().y() == 1.0);
  CHECK(seg.first().z() == 2.0);
  CHECK(seg.second().x() == 2.0);
  CHECK(seg.second().y() == 3.0);
  CHECK(seg.second().z() == 4.0);
  ++its;
  seg = *its;
  CHECK(seg.first().x() == 2.0);
  CHECK(seg.first().y() == 3.0);
  CHECK(seg.first().z() == 4.0);
  CHECK(seg.second().x() == 4.0);
  CHECK(seg.second().y() == 5.0);
  CHECK(seg.second().z() == 6.0);
  ++its;
  CHECK(its != ls1.segments().begin());
  CHECK(its == ls1.segments().end());

  // Check creation from a simple::segment
  line_string_type ls2(euclid::simple::segment({0.0, 1.0}, {3.0, 4.0}));
  CHECK(euclid::io::to_wkt(ls2) == wkt<system>::line_string2);
  CHECK(ls2.dimensions() == 2);
  line_string_type ls3;
  ls3 = euclid::simple::segment({0.0, 1.0}, {3.0, 4.0});
  CHECK(euclid::io::to_wkt(ls3) == wkt<system>::line_string3);
  CHECK(ls3.dimensions() == 2);
}
