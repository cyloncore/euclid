#include <euclid/geometry>

namespace euclid_tests
{
  template<typename _system_>
  struct geometries
  {
    using system = _system_;
    using point_type = typename system::point;
    using line_string_type = typename system::line_string;
    using line_string_builder = typename line_string_type::builder;
    using linear_ring_type = typename system::linear_ring;
    using linear_ring_builder = typename linear_ring_type::builder;
    using polygon_type = typename system::polygon;
    using polygon_builder = typename polygon_type::builder;

    using multi_geometry = typename system::multi_geometry;
    using multi_geometry_builder = typename multi_geometry::builder;
    using multi_point = typename system::multi_point;
    using multi_point_builder = typename multi_point::builder;
    using multi_line_string = typename system::multi_line_string;
    using multi_line_string_builder = typename multi_line_string::builder;
    using multi_polygon = typename system::multi_polygon;
    using multi_polygon_builder = typename multi_polygon::builder;

    static line_string_type line_string_1()
    {
      return line_string_builder()
        .add_point(0.0, 1.0, 2.0)
        .add_point(2.0, 3.0, 4.0)
        .add_point(4.0, 5.0, 6.0)
        .create();
    }

    static line_string_type line_string_2()
    {
      return line_string_builder()
        .add_point(0.012345678901, 1.012345678901, 2.012345678901)
        .add_point(2.012345678901, 3.012345678901, 4.012345678901)
        .add_point(4.012345678901, 5.012345678901, 6.012345678901)
        .create();
    }

    static linear_ring_type linear_ring_invalid_1()
    {
      return linear_ring_builder()
        .add_point(0, 0)
        .add_point(1, 1)
        .add_point(1, 0)
        .add_point(0, 1)
        .create();
    }
    static linear_ring_type linear_ring_valid_1()
    {
      return linear_ring_builder()
        .add_point(0, 0)
        .add_point(0.5, 0.49)
        .add_point(1, 0)
        .add_point(1, 1)
        .add_point(0.5, 0.51)
        .add_point(0, 1)
        .create();
    }

    static linear_ring_type linear_ring_invalid_2()
    {
      return linear_ring_builder()
        .add_point(294, -3)
        .add_point(268, -6)
        .add_point(254, 18.5)
        .add_point(269, 45)
        .add_point(294, 39.5)
        .add_point(294, 50)
        .add_point(278, 115)
        .add_point(180, -5)
        .create();
    }
    static linear_ring_type linear_ring_valid_2()
    {
      return linear_ring_builder()
        .add_point(294.00, -3.00)
        .add_point(268.00, -6.00)
        .add_point(266.55, -3.48)
        .add_point(180.00, -5.00)
        .add_point(278.00, 115.00)
        .add_point(294.00, 50.00)
        .add_point(294.00, 39.50)
        .add_point(269.00, 45.00)
        .add_point(254.00, 18.50)
        .add_point(266.57, -3.47)
        .create();
    }
    static linear_ring_type linear_ring_invalid_3()
    {
      return linear_ring_builder()
        .add_point(-97, -262)
        .add_point(-97, -246)
        .add_point(-92, -246)
        .add_point(-92, -240)
        .add_point(-81, -240)
        .add_point(-81, -246)
        .add_point(-76, -246)
        .add_point(-76, -251)
        .add_point(-76, -256)
        .add_point(-81, -256)
        .add_point(-81, -262)
        .add_point(-1, -262)
        .add_point(105, 58)
        .add_point(-3, -74)
        .add_point(-108, -209)
        .add_point(-90, -243)
        .add_point(-129, -251)
        .add_point(-129, -262)
        .create();
    }
    static linear_ring_type linear_ring_valid_3()
    {
      return linear_ring_builder()
        .add_point(-97.00, -262.00)
        .add_point(-97.00, -246.00)
        .add_point(-92.00, -246.00)
        .add_point(-91.93, -243.48)
        .add_point(-90.00, -243.00)
        .add_point(-91.49, -240.02)
        .add_point(-81.00, -240.00)
        .add_point(-81.00, -246.00)
        .add_point(-76.00, -246.00)
        .add_point(-76.00, -251.00)
        .add_point(-76.00, -256.00)
        .add_point(-81.00, -256.00)
        .add_point(-81.00, -262.00)
        .add_point(-1.00, -262.00)
        .add_point(105.00, 58.00)
        .add_point(-3.00, -74.00)
        .add_point(-108.00, -209.00)
        .add_point(-91.64, -239.91)
        .add_point(-92.00, -240.00)
        .add_point(-92.10, -243.42)
        .add_point(-129.00, -251.00)
        .add_point(-129.00, -262.00)
        .create();
    }
    static linear_ring_type linear_ring_invalid_3a()
    {
      return linear_ring_builder()
        .add_point(15.6841306360833794, 58.4294138316326865)
        .add_point(15.6828425558032940, 58.4293886751398404)
        .add_point(15.6823083039117730, 58.4286475873724669)
        .add_point(15.6822949864810024, 58.4286579448556864)
        .add_point(15.6832250423640236, 58.4298991027730708)
        .add_point(15.6825557183203674, 58.4300370880893780)
        .add_point(15.6837487414141972, 58.4304922816094319)
        .create();
    }
    static linear_ring_type linear_ring_valid_3a()
    {
      return linear_ring_builder()
        .add_point(15.6841306360833794, 58.4294138316326865)
        .add_point(15.6828425631334927, 58.4293886696469826)
        .add_point(15.6823083039117730, 58.4286475873724669)
        .add_point(15.6822949864810024, 58.4286579448556864)
        .add_point(15.6832250423640236, 58.4298991027730708)
        .add_point(15.6825557183203674, 58.4300370880893780)
        .add_point(15.6837487414141972, 58.4304922816094319)
        .create();
    }
    static linear_ring_type linear_ring_valid_4()
    {
      return linear_ring_builder()
        .add_point(200.00, 300.00)
        .add_point(300.00, 200.00)
        .add_point(350.00, 100.00)
        .add_point(100.00, 200.00)
        .add_point(150.00, 400.00)
        .add_point(450.00, 450.00)
        .add_point(350.00, 350.00)
        .create();
    }

    /**
     * Used to test dead_angle
     */
    static linear_ring_type linear_ring_valid_5()
    {
      return linear_ring_builder()
        .add_point(533304.579540124046616, 6479093.820733979344368)
        .add_point(533318.604710812098347, 6479221.550906726159155)
        .add_point(533309.503051220788620, 6479138.660182774066925)
        .add_point(533726.966733815032057, 6479473.931415163911879)
        .add_point(533546.661451655323617, 6479698.438917776569724)
        .add_point(533718.295721291680820, 6479888.336007140576839)
        .add_point(534081.902517739566974, 6479597.897023654542863)
        .add_point(534125.161439000745304, 6479652.053795548155904)
        .add_point(534392.289598178234883, 6479410.615937530063093)
        .add_point(533745.649312827386893, 6478695.169609044678509)
        .create();
    }
    /**
     * Expected correction after removing dead angle
     */
    static linear_ring_type linear_ring_valid_no_dead_angle_5()
    { // Invalid one without the second point
      return linear_ring_builder()
        .add_point(533304.579540124046616, 6479093.820733979344368)
        .add_point(533309.503051220788620, 6479138.660182774066925)
        .add_point(533726.966733815032057, 6479473.931415163911879)
        .add_point(533546.661451655323617, 6479698.438917776569724)
        .add_point(533718.295721291680820, 6479888.336007140576839)
        .add_point(534081.902517739566974, 6479597.897023654542863)
        .add_point(534125.161439000745304, 6479652.053795548155904)
        .add_point(534392.289598178234883, 6479410.615937530063093)
        .add_point(533745.649312827386893, 6478695.169609044678509)
        .create();
    }

    /**
     * Used to test dead_angle
     */
    static linear_ring_type linear_ring_valid_5a()
    {
      return linear_ring_builder()
        .add_point(543871.013664874364622, 6471971.398127015680075)
        .add_point(544003.680103511200286, 6471912.278088836930692)
        .add_point(544021.404295719577931, 6471971.691580005921423)
        .add_point(543926.147195947123691, 6471652.379038730636239)
        .add_point(543251.984194484190084, 6471651.063563506118953)
        .add_point(543251.361493999254890, 6471970.189017235301435)
        .create();
    }
    /**
     * Expected correction after removing dead angle
     */
    static linear_ring_type linear_ring_valid_no_dead_angle_5a()
    { // Valid one without the third point (544021.404295719577931, 6471971.691580005921423)
      return linear_ring_builder()
        .add_point(543871.013664874364622, 6471971.398127015680075)
        .add_point(544003.680103511200286, 6471912.278088836930692)
        .add_point(543926.147195947123691, 6471652.379038730636239)
        .add_point(543251.984194484190084, 6471651.063563506118953)
        .add_point(543251.361493999254890, 6471970.189017235301435)
        .create();
    }

    /**
     * Used to test dead_angle
     */
    static linear_ring_type linear_ring_valid_5b()
    {
      return linear_ring_builder()
        .add_point(534843.98, 6469278.99)
        .add_point(534919.02, 6469485.55)
        .add_point(535028.73, 6469567.70)
        .add_point(535042.96, 6469340.20)
        .add_point(534917.74, 6469482.02)
        .create();
    }

    static linear_ring_type linear_ring_valid_no_dead_angle_5b()
    {
      return linear_ring_builder()
        .add_point(534919.02, 6469485.55)
        .add_point(535028.73, 6469567.70)
        .add_point(535042.96, 6469340.20)
        .add_point(534917.74, 6469482.02)
        .create();
    }

    /**
     * Used to test dead_angle
     */
    static linear_ring_type linear_ring_valid_5c()
    {
      return linear_ring_builder()
        .add_point(531374.530403683078475, 6469099.137779771350324)
        .add_point(531303.597700689686462, 6469281.905643973499537)
        .add_point(531209.743913788348436, 6469261.799204397946596)
        .add_point(531385.816507782321423, 6469503.838512266986072)
        .add_point(531486.684332075528800, 6469360.115522462874651)
        .add_point(531301.184854865190573, 6469288.122673064470291)
        .add_point(531374.530403683194891, 6469099.137779771350324)
        .add_point(531329.908917715772986, 6469044.208344805985689)
        .create();
    }
    static linear_ring_type linear_ring_valid_no_dead_angle_5c()
    {
      return linear_ring_builder()
        .add_point(531303.597700689686462, 6469281.905643973499537)
        .add_point(531209.743913788348436, 6469261.799204397946596)
        .add_point(531385.816507782321423, 6469503.838512266986072)
        .add_point(531486.684332075528800, 6469360.115522462874651)
        .add_point(531301.184854865190573, 6469288.122673064470291)
        .create();
    }
    /**
     * Used to test dead_angle
     * remove dead angle need to fall back one or two points
     */
    static linear_ring_type linear_ring_valid_5d()
    {
      return linear_ring_builder()
        .add_point(533234.707188, 6467362.795234)
        .add_point(533207.451936, 6467335.898406)
        .add_point(533195.736412, 6467283.986938)
        .add_point(533193.203849, 6467272.765156)
        .add_point(533217.567231, 6467380.719262)
        .create();
    }
    static linear_ring_type linear_ring_valid_no_dead_angle_5d()
    {
      return linear_ring_builder()
        .add_point(533234.707188, 6467362.795234)
        .add_point(533207.451936, 6467335.898406)
        .add_point(533217.567231, 6467380.719262)
        .create();
    }
    static linear_ring_type linear_ring_valid_5e()
    {
      return linear_ring_builder()
        .add_point(533940.688648369512521, 6467335.491784493438900)
        .add_point(533809.947540613124147, 6467355.631487810984254)
        .add_point(533781.287302143522538, 6467295.549573158845305)
        .add_point(533817.266464955639094, 6467370.972303315065801)
        .create();
    }
    static linear_ring_type linear_ring_valid_no_dead_angle_5e()
    {
      return linear_ring_builder()
        .add_point(533940.688648369512521, 6467335.491784493438900)
        .add_point(533809.947540613124147, 6467355.631487810984254)
        .add_point(533817.266464955639094, 6467370.972303315065801)
        .create();
    }

    /**
     * Used to test duplicate points
     */
    static linear_ring_type linear_ring_valid_6()
    {
      return linear_ring_builder()
        .add_point(15.66558709739223, 58.38675846514775)
        .add_point(15.67108556590190, 58.38437231173737)
        .add_point(15.67108556590189, 58.38437231173737)
        .add_point(15.66576240216444, 58.38676306358337)
        .create();
    }
    /**
     * Expected correction after removing duplicate points
     */
    static linear_ring_type linear_ring_valid_no_duplicate_points_6()
    {
      return linear_ring_builder()
        .add_point(15.66558709739223, 58.38675846514775)
        .add_point(15.67108556590190, 58.38437231173737)
        .add_point(15.66576240216444, 58.38676306358337)
        .create();
    }

    static linear_ring_type linear_ring_invalid_6a()
    {
      return linear_ring_builder()
        .add_point(543871.013664874364622, 6471971.398127015680075)
        .add_point(544003.680103511200286, 6471912.278088836930692)
        .add_point(544021.404295719577931, 6471971.691580005921423)
        .add_point(544021.404295719694346, 6471971.691580005921423)
        .add_point(543926.147195947123691, 6471652.379038730636239)
        .add_point(543251.984194484190084, 6471651.063563506118953)
        .add_point(543251.361493999254890, 6471970.189017235301435)
        .create();
    }
    static linear_ring_type linear_ring_invalid_no_duplicate_points_6a()
    {
      return linear_ring_builder()
        .add_point(543871.013664874364622, 6471971.398127015680075)
        .add_point(544003.680103511200286, 6471912.278088836930692)
        .add_point(544021.404295719694346, 6471971.691580005921423)
        .add_point(543926.147195947123691, 6471652.379038730636239)
        .add_point(543251.984194484190084, 6471651.063563506118953)
        .add_point(543251.361493999254890, 6471970.189017235301435)
        .create();
    }

    static linear_ring_type linear_ring_invalid_6b()
    {
      return linear_ring_builder()
        .add_point(533940.688648369512521, 6467335.491784493438900)
        .add_point(533809.947540613124147, 6467355.631487810984254)
        .add_point(533781.287302143522538, 6467295.549573158845305)
        .add_point(533781.283088027150370, 6467295.549424100667238)
        .add_point(533817.266464955639094, 6467370.972303315065801)
        .create();
    }
    static linear_ring_type linear_ring_invalid_no_duplicate_points_6b()
    {
      return linear_ring_builder()
        .add_point(533940.688648369512521, 6467335.491784493438900)
        .add_point(533809.947540613124147, 6467355.631487810984254)
        .add_point(533781.287302144, 6467295.549573159)
        .add_point(533817.266464955639094, 6467370.972303315065801)
        .create();
    }

    static polygon_type polygon_1()
    {
      return polygon_builder()
        .shell()
        .add_point(0.0, 1.0)
        .add_point(1.0, 1.0)
        .add_point(1.0, 0.0)
        .add_point(0.0, 0.0)
        .create();
    }
    static polygon_type polygon_2()
    {
      return polygon_builder()
        .shell()
        .add_point(0.5, 1.5)
        .add_point(1.5, 1.5)
        .add_point(1.5, 0.5)
        .add_point(0.5, 0.5)
        .create();
    }
    static polygon_type polygon_3()
    {
      return polygon_builder()
        .add_point(0.0, 1.0)
        .add_point(1.0, 1.0)
        .add_point(1.0, 0.0)
        .add_point(0.0, 0.0)
        .start_interior_ring()
        .add_point(0.1, 0.1)
        .add_point(0.1, 0.4)
        .add_point(0.4, 0.4)
        .add_point(0.4, 0.1)
        .start_interior_ring()
        .add_point(0.9, 0.9)
        .add_point(0.9, 0.6)
        .add_point(0.6, 0.6)
        .add_point(0.6, 0.9)
        .create();
    }
    static polygon_type polygon_4()
    {
      return polygon_builder()
        .shell()
        .add_point(0.0, 1.0)
        .add_point(0.9, 1.1)
        .add_point(1.1, -0.1)
        .add_point(-0.1, 0.1)
        .create();
    }
    static polygon_type polygon_5()
    {
      return polygon_builder()
        .shell()
        .add_point(798.50, 1107.04)
        .add_point(1519.00, 1586.00)
        .add_point(1482.00, 1622.00)
        .add_point(1466.00, 1856.00)
        .add_point(1173.00, 1584.00)
        .add_point(998.00, 1708.00)
        .add_point(1055.00, 1350.00)
        .add_point(726.00, 1237.00)
        .add_point(832.00, 1047.00)
        .create();
    }
    /**
     * Use to test polugon need splitting
     */
    static polygon_type polygon_valid_6()
    {
      return polygon_builder()
        .add_point(15.57174301244809, 58.44745057026853)
        .add_point(15.56479886995026, 58.44772405882680)
        .add_point(15.57040226765988, 58.45018755478176)
        .add_point(15.56925295824999, 58.45141558484939)
        .add_point(15.56427033954281, 58.44922504049960)
        .add_point(15.56507762411119, 58.45486176235363)
        .add_point(15.56606353570062, 58.45482296171626)
        .add_point(15.57051313391589, 58.45006908980116)
        .add_point(15.57218001813660, 58.45049773929289)
        .create();
    }
    /**
     * Resulting linear rings after splitting
     */
    static multi_polygon multi_polygon_splited_valid_6()
    {
      return multi_polygon_builder()
        .add_element(polygon_builder()
                       .add_point(15.57174301244809, 58.44745057026853)
                       .add_point(15.56479886995026, 58.44772405882680)
                       .add_point(15.57040226765988, 58.45018755478176)
                       .add_point(15.57051313391589, 58.45006908980116)
                       .add_point(15.57218001813660, 58.45049773929289)
                       .create())
        .add_element(polygon_builder()
                       .add_point(15.56925295824999, 58.45141558484939)
                       .add_point(15.56427033954281, 58.44922504049960)
                       .add_point(15.56507762411119, 58.45486176235363)
                       .add_point(15.56606353570062, 58.45482296171626)
                       .create())
        .create();
    }
    /**
     * polygon that should not get splited
     */
    static polygon_type polygon_valid_6a()
    {
      return polygon_builder()
        .add_point(0, 0)
        .add_point(10, 10)
        .add_point(5, 10)
        .add_point(5, 12)
        .add_point(15, 12)
        .add_point(5, 4)
        .add_point(3, 2)
        .add_point(20, 0)
        .add_point(20, 20)
        .add_point(0, 20)
        .create();
    }

    static polygon_type polygon_valid_6b()
    {
      return polygon_builder()
        .add_point(537042.74, 6479091.97)
        .add_point(536367.17, 6479012.67)
        .add_point(536370.92, 6478980.76)
        .add_point(536199.17, 6479249.73)
        .add_point(536726.85, 6479586.68)
        .add_point(536956.92, 6479226.38)
        .add_point(536872.89, 6479072.04)
        .add_point(536901.71, 6479075.42)
        .add_point(536971.48, 6479203.57)
        .start_interior_ring()
        .add_point(536461.94, 6479078.63)
        .add_point(536384.02, 6479139.45)
        .add_point(536433.63, 6479042.37)
        .create();
    }
    static multi_polygon multi_polygon_splited_valid_6b()
    {
      return multi_polygon_builder()
        .add_element(polygon_builder()
                       .add_point(537042.74, 6479091.97)
                       .add_point(536901.71, 6479075.42)
                       .add_point(536971.48, 6479203.57)
                       .create())
        .add_element(polygon_builder()
                       .add_point(536367.17, 6479012.67)
                       .add_point(536370.92, 6478980.76)
                       .add_point(536199.17, 6479249.73)
                       .add_point(536726.85, 6479586.68)
                       .add_point(536956.92, 6479226.38)
                       .add_point(536872.89, 6479072.04)
                       .start_interior_ring()
                       .add_point(536461.94, 6479078.63)
                       .add_point(536384.02, 6479139.45)
                       .add_point(536433.63, 6479042.37)
                       .create())
        .create();
    }

    static polygon_type polygon_valid_6c()
    {
      return polygon_builder()
        .add_point(527151.144100819248706, 6480799.566731757484376)
        .add_point(527220.695462721283548, 6480650.452153471298516)
        .add_point(527003.198463596054353, 6480669.513895491138101)
        .add_point(526988.519605900044553, 6480396.054974288679659)
        .add_point(526881.727140495087951, 6480401.785222706384957)
        .add_point(526868.554363239556551, 6480251.368630995042622)
        .add_point(526821.390313436742872, 6480405.025034342892468)
        .add_point(526988.519605900044553, 6480396.054974289610982)
        .add_point(527012.183314963360317, 6480836.954915769398212)
        .create();
    }

    static multi_polygon multi_polygon_splited_valid_6c()
    {
      return multi_polygon_builder()
        .add_element(polygon_builder()
                       .add_point(527151.14, 6480799.57)
                       .add_point(527220.70, 6480650.45)
                       .add_point(527003.20, 6480669.51)
                       .add_point(527012.18, 6480836.95)
                       .create())
        .add_element(polygon_builder()
                       .add_point(526988.52, 6480396.05)
                       .add_point(526881.73, 6480401.79)
                       .add_point(526868.55, 6480251.37)
                       .add_point(526821.39, 6480405.03)
                       .add_point(526988.52, 6480396.05)
                       .create())
        .create();
    }

    /**
     * Use to test polugon that have tangential holes
     */
    static polygon_type polygon_valid_7()
    {
      return polygon_builder()
        .add_point(542449.364744876977056, 6475715.751960185356438)
        .add_point(542269.500271608936600, 6475759.903041247278452)
        .add_point(542238.134017288335599, 6476043.567167147062719)
        .add_point(542241.353747019311413, 6476043.923189824447036)
        .start_interior_ring()
        .add_point(542285.981810987694189, 6475973.515149962157011)
        .add_point(542282.711758913123049, 6475783.831323725171387)
        .add_point(542407.577143020695075, 6475781.678708415478468)
        .create();
    }
    static polygon_type polygon_valid_7r()
    { // reversed hole
      return polygon_builder()
        .add_point(542449.364744876977056, 6475715.751960185356438)
        .add_point(542269.500271608936600, 6475759.903041247278452)
        .add_point(542238.134017288335599, 6476043.567167147062719)
        .add_point(542241.353747019311413, 6476043.923189824447036)
        .start_interior_ring()
        .add_point(542407.577143020695075, 6475781.678708415478468)
        .add_point(542282.711758913123049, 6475783.831323725171387)
        .add_point(542285.981810987694189, 6475973.515149962157011)
        .create();
    }
    /**
     * Use to test polugon that have tangential holes
     */
    static polygon_type polygon_valid_without_holes_7()
    {
      return polygon_builder()
        // points added from the hole
        .add_point(542449.364744876977056, 6475715.751960185356438)
        .add_point(542269.500271608936600, 6475759.903041247278452)
        .add_point(542238.134017288335599, 6476043.567167147062719)
        .add_point(542241.353747019311413, 6476043.923189824447036)
        .add_point(542285.981810987694189, 6475973.515149962157011)
        .add_point(542282.711758913123049, 6475783.831323725171387)
        .add_point(542407.577143020695075, 6475781.678708415478468)
        .create();
    }
    /**
     * Use to test polugon that have tangential holes
     */
    static polygon_type polygon_valid_8()
    {
      return polygon_builder()
        .add_point(532110.14, 6464415.79)
        .add_point(532272.95, 6464334.03)
        .add_point(531826.72, 6464138.82)
        .add_point(531511.81, 6464858.67)
        .add_point(532322.81, 6465213.45)
        .add_point(532492.98, 6464824.45)
        .add_point(532486.97, 6464814.41)
        .add_point(532351.18, 6464895.78)
        .start_interior_ring()
        .add_point(532222.43, 6464972.92)
        .add_point(531867.06, 6464970.00)
        .add_point(531869.55, 6464667.50)
        .add_point(532238.06, 6464670.53)
        .add_point(532351.18, 6464895.78)
        .create();
    }

    static polygon_type polygon_valid_without_holes_8()
    {
      return polygon_builder()
        .add_point(532110.14, 6464415.79)
        .add_point(532272.95, 6464334.03)
        .add_point(531826.72, 6464138.82)
        .add_point(531511.81, 6464858.67)
        .add_point(532322.81, 6465213.45)
        .add_point(532492.98, 6464824.45)
        .add_point(532486.97, 6464814.41)
        .add_point(532351.18, 6464895.78)
        .add_point(532222.43, 6464972.92)
        .add_point(531867.06, 6464970.00)
        .add_point(531869.55, 6464667.50)
        .add_point(532238.06, 6464670.53)
        .create();
    }

    static multi_point multi_point_1()
    {
      return multi_point_builder()
        .add_point(0.0, 1.0)
        .add_point(1.0, 2.0)
        .add_point(2.0, 3.0)
        .create();
    }
    static multi_line_string multi_line_string_1()
    {
      return multi_line_string_builder().add_element(line_string_1()).create();
    }
    static multi_polygon multi_polygon_1()
    {
      return multi_polygon_builder()
        .add_element(polygon_1())
        .add_element(polygon_2())
        .add_element(polygon_3())
        .create();
    }
    static multi_geometry collection_1()
    {
      return multi_geometry_builder()
        .add_element(point_type(1.0, 2.0))
        .add_element(line_string_1())
        .add_element(polygon_1())
        .add_element(polygon_2())
        .add_element(polygon_3())
        .create();
    }
    static multi_geometry collection_only_poiny_1()
    {
      return multi_geometry_builder()
        .add_element(point_type(1.0, 2.0))
        .add_element(point_type(2.0, 3.0))
        .create();
    }
  };
} // namespace euclid_tests
