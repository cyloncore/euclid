#include "systems.h"

template<typename _system_>
void test_traits()
{
  using system = _system_;
  using point_type = typename system::point;
  using line_string_type = typename system::line_string;
  using linear_ring_type = typename system::linear_ring;
  using polygon_type = typename system::polygon;
  using multi_geometry_type = typename system::multi_geometry;
  using multi_point_type = typename system::multi_point;
  using multi_line_string_type = typename system::multi_line_string;
  using multi_polygon_type = typename system::multi_polygon;

  namespace egt = euclid::geometry::traits;
  static_assert(egt::is_a_geometry_v<point_type>);
  static_assert(egt::is_a_geometry_v<line_string_type>);
  static_assert(egt::is_a_geometry_v<linear_ring_type>);
  static_assert(egt::is_a_geometry_v<polygon_type>);
  static_assert(egt::is_a_geometry_v<multi_geometry_type>);
  static_assert(egt::is_a_geometry_v<multi_point_type>);
  static_assert(egt::is_a_geometry_v<multi_line_string_type>);
  static_assert(egt::is_a_geometry_v<multi_polygon_type>);
  static_assert(not egt::is_a_geometry_v<int>);
  static_assert(not egt::is_a_geometry_v<system>);
  static_assert(egt::is_collection_v<multi_geometry_type>);
  static_assert(egt::is_collection_v<multi_point_type>);
  static_assert(egt::is_collection_v<multi_line_string_type>);
  static_assert(egt::is_collection_v<multi_polygon_type>);
}

#define TEST_TRAITS(_system_)                                                                      \
  void test_traits_##_system_() { test_traits<euclid::_system_>(); }

#ifdef EUCLID_HAVE_GEOS
TEST_TRAITS(geos)
#endif
#ifdef EUCLID_HAVE_BOOST
TEST_TRAITS(boost)
#endif
#ifdef EUCLID_HAVE_GDAL
TEST_TRAITS(gdal)
#endif
