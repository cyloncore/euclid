#include "systems.h"

#include <euclid/extra/compactness>
#include <euclid/extra/curve_value>
#include <euclid/extra/fix_it>
#include <euclid/extra/is_clock_wise>
#include <euclid/extra/is_convex>
#include <euclid/extra/longest_segment>
#include <euclid/extra/reverse_order>
#include <euclid/extra/transform>

#include "catch.hpp"
#include "geometries.h"

TEMPLATE_TEST_CASE("test correct_self_intersecting", "[extra][correct_self_intersecting]",
                   euclid::geos)
{
#include "common_using.h"

  // lr1
  linear_ring_type lr1 = geometries::linear_ring_invalid_1();
  CHECK(not euclid::ops::is_valid(lr1));
  linear_ring_type lr1_f = euclid::extra::fix_it::correct_self_intersection(lr1, 0.01);
  CHECK(euclid::ops::is_valid(lr1_f));
  CHECK(euclid::io::to_wkt(lr1_f) == euclid::io::to_wkt(geometries::linear_ring_valid_1()));

  // lr2
  linear_ring_type lr2 = geometries::linear_ring_invalid_2();
  CHECK(not euclid::ops::is_valid(lr2));
  linear_ring_type lr2_f = euclid::extra::fix_it::correct_self_intersection(lr2, 0.01);
  CHECK(euclid::ops::is_valid(lr2_f));
  CHECK(euclid::io::to_wkt(lr2_f) == euclid::io::to_wkt(geometries::linear_ring_valid_2()));

  // lr3
  linear_ring_type lr3 = geometries::linear_ring_invalid_3();
  CHECK(not euclid::ops::is_valid(lr3));
  linear_ring_type lr3_f = euclid::extra::fix_it::correct_self_intersection(lr3, 0.1 / 1.1);
  CHECK(euclid::ops::is_valid(lr3_f));
  CHECK(euclid::io::to_wkt(lr3_f) == euclid::io::to_wkt(geometries::linear_ring_valid_3()));

  // lr3a
  linear_ring_type lr3a = geometries::linear_ring_invalid_3a();
  CHECK(not euclid::ops::is_valid(lr3a));
  linear_ring_type lr3a_f = euclid::extra::fix_it::correct_self_intersection(lr3a);
  CHECK(euclid::ops::is_valid(lr3a_f));
  CHECK(euclid::io::to_wkt(lr3a_f) == euclid::io::to_wkt(geometries::linear_ring_valid_3a()));
}

TEMPLATE_TEST_CASE("test remove_dead_angle", "[extra][remove_dead_angle]", euclid::geos)
{
#include "common_using.h"
  // lr5
  linear_ring_type lr5 = geometries::linear_ring_valid_5();
  linear_ring_type lr5_f = euclid::extra::fix_it::remove_dead_angle(lr5);
  CHECK(euclid::ops::is_valid(lr5_f));
  CHECK(euclid::io::to_wkt(lr5_f)
        == euclid::io::to_wkt(geometries::linear_ring_valid_no_dead_angle_5()));
  // lr5a
  linear_ring_type lr5a = geometries::linear_ring_valid_5a();
  linear_ring_type lr5a_f = euclid::extra::fix_it::remove_dead_angle(lr5a);
  CHECK(euclid::ops::is_valid(lr5a_f));
  CHECK(euclid::io::to_wkt(lr5a_f)
        == euclid::io::to_wkt(geometries::linear_ring_valid_no_dead_angle_5a()));
  // lr5b
  linear_ring_type lr5b = geometries::linear_ring_valid_5b();
  linear_ring_type lr5b_f = euclid::extra::fix_it::remove_dead_angle(lr5b);
  CHECK(euclid::ops::is_valid(lr5b_f));
  CHECK(euclid::io::to_wkt(lr5b_f)
        == euclid::io::to_wkt(geometries::linear_ring_valid_no_dead_angle_5b()));
  // lr5c
  linear_ring_type lr5c = geometries::linear_ring_valid_5c();
  linear_ring_type lr5c_f = euclid::extra::fix_it::remove_dead_angle(lr5c);
  CHECK(euclid::ops::is_valid(lr5c_f));
  CHECK(euclid::io::to_wkt(lr5c_f)
        == euclid::io::to_wkt(geometries::linear_ring_valid_no_dead_angle_5c()));
  // lr5d
  linear_ring_type lr5d = geometries::linear_ring_valid_5d();
  linear_ring_type lr5d_f = euclid::extra::fix_it::remove_dead_angle(lr5d);
  CHECK(euclid::ops::is_valid(lr5d_f));
  CHECK(euclid::io::to_wkt(lr5d_f)
        == euclid::io::to_wkt(geometries::linear_ring_valid_no_dead_angle_5d()));
  // lr5e
  linear_ring_type lr5e = geometries::linear_ring_valid_5e();
  linear_ring_type lr5e_f = euclid::extra::fix_it::remove_dead_angle(lr5e);
  CHECK(euclid::ops::is_valid(lr5e_f));
  CHECK(euclid::io::to_wkt(lr5e_f)
        == euclid::io::to_wkt(geometries::linear_ring_valid_no_dead_angle_5e()));
}

TEMPLATE_TEST_CASE("test remove_duplicate_points", "[extra][remove_duplicate_points]", euclid::geos)
{
#include "common_using.h"
  // lr6
  linear_ring_type lr6 = geometries::linear_ring_valid_6();
  linear_ring_type lr6_f = euclid::extra::fix_it::remove_duplicate_points(lr6);
  CHECK(euclid::ops::is_valid(lr6_f));
  CHECK(euclid::io::to_wkt(lr6_f, {10})
        == euclid::io::to_wkt(geometries::linear_ring_valid_no_duplicate_points_6(), {10}));
  // lr6a
  linear_ring_type lr6a = geometries::linear_ring_invalid_6a();
  linear_ring_type lr6a_f = euclid::extra::fix_it::remove_duplicate_points(lr6a);
  CHECK(euclid::io::to_wkt(lr6a_f, {9})
        == euclid::io::to_wkt(geometries::linear_ring_invalid_no_duplicate_points_6a(), {9}));
  // lr6b
  linear_ring_type lr6b = geometries::linear_ring_invalid_6b();
  linear_ring_type lr6b_f = euclid::extra::fix_it::remove_duplicate_points(lr6b);
  CHECK(euclid::io::to_wkt(lr6b_f, {9})
        == euclid::io::to_wkt(geometries::linear_ring_invalid_no_duplicate_points_6b(), {9}));
}

TEMPLATE_TEST_CASE("test split", "[extra][split]", euclid::geos)
{
#include "common_using.h"
  // lr5
  polygon_type lr6 = geometries::polygon_valid_6();
  multi_polygon_type lr6_f = euclid::extra::fix_it::split(lr6).template cast<multi_polygon_type>();
  CHECK(euclid::ops::is_valid(lr6_f));
  CHECK(euclid::io::to_wkt(lr6_f, {10})
        == euclid::io::to_wkt(geometries::multi_polygon_splited_valid_6(), {10}));

  polygon_type lr6a = geometries::polygon_valid_6a();
  CHECK(euclid::io::to_wkt(euclid::extra::fix_it::split(lr6a), {10})
        == euclid::io::to_wkt(lr6a, {10}));

  polygon_type lr6b = geometries::polygon_valid_6b();
  geometry_type lr6b_g = euclid::extra::fix_it::split(lr6b);
  CHECK(lr6b_g.template is<multi_polygon_type>());
  if(lr6b_g.template is<multi_polygon_type>())
  {
    multi_polygon_type lr6b_f = lr6b_g.template cast<multi_polygon_type>();
    CHECK(euclid::ops::is_valid(lr6b_f));
    CHECK(euclid::io::to_wkt(lr6b_f, {2})
          == euclid::io::to_wkt(geometries::multi_polygon_splited_valid_6b(), {2}));
  }

  polygon_type lr6c = geometries::polygon_valid_6c();
  geometry_type lr6c_g = euclid::extra::fix_it::split(lr6c);
  CHECK(lr6c_g.template is<multi_polygon_type>());
  if(lr6c_g.template is<multi_polygon_type>())
  {
    multi_polygon_type lr6c_f = lr6c_g.template cast<multi_polygon_type>();
    CHECK(euclid::ops::is_valid(lr6c_f));
    CHECK(euclid::io::to_wkt(lr6c_f, {2})
          == euclid::io::to_wkt(geometries::multi_polygon_splited_valid_6c(), {2}));
  }
}

TEMPLATE_TEST_CASE("test remove_tangeant_holes", "[extra][remove_tangeant_holes]", euclid::geos)
{
#include "common_using.h"
  // lr7
  polygon_type lr7 = geometries::polygon_valid_7();
  polygon_type lr7_f
    = euclid::extra::fix_it::remove_tangeant_holes(lr7).template cast<polygon_type>();
  CHECK(euclid::ops::is_valid(lr7_f));
  CHECK(euclid::io::to_wkt(lr7_f, {10})
        == euclid::io::to_wkt(geometries::polygon_valid_without_holes_7(), {10}));
  // lr7
  polygon_type lr7r = geometries::polygon_valid_7r();
  polygon_type lr7r_f
    = euclid::extra::fix_it::remove_tangeant_holes(lr7r).template cast<polygon_type>();
  CHECK(euclid::ops::is_valid(lr7r_f));
  CHECK(euclid::io::to_wkt(lr7r_f, {10})
        == euclid::io::to_wkt(geometries::polygon_valid_without_holes_7(), {10}));
  // lr8
  polygon_type lr8 = geometries::polygon_valid_8();
  polygon_type lr8_f
    = euclid::extra::fix_it::remove_tangeant_holes(lr8).template cast<polygon_type>();
  CHECK(euclid::ops::is_valid(lr8_f));
  CHECK(euclid::io::to_wkt(lr8_f, {2})
        == euclid::io::to_wkt(geometries::polygon_valid_without_holes_8(), {2}));
}

TEMPLATE_TEST_CASE("test is_convex", "[extra][is_convex]", euclid::geos)
{
#include "common_using.h"

  CHECK(not euclid::extra::is_convex(geometries::linear_ring_valid_1()));
  CHECK(not euclid::extra::is_convex(geometries::linear_ring_valid_2()));
  CHECK(not euclid::extra::is_convex(geometries::linear_ring_valid_3()));
  CHECK(not euclid::extra::is_convex(geometries::linear_ring_valid_4()));
  CHECK(euclid::extra::is_convex(geometries::polygon_1().exterior_ring()));
  CHECK(euclid::extra::is_convex(geometries::polygon_2().exterior_ring()));
  CHECK(euclid::extra::is_convex(geometries::polygon_3().exterior_ring()));
  CHECK(euclid::extra::is_convex(geometries::polygon_3().interior_rings()[0]));
  CHECK(euclid::extra::is_convex(geometries::polygon_3().interior_rings()[1]));
}

TEMPLATE_TEST_CASE("test is_clock_wise", "[extra][is_clock_wise]", euclid::geos)
{
#include "common_using.h"

  CHECK(not euclid::extra::is_clock_wise(geometries::linear_ring_valid_1()));
  CHECK(euclid::extra::is_clock_wise(geometries::linear_ring_valid_2()));
  CHECK(not euclid::extra::is_clock_wise(geometries::linear_ring_valid_3()));
  CHECK(euclid::extra::is_clock_wise(geometries::polygon_1().exterior_ring()));
  CHECK(euclid::extra::is_clock_wise(geometries::polygon_2().exterior_ring()));
  CHECK(euclid::extra::is_clock_wise(geometries::polygon_3().exterior_ring()));
  CHECK(euclid::extra::is_clock_wise(geometries::polygon_3().interior_rings()[0]));
  CHECK(euclid::extra::is_clock_wise(geometries::polygon_3().interior_rings()[1]));
}

#define CHECK_SIGNED_AREA(_WHAT_)                                                                  \
  CHECK(std::abs(euclid::extra::signed_area(_WHAT_))                                               \
        == Approx(euclid::ops::area(polygon_type(_WHAT_))))

TEMPLATE_TEST_CASE("test signed_area", "[extra][signed_area]", euclid::geos)
{
#include "common_using.h"

  CHECK_SIGNED_AREA(geometries::linear_ring_valid_1());
  CHECK_SIGNED_AREA(geometries::linear_ring_valid_2());
  CHECK_SIGNED_AREA(geometries::linear_ring_valid_3());
  CHECK_SIGNED_AREA(geometries::polygon_1().exterior_ring());
  CHECK_SIGNED_AREA(geometries::polygon_2().exterior_ring());
  CHECK_SIGNED_AREA(geometries::polygon_1());
  CHECK_SIGNED_AREA(geometries::polygon_2());
  CHECK_SIGNED_AREA(geometries::polygon_3().exterior_ring());
  CHECK_SIGNED_AREA(geometries::polygon_3().interior_rings()[0]);
}

TEMPLATE_TEST_CASE("test reverse_order", "[extra][reverse_order]", euclid::geos)
{
#include "common_using.h"

  CHECK(euclid::io::to_wkt(euclid::extra::reverse_order(geometries::line_string_1()), {2, 3})
        == wkt<system>::rline_string1);
  CHECK(euclid::io::to_wkt(euclid::extra::reverse_order(geometries::linear_ring_valid_1()))
        == wkt<system>::rlinear_ring_valid_1);
}

TEMPLATE_TEST_CASE("test curve_value", "[extra][curve_value]", euclid::geos)
{
#include "common_using.h"

  linear_ring_type lr1 = geometries::polygon_1().exterior_ring();

  CHECK(euclid::extra::curve_value(lr1, 0.5) == euclid::simple::point(0.5, 1.0));
  CHECK(euclid::extra::curve_value(lr1, 1.5) == euclid::simple::point(1.0, 0.5));
  CHECK(euclid::extra::curve_value(lr1, 4.5) == euclid::simple::point(0.5, 1.0));
  euclid::simple::point pt = euclid::extra::curve_value(geometry_type(lr1), 4.5);
  CHECK(pt == euclid::simple::point(0.5, 1.0));
  CHECK(euclid::extra::curve_value(geometry_type(lr1), 4.5) == euclid::simple::point(0.5, 1.0));
}

TEMPLATE_TEST_CASE("test compactness", "[extra][compactness]", euclid::geos)
{
#include "common_using.h"

  polygon_type poly1 = geometries::polygon_1();
  polygon_type poly4 = geometries::polygon_4();
  polygon_type poly5 = geometries::polygon_5();
  linear_ring_type lr1 = geometries::polygon_1().exterior_ring();

  CHECK(euclid::extra::compactness::schwartzberg(lr1) == 0.8862269254527579);
  CHECK(euclid::extra::compactness::schwartzberg(geometry_type(lr1)) == 0.8862269254527579);
  CHECK(euclid::extra::compactness::schwartzberg(poly1) == 0.8862269254527579);
  CHECK(euclid::extra::compactness::schwartzberg(geometry_type(poly1)) == 0.8862269254527579);
  CHECK(euclid::extra::compactness::schwartzberg(poly4) == 0.8760063844546492);
  CHECK(euclid::extra::compactness::schwartzberg(geometry_type(poly4)) == 0.8760063844546492);
  CHECK(euclid::extra::compactness::schwartzberg(poly5) == 0.5172423049028898);

  CHECK(euclid::extra::compactness::reock(poly1) == 0.6366197723675813);
  CHECK(euclid::extra::compactness::reock(geometry_type(poly1)) == 0.6366197723675813);
  CHECK(euclid::extra::compactness::reock(poly4) == 0.5787452476068922);
  CHECK(euclid::extra::compactness::reock(geometry_type(poly4)) == 0.5787452476068922);

#ifdef EUCLID_GEOS_HAS_MAXIMUM_INSCRIBED_CIRCLE
  CHECK(euclid::extra::compactness::two_balls(poly1) == 0.7071067811865476);
  CHECK(euclid::extra::compactness::two_balls(geometry_type(poly1)) == 0.7071067811865476);
  CHECK(euclid::extra::compactness::two_balls(poly4) == Approx(0.6662773044478847));
  CHECK(euclid::extra::compactness::two_balls(geometry_type(poly4)) == Approx(0.6662773044478847));
  CHECK(euclid::extra::compactness::two_balls(poly5) == Approx(0.218209248043241821));
#endif

  CHECK(euclid::extra::compactness::minimum_rotated_rectangle_ratio(poly1) == 1.0);
  CHECK(euclid::extra::compactness::minimum_rotated_rectangle_ratio(geometry_type(poly1)) == 1.0);
#if GEOS_VERSION_MAJOR > 3 || (GEOS_VERSION_MAJOR == 3 && GEOS_VERSION_MINOR >= 12)
  CHECK(euclid::extra::compactness::minimum_rotated_rectangle_ratio(poly4) == Approx(0.963636));
  CHECK(euclid::extra::compactness::minimum_rotated_rectangle_ratio(geometry_type(poly4))
        == Approx(0.963636));
#else
  CHECK(euclid::extra::compactness::minimum_rotated_rectangle_ratio(poly4) == 0.9090909090909091);
  CHECK(euclid::extra::compactness::minimum_rotated_rectangle_ratio(geometry_type(poly4))
        == 0.9090909090909091);
#endif

  CHECK(euclid::extra::compactness::polsby_popper(lr1) == 0.7853981633974483);
  CHECK(euclid::extra::compactness::polsby_popper(geometry_type(lr1)) == 0.7853981633974483);
  CHECK(euclid::extra::compactness::polsby_popper(poly1) == 0.7853981633974483);
  CHECK(euclid::extra::compactness::polsby_popper(geometry_type(poly1)) == 0.7853981633974483);
  CHECK(euclid::extra::compactness::polsby_popper(poly4) == 0.7673871856053067);
  CHECK(euclid::extra::compactness::polsby_popper(geometry_type(poly4)) == 0.7673871856053067);

  CHECK(euclid::extra::compactness::moment_of_inertia(lr1) == 0.6366197723675814);
  CHECK(euclid::extra::compactness::moment_of_inertia(geometry_type(lr1)) == 0.6366197723675814);
  CHECK(euclid::extra::compactness::moment_of_inertia(poly1) == 0.6366197723675814);
  CHECK(euclid::extra::compactness::moment_of_inertia(geometry_type(poly1)) == 0.6366197723675814);
  CHECK(euclid::extra::compactness::moment_of_inertia(poly4) == 0.7294601558378535);
  CHECK(euclid::extra::compactness::moment_of_inertia(geometry_type(poly4)) == 0.7294601558378535);
}
