#ifndef EUCLID_SIMPLE_HEADER
#include <euclid/forward>
#include <iomanip>
#include <tuple>
#endif

namespace euclid::simple
{
  class point : public std::tuple<double, double, double>
  {
    using __tuple__ = std::tuple<double, double, double>;
  public:
    /**
     * @return an invalid point (i.e. one where x, y, z are NAN)
     */
    static point invalid() { return point(NAN, NAN, NAN); }
  public:
    point() : point(0.0, 0.0) {}
    template<typename _interface_>
    point(const euclid::geometry::point<_interface_>& _pt) : point(_pt.x(), _pt.y(), _pt.z())
    {
    }
    point(double _x, double _y, double _z = NAN) : __tuple__(_x, _y, _z) {}
    double x() const { return std::get<0>(*this); }
    double y() const { return std::get<1>(*this); }
    double z() const { return std::get<2>(*this); }
    double& x() { return std::get<0>(*this); }
    double& y() { return std::get<1>(*this); }
    double& z() { return std::get<2>(*this); }
    inline bool operator==(const std::tuple<double, double, double>& _rhs) const;
    inline bool operator==(const point& _rhs) const;
    std::size_t dimensions() const { return std::isnan(z()) ? 2 : 3; }
    /**
     * @return true if a point is valid (i.e. x and y are not NAN)
     */
    bool is_valid() const { return not std::isnan(x()) and not std::isnan(y()); }
  };

  bool point::operator==(const std::tuple<double, double, double>& _rhs) const
  {
    auto [xr, yr, zr] = _rhs;
    return x() == xr and y() == yr and ((std::isnan(z()) and std::isnan(zr)) or z() == zr);
  }
  bool point::operator==(const point& _rhs) const
  {
    return operator==((const std::tuple<double, double, double>&)_rhs);
  }

  inline std::ostream& operator<<(std::ostream& os, euclid::simple::point const& value)
  {
    os << "{" << value.x() << ", " << value.y();
    if(not std::isnan(value.z()))
    {
      os << ", " << value.z();
    }
    os << "}";
    return os;
  }
} // namespace euclid::simple

namespace std
{
  template<>
  struct tuple_size<euclid::simple::point> : std::integral_constant<size_t, 3>
  {
  };

  template<std::size_t _index_>
  struct tuple_element<_index_, euclid::simple::point>
  {
    using type = double;
  };
} // namespace std

namespace euclid::simple
{
  namespace __internal__
  {
    template<template<typename _T_> class _Op_>
    inline point binop(const point& _pt1, const point& _pt2)
    {
      using op = _Op_<double>;
      return point(op()(_pt1.x(), _pt2.x()), op()(_pt1.y(), _pt2.y()), op()(_pt1.z(), _pt2.z()));
    }
    template<template<typename _T_> class _Op_>
    inline point binop(double _v, const point& _pt2)
    {
      using op = _Op_<double>;
      return point(op()(_v, _pt2.x()), op()(_v, _pt2.y()), op()(_v, _pt2.z()));
    }
    template<typename _T1_, typename _T2_>
    inline point add(const _T1_& _pt1, const _T2_& _pt2)
    {
      return binop<std::plus>(_pt1, _pt2);
    }
    template<typename _T1_, typename _T2_>
    inline point sub(const _T1_& _pt1, const _T2_& _pt2)
    {
      return binop<std::minus>(_pt1, _pt2);
    }
    template<typename _T1_, typename _T2_>
    inline point mul(const _T1_& _pt1, const _T2_& _pt2)
    {
      return binop<std::multiplies>(_pt1, _pt2);
    }
    inline double norm(const point& _pt1)
    {
      if(std::isnan(_pt1.z()))
      {
        return std::sqrt(_pt1.x() * _pt1.x() + _pt1.y() * _pt1.y());
      }
      else
      {
        return std::sqrt(_pt1.x() * _pt1.x() + _pt1.y() * _pt1.y() + _pt1.z() * _pt1.z());
      }
    }
    inline point normalized(const point& _pt1) { return mul(1.0 / norm(_pt1), _pt1); }
    inline double dot(const point& _pt1, const point& _pt2)
    {
      return _pt1.x() * _pt2.x() + _pt1.y() * _pt2.y();
    }
    inline double angle(const euclid::simple::point& _p1, const euclid::simple::point& _p2,
                        const euclid::simple::point& _p3)
    {
      const double u1 = _p2.x() - _p1.x();
      const double v1 = _p2.y() - _p1.y();
      const double u2 = _p2.x() - _p3.x();
      const double v2 = _p2.y() - _p3.y();
      const double dotproduct = (u1 * u2 + v1 * v2);
      const double determinant = (u1 * v2 - v1 * u2);
      return std::atan2(determinant, dotproduct);
    }
  } // namespace __internal__
  /**
   * Compute the distance between two points. If z is nan in one of the point, and not in an other
   * return NAN.
   */
  template<std::size_t _rank_>
  inline double distance(const point& _p1, const point& _p2)
  {
    auto [x1, y1, z1] = _p1;
    auto [x2, y2, z2] = _p2;

    double dx = x1 - x2;
    double dy = y1 - y2;

    if(std::isnan(z1) != std::isnan(z2))
    {
      return NAN;
    }
    else if(std::isnan(z1))
    {
      return std::pow(std::pow(dx, _rank_) + std::pow(dy, _rank_), 1.0 / _rank_);
    }
    else
    {
      double dz = z1 - z2;
      return std::pow(std::pow(dx, _rank_) + std::pow(dy, _rank_) + std::pow(dz, _rank_),
                      1.0 / _rank_);
    }
  }
  template<>
  inline double distance<1>(const point& _p1, const point& _p2)
  {
    auto [x1, y1, z1] = _p1;
    auto [x2, y2, z2] = _p2;

    double dx = x1 - x2;
    double dy = y1 - y2;

    if(std::isnan(z1) != std::isnan(z2))
    {
      return NAN;
    }
    else if(std::isnan(z1))
    {
      return std::abs(dx) + std::abs(dy);
    }
    else
    {
      double dz = z1 - z2;
      return std::abs(dx) + std::abs(dy) + std::abs(dz);
    }
  }
  template<>
  inline double distance<2>(const point& _p1, const point& _p2)
  {
    auto [x1, y1, z1] = _p1;
    auto [x2, y2, z2] = _p2;

    double dx = x1 - x2;
    double dy = y1 - y2;

    if(std::isnan(z1) != std::isnan(z2))
    {
      return NAN;
    }
    else if(std::isnan(z1))
    {
      return std::sqrt(dx * dx + dy * dy);
    }
    else
    {
      double dz = z1 - z2;
      return std::sqrt(dx * dx + dy * dy + dz * dz);
    }
  }
  inline point set_z(const point& _pt, double _z) { return point(_pt.x(), _pt.y(), _z); }
} // namespace euclid::simple
