#ifndef EUCLID_SIMPLE_HEADER
#include "point.h"
#endif

namespace euclid::simple
{
  /**
   * Represent a circle (or a sphere).
   */
  class circle : public std::tuple<point, double>
  {
    using __tuple__ = std::tuple<point, double>;
  public:
    using __tuple__::__tuple__;
    circle() : circle({NAN, NAN}, NAN) {}
    point center() const { return std::get<0>(*this); }
    double radius() const { return std::get<1>(*this); }
    inline bool operator==(const __tuple__& _rhs) const;
  };
  inline std::ostream& operator<<(std::ostream& os, euclid::simple::circle const& value)
  {
    os << "(" << value.center() << ", " << value.radius() << ")";
    return os;
  }

  bool circle::operator==(const __tuple__& _rhs) const
  {
    auto [pt, r] = _rhs;
    return point() == pt and radius() == r;
  }
  using sphere = circle;

  inline double circumference(const circle& _circle) { return 2 * M_PI * _circle.radius(); }
  inline double area(const circle& _circle) { return M_PI * _circle.radius() * _circle.radius(); }
} // namespace euclid::simple
