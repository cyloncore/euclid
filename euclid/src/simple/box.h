#ifndef EUCLID_SIMPLE_HEADER
#include "point.h"
#endif

namespace euclid::simple
{
  class box : public std::tuple<point, point>
  {
  public:
    box() {}
    box(const point& _p1, const point& _p2) : std::tuple<point, point>(_p1, _p2) {}
    point& top_left() { return std::get<0>(*this); }
    point& bottom_right() { return std::get<1>(*this); }
    double left() const { return top_left().x(); }
    double right() const { return bottom_right().x(); }
    double top() const { return top_left().y(); }
    double bottom() const { return bottom_right().y(); }
    const point& top_left() const { return std::get<0>(*this); }
    const point& bottom_right() const { return std::get<1>(*this); }
    double width() const { return bottom_right().x() - top_left().x(); }
    double height() const { return bottom_right().y() - top_left().y(); }
  };
} // namespace euclid::simple

namespace std
{
  template<>
  struct tuple_size<euclid::simple::box> : std::integral_constant<size_t, 2>
  {
  };

  template<std::size_t _index_>
  struct tuple_element<_index_, euclid::simple::box>
  {
    using type = euclid::simple::point;
  };
} // namespace std
