#ifndef EUCLID_SIMPLE_HEADER
#include "point.h"
#endif

namespace euclid::simple
{
  class segment : public std::tuple<point, point>
  {
  public:
    segment() {}
    segment(const point& _p1, const point& _p2) : std::tuple<point, point>(_p1, _p2) {}
    const point& first() const { return std::get<0>(*this); }
    const point& second() const { return std::get<1>(*this); }
  };

  inline std::ostream& operator<<(std::ostream& os, euclid::simple::segment const& value)
  {
    os << "[" << value.first() << ", " << value.second() << "]";
    return os;
  }
} // namespace euclid::simple

namespace std
{
  template<>
  struct tuple_size<euclid::simple::segment> : std::integral_constant<size_t, 2>
  {
  };

  template<std::size_t _index_>
  struct tuple_element<_index_, euclid::simple::segment>
  {
    using type = euclid::simple::point;
  };
} // namespace std

namespace euclid::simple
{
  inline double length(const segment& _seg) { return distance<2>(_seg.first(), _seg.second()); }
  namespace details
  {
    /**
     * @internal
     * Check if a point \p q which is colinerar with \p _seg is inside the segment boundary, given
     * \p _tolerance.
     */
    inline bool onSegment(const segment& _seg, const point& q, double _tolerance)
    {
      point p = _seg.first();
      point r = _seg.second();

      // Quick check
      return q.x() <= (std::max(p.x(), r.x()) + _tolerance)
             and q.x() >= (std::min(p.x(), r.x()) - _tolerance)
             and q.y() <= (std::max(p.y(), r.y()) + _tolerance)
             and q.y() >= (std::min(p.y(), r.y()) - _tolerance);
    }
    inline double area(const euclid::simple::point& p1, const euclid::simple::point& p2,
                       const euclid::simple::point& p3)
    {
      return p2.x() * (p3.y() - p1.y());
    }
  } // namespace details
  /**
   * @ingroup euclid_segment
   *
   * Check if two segment overlaps.
   */
  inline bool overlaps(const segment& _seg_1, const segment& _seg_2, double _tolerance = 1e-2)
  {
    // check if the four points are colinear by checking if the area of the polygon is null
    point v1 = _seg_1.first();
    point v2 = _seg_1.second();
    point v3 = _seg_2.first();
    point v4 = _seg_2.second();

    double adj_tolerance = _tolerance * std::max(distance<2>(v1, v2), distance<2>(v3, v4));

    double d = details::area(v1, v2, v3) + details::area(v2, v3, v4) + details::area(v3, v4, v1)
               + details::area(v4, v1, v2) + details::area(v2, v1, v3) + details::area(v1, v3, v4)
               + details::area(v3, v4, v2) + details::area(v4, v2, v1);

    if(std::abs(d) > adj_tolerance)
      return false;

    int required_count = 2;

    if(distance<1>(v1, v3) < adj_tolerance)
      ++required_count;
    if(distance<1>(v1, v4) < adj_tolerance)
      ++required_count;
    if(distance<1>(v2, v3) < adj_tolerance)
      ++required_count;
    if(distance<1>(v2, v4) < adj_tolerance)
      ++required_count;

    int count = 0;
    if(details::onSegment(_seg_2, v1, 1e-8))
      ++count;
    if(details::onSegment(_seg_2, v2, 1e-8))
      ++count;
    if(details::onSegment(_seg_1, v3, 1e-8))
      ++count;
    if(details::onSegment(_seg_1, v4, 1e-8))
      ++count;

    return count >= required_count;
  }
  /**
   * @ingroup euclid_segment
   *
   * Check if two segment intersects.
   */
  inline bool intersection(const segment& _seg_1, const segment& _seg_2, point* _pt = nullptr,
                           double _tolerance = 1e-6)
  {
    point v1 = _seg_1.first();
    double x1 = v1.x();
    double y1 = v1.y();
    point v2 = _seg_1.second();
    double x2 = v2.x();
    double y2 = v2.y();
    point v3 = _seg_2.first();
    double x3 = v3.x();
    double y3 = v3.y();
    point v4 = _seg_2.second();
    double x4 = v4.x();
    double y4 = v4.y();

    double d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
    if(std::abs(d) < _tolerance)
      return false;

    const double x1y2my1x2 = (x1 * y2 - y1 * x2);
    const double x3y4my3x4 = (x3 * y4 - y3 * x4);
    const double x_p = (x1y2my1x2 * (x3 - x4) - (x1 - x2) * x3y4my3x4) / d;
    const double y_p = (x1y2my1x2 * (y3 - y4) - (y1 - y2) * x3y4my3x4) / d;
    point pt(
      x_p, y_p,
      0.25 * (_seg_1.first().z() + _seg_1.second().z() + _seg_2.first().z() + _seg_2.second().z()));
    if(_pt)
      *_pt = pt;
    return details::onSegment(_seg_1, pt, 1e-8) and details::onSegment(_seg_2, pt, 1e-8);
  }
  inline segment flip(const segment& _seg) { return segment(_seg.second(), _seg.first()); }
} // namespace euclid::simple
