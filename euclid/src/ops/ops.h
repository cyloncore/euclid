namespace euclid::ops
{
#define EUCLID_OPS_BINARY_OP(_NAME_, _RET_)                                                        \
  template<class _T1_, class _T2_>                                                                 \
  _RET_ _NAME_(const _T1_& _t1, const _T2_& _t2)                                                   \
  {                                                                                                \
    static_assert(std::is_same_v<typename _T1_::system, typename _T2_::system>);                   \
    return _T1_::interface::_NAME_(_t1, _t2);                                                      \
  }

#define EUCLID_OPS_BINARY_TOL_OP(_NAME_, _RET_)                                                    \
  template<class _T1_, class _T2_>                                                                 \
  _RET_ _NAME_(const _T1_& _t1, const _T2_& _t2, double _tol)                                      \
  {                                                                                                \
    static_assert(std::is_same_v<typename _T1_::system, typename _T2_::system>);                   \
    return _T1_::interface::_NAME_(_t1, _t2, _tol);                                                \
  }

#define EUCLID_OPS_UNARY_OP(_NAME_, _RET_)                                                         \
  template<class _T1_>                                                                             \
  _RET_ _NAME_(const _T1_& _t1)                                                                    \
  {                                                                                                \
    return _T1_::interface::_NAME_(_t1);                                                           \
  }

#define EUCLID_OPS_UNARY_TOL_OP(_NAME_, _RET_)                                                     \
  template<class _T1_>                                                                             \
  _RET_ _NAME_(const _T1_& _t1, double _tol)                                                       \
  {                                                                                                \
    return _T1_::interface::_NAME_(_t1, _tol);                                                     \
  }

  EUCLID_OPS_BINARY_OP(difference, euclid::geometry::geometry<typename _T1_::system>)
  EUCLID_OPS_BINARY_OP(intersection, euclid::geometry::geometry<typename _T1_::system>)
  EUCLID_OPS_BINARY_OP(union_, euclid::geometry::geometry<typename _T1_::system>)
  /**
   * @return true if no point of geography_2 is outside geography_1, and the interiors intersect;
   * returns false otherwise.
   */
  EUCLID_OPS_BINARY_OP(contains, bool)
  EUCLID_OPS_BINARY_OP(distance, double)
  EUCLID_OPS_BINARY_OP(equals, bool)
  EUCLID_OPS_BINARY_OP(intersects, bool)
  EUCLID_OPS_BINARY_OP(overlaps, bool)
  EUCLID_OPS_BINARY_OP(touches, bool)
  EUCLID_OPS_BINARY_OP(disjoint, bool)
  /**
   * @return true if no point of geography_1 is outside geography_2, and the interiors intersect;
   * returns false otherwise.
   */
  EUCLID_OPS_BINARY_OP(within, bool)

  EUCLID_OPS_BINARY_TOL_OP(near_equals, bool)

  EUCLID_OPS_UNARY_OP(area, double)
  EUCLID_OPS_UNARY_OP(length, double)
  EUCLID_OPS_UNARY_OP(is_valid, bool)
  EUCLID_OPS_UNARY_OP(envelope, euclid::simple::box)
  /**
   * @return the centroid of a geometry.
   */
  EUCLID_OPS_UNARY_OP(centroid, euclid::geometry::point<typename _T1_::system>)
  /**
   * Return the maximum circle which is enclosed by the geometry.
   */
  EUCLID_OPS_UNARY_TOL_OP(maximum_inscribed_circle, simple::circle)
  /**
   * Return the minimal circle which encloses the geometry.
   */
  EUCLID_OPS_UNARY_OP(minimum_enclosing_circle, simple::circle)
  /**
   * Return the minimal circle which encloses the geometry.
   */
  EUCLID_OPS_UNARY_OP(minimum_rotated_rectangle, euclid::geometry::polygon<typename _T1_::system>)

  EUCLID_OPS_UNARY_TOL_OP(delaunay_triangulation, typename _T1_::system::multi_geometry)

#undef EUCLID_OPS_BINARY_OP
#undef EUCLID_OPS_BINARY_TOL_OP
#undef EUCLID_OPS_UNARY_OP
#undef EUCLID_OPS_UNARY_TOL_OP

  template<class _T1_>
  euclid::geometry::geometry<typename _T1_::system> offsetting(const _T1_& _t1, double _amount)
  {
    return _T1_::interface::offsetting(_t1, _amount);
  }

  template<typename _system_>
  euclid::geometry::multi_geometry<_system_>
    voronoi_diagram(const euclid::geometry::multi_point<_system_>& _source,
                    const euclid::geometry::geometry<_system_>& _envelope, double _tol)
  {
    return _system_::interface::voronoi_diagram(_source, _envelope, _tol);
  }

  template<class _T1_>
  _T1_ simplify(const _T1_& _source, double _tol)
  {
    return _T1_::system::interface::simplify(_source, _tol);
  }
} // namespace euclid::ops
