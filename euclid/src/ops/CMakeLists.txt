install( FILES
  ops.h
  DESTINATION ${INSTALL_INCLUDE_DIR}/euclid/src/ops
  ${EUCLID_INSTALL_PERMISSIONS})

add_subdirectory(impl)
