namespace euclid::ops::impl
{
  namespace details
  {
    template<typename _T_, typename _Enabled_ = void>
    struct envelope;
  }
  template<class _T1_>
  euclid::simple::box envelope(const _T1_& _t1)
  {
    return details::envelope<_T1_>::compute(_t1);
  }

  namespace details
  {
    template<typename _I_>
    struct envelope<euclid::geometry::geometry<_I_>>
    {
      static inline euclid::simple::box compute(const euclid::geometry::geometry<_I_>& _t1)
      {
        using interface = _I_;
        using polygon = euclid::geometry::polygon<interface>;
        using line_string = euclid::geometry::line_string<interface>;
        using linear_ring = euclid::geometry::linear_ring<interface>;
        using point = euclid::geometry::point<interface>;
        if(_t1.template is<polygon>())
        {
          return impl::envelope(_t1.template cast<polygon>().exterior_ring());
        }
        else if(_t1.template is<line_string>())
        {
          return impl::envelope(_t1.template cast<line_string>());
        }
        else if(_t1.template is<linear_ring>())
        {
          return impl::envelope(_t1.template cast<linear_ring>());
        }
        else if(_t1.template is<point>())
        {
          return impl::envelope(_t1.template cast<point>());
        }
        std::abort(); // TODO handle collections
      }
    };
    template<template<typename I> class _T_, typename _I_>
      requires(std::is_same_v<_T_<_I_>, euclid::geometry::line_string<_I_>>
               or std::is_same_v<_T_<_I_>, euclid::geometry::linear_ring<_I_>>)
    struct envelope<_T_<_I_>>
    {
      static inline euclid::simple::box compute(const _T_<_I_>& _t1)
      {
        auto it = _t1.points().begin();
        euclid::simple::box r;
        r.top_left() = *it;
        r.bottom_right() = *it;
        ++it;
        for(; it != _t1.points().end(); ++it)
        {
          euclid::simple::point pt = *it;
          if(pt.x() < r.top_left().x())
            r.top_left().x() = pt.x();
          if(pt.y() < r.top_left().y())
            r.top_left().y() = pt.y();
          if(pt.z() < r.top_left().z())
            r.top_left().z() = pt.z();
          if(pt.x() > r.bottom_right().x())
            r.bottom_right().x() = pt.x();
          if(pt.y() > r.bottom_right().y())
            r.bottom_right().y() = pt.y();
          if(pt.z() > r.bottom_right().z())
            r.bottom_right().z() = pt.z();
        }
        return r;
      }
    };
    template<typename _I_>
    struct envelope<euclid::geometry::point<_I_>>
    {
      static inline euclid::simple::box compute(const euclid::geometry::point<_I_>& _t1)
      {
        euclid::simple::point p1(_t1);
        return euclid::simple::box(p1, p1);
      }
    };
  } // namespace details
} // namespace euclid::ops::impl
