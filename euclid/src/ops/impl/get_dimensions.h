#pragma once

#ifndef EUCLID_EXTRA_HEADER

#include <euclid/geometry>

#endif

namespace euclid::ops::impl
{

  class get_dimensions : public euclid::operation::operation<get_dimensions, std::size_t>
  {
    inline std::size_t get_dimensions_detail(const euclid::simple::point& _pt)
    {
      return _pt.dimensions();
    }
    template<std::size_t skip, typename _system_, template<typename> class _geometry_>
    inline std::size_t get_dimensions_detail(const _geometry_<_system_>& _geometry)
    {
      std::size_t dim = 0;
      for(auto it = _geometry.points().begin(); it != std::prev(_geometry.points().end(), skip);
          ++it)
      {
        dim = std::max<std::size_t>(dim, get_dimensions(*it));
        if(dim == 3)
          return dim;
      }
      return dim;
    }
  public:
    using euclid::operation::operation<get_dimensions, std::size_t>::operation;
    // BEGIN simple point
    inline std::size_t operator()(const euclid::simple::point& _point)
    {
      return get_dimensions_detail(_point);
    }
    // END simpole point
    // BEGIN point
    template<typename _system_>
    inline std::size_t operator()(const geometry::point<_system_>& _point)
    {
      return get_dimensions_detail(_point);
    }
    // END point
    // BEGIN line_string
    template<typename _system_>
    inline std::size_t operator()(const geometry::line_string<_system_>& _ring)
    {
      return get_dimensions_detail<0>(_ring);
    }
    // END line_string
    // BEGIN linear_ring
    template<typename _system_>
    inline std::size_t operator()(const geometry::linear_ring<_system_>& _ring)
    {
      return get_dimensions_detail<1>(_ring);
    }
    // END linear_ring
    // BEGIN polygon
    template<typename _system_>
    inline std::size_t operator()(const geometry::polygon<_system_>& _polygon)
    {
      std::size_t dim = get_dimensions_detail<1>(_polygon.exterior_ring());
      for(const geometry::linear_ring<_system_>& ring : _polygon.interior_rings())
      {
        dim = std::max(dim, get_dimensions_detail<1>(ring));
        if(dim == 3)
          return dim;
      }
      return dim;
    }
    // END polygon
    // BEGIN polygon
    template<typename _system_, template<typename> class _geometry_t_>
    inline std::size_t
      operator()(const geometry::details::collection<_geometry_t_<_system_>>& _collection)
    {
      std::size_t dim = 0;
      for(std::size_t i = 0; i < _collection.size(); ++i)
      {
        dim = std::max<std::size_t>(dim, get_dimensions(_collection.at(i)));
        if(dim == 3)
          return dim;
      }
      return dim;
    }
    // END polygon
    template<typename _T_>
    return_type operator()(const _T_& _t)
    {
      return euclid::ops::impl::get_dimensions(_t);
    }
    // BEGIN geometry
    template<typename _system_>
    inline std::size_t operator()(const geometry::geometry<_system_>& _geometry)
    {
      return euclid::operation::dispatch<get_dimensions>()(_geometry);
    }
    // END geometry
  };
} // namespace euclid::ops::impl
