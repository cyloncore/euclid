#ifndef EUCLID_CONVERT_HEADER
#include <euclid/geometry>
#endif

namespace euclid
{
  namespace details::convert
  {
    template<std::size_t skip, typename _Builder_, typename _GeometryT_>
    _Builder_& add_points(_Builder_& _builder, const _GeometryT_& _geometry)
    {
      for(auto it = _geometry.points().begin(); it != std::prev(_geometry.points().end(), skip);
          ++it)
      {
        _builder.add_point(*it);
      }
      return _builder;
    }
  } // namespace details::convert
  // BEGIN point
  template<typename _destination_system_, typename _source_system_>
  typename _destination_system_::point convert(const geometry::point<_source_system_>& _point)
  {
    return typename _destination_system_::point(_point.x(), _point.y(), _point.z());
  }
  // END point
  // BEGIN line_string
  template<typename _destination_system_, typename _source_system_>
  typename _destination_system_::line_string
    convert(const geometry::line_string<_source_system_>& _ring)
  {
    typename _destination_system_::line_string::builder builder;
    return details::convert::add_points<0>(builder, _ring).create();
  }
  // END line_string
  // BEGIN linear_ring
  template<typename _destination_system_, typename _source_system_>
  typename _destination_system_::linear_ring
    convert(const geometry::linear_ring<_source_system_>& _ring)
  {
    typename _destination_system_::linear_ring::builder builder;
    return details::convert::add_points<1>(builder, _ring).create();
  }
  // END linear_ring
  // BEGIN polygon
  template<typename _destination_system_, typename _source_system_>
  typename _destination_system_::polygon convert(const geometry::polygon<_source_system_>& _polygon)
  {
    typename _destination_system_::polygon::builder builder;
    details::convert::add_points<1>(builder, _polygon.exterior_ring());
    for(const geometry::linear_ring<_source_system_>& ring : _polygon.interior_rings())
    {
      builder.start_interior_ring();
      details::convert::add_points<1>(builder, ring);
    }
    return builder.create();
  }
  // END polygon
  // BEGIN polygon
  template<typename _destination_system_, typename _source_system_,
           template<typename> class _geometry_t_>
  geometry::details::collection<_geometry_t_<_destination_system_>>
    convert(const geometry::details::collection<_geometry_t_<_source_system_>>& _collection)
  {
    typename geometry::details::collection<_geometry_t_<_destination_system_>>::builder builder;
    for(std::size_t i = 0; i < _collection.size(); ++i)
    {
      builder.add_element(convert<_destination_system_>(_collection.at(i)));
    }
    return builder.create();
  }
  // END polygon
  // BEGIN geometry
  namespace details::convert
  {
    template<typename _to_system_, typename _from_system_>
    class convert_operation
        : public euclid::operation::operation<convert_operation<_to_system_, _from_system_>,
                                              typename _to_system_::geometry>
    {
    public:
      using return_type = typename _to_system_::geometry;
      using euclid::operation::operation<convert_operation<_to_system_, _from_system_>,
                                         return_type>::operation;

      template<typename _T_>
      return_type operator()(const _T_& _t)
      {
        return euclid::convert<_to_system_>(_t);
      }
    };
  } // namespace details::convert
  template<typename _destination_system_, typename _source_system_>
  typename _destination_system_::geometry
    convert(const geometry::geometry<_source_system_>& _geometry)
  {
    using convert_operation
      = details::convert::convert_operation<_destination_system_, _source_system_>;
    return operation::dispatch<convert_operation>()(_geometry);
  }
  // END geometry
} // namespace euclid
