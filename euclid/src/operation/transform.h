#ifndef EUCLID_GEOMETRY_HEADER
#include <euclid/geometry>
#endif

namespace euclid::operation
{
  /**
   * @ingroup euclid_operation
   * Base class for transformations.
   * The class is designed so that operation can be use as classes or like a function.
   */
  template<typename _system_, typename _Op_>
  class transform
  {
  public:
    transform() {}
    template<typename... _TArgs>
    transform(const _TArgs&... _args) : m_ret(_Op_()(_args...))
    {
    }
    template<template<typename> class _T_>
    operator _T_<_system_>() const
    {
      return m_ret.template cast<_T_<_system_>>();
    }
  private:
    geometry::geometry<_system_> m_ret;
  };
} // namespace euclid::operation
