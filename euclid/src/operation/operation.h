#ifndef EUCLID_GEOMETRY_HEADER
#include <euclid/src/utils.h>
#include <memory>
#endif

namespace euclid::operation
{
  /**
   * @ingroup euclid_operation
   * Base class for operations.
   * The class is designed so that operation can be use as classes or like a function.
   */
  template<typename _Op_, typename _TRet_>
  class operation
  {
  public:
    using return_type = _TRet_;
  public:
    operation() {}
    template<typename... _TArgs>
    operation(const _TArgs&... _args) : m_ret(_Op_()(_args...))
    {
    }
    operator const _TRet_&() const { return *m_ret; }
#define EUCLID_OP_OP(_OP_)                                                                         \
  template<typename _T_>                                                                           \
  bool operator _OP_(const _T_& _value) const                                                      \
  {                                                                                                \
    return *m_ret _OP_ _value;                                                                     \
  }
    EUCLID_OP_OP(==)
    EUCLID_OP_OP(!=)
    EUCLID_OP_OP(<)
    EUCLID_OP_OP(<=)
    EUCLID_OP_OP(>)
    EUCLID_OP_OP(>=)
    EUCLID_OP_OP(&&)
    EUCLID_OP_OP(||)
#undef EUCLID_OP_OP
  private:
    std::optional<_TRet_> m_ret;
  };
} // namespace euclid::operation
