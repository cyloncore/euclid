#ifndef EUCLID_GEOMETRY_HEADER
#include "operation.h"
#include <euclid/geometry>
#endif

namespace euclid::operation
{
  namespace details
  {
    template<template<typename> class T, template<typename> class U>
    struct is_same : std::false_type
    {
    };

    template<template<typename> class T>
    struct is_same<T, T> : std::true_type
    {
    };

    template<template<typename> class T, template<typename> class U>
    inline constexpr bool is_same_v = is_same<T, U>::value;

    template<template<typename> class T, template<typename> class... Ts>
    struct contains : std::disjunction<is_same<T, Ts>...>
    {
      static_assert(sizeof...(Ts) > 0, "List is empty");
    };
    template<template<typename> class T, template<typename> class... Ts>
    inline constexpr bool contains_v = contains<T, Ts...>::value;

    template<typename _T_Op_, typename _T_ReturnType_, typename _enable_, typename _tuple_,
             typename... _TArgs_>
    struct Call;

    template<typename _T_Op_, typename _T_ReturnType_, typename _enable_, typename _tuple_,
             typename _CArg_, typename... _TArgs_>
    struct Call<_T_Op_, _T_ReturnType_, _enable_, _tuple_, _CArg_, _TArgs_...>
    {
      static _T_ReturnType_ call(const _tuple_& _tuple, const _CArg_& _arg,
                                 const _TArgs_&... _other_args)
      {
        auto nt = std::tuple_cat(_tuple, std::make_tuple(_arg));
        return Call<_T_Op_, _T_ReturnType_, _enable_, decltype(nt), _TArgs_...>::call(
          nt, _other_args...);
      }
    };
    template<typename _T_Op_, typename _T_ReturnType_, typename _enable_, typename _tuple_,
             typename _system_, typename... _TArgs_>
    struct Call<_T_Op_, _T_ReturnType_, _enable_, _tuple_, euclid::geometry::geometry<_system_>,
                _TArgs_...>
    {
      static _T_ReturnType_ call(const _tuple_& _tuple,
                                 const euclid::geometry::geometry<_system_>& _geometry,
                                 const _TArgs_&... _other_args)
      {
        using system = _system_;

        using point = typename system::point;
        using line_string = typename system::line_string;
        using linear_ring = typename system::linear_ring;
        using polygon = typename system::polygon;
        using multi_geometry = typename system::multi_geometry;
        using multi_point = typename system::multi_point;
        using multi_line_string = typename system::multi_line_string;
        using multi_polygon = typename system::multi_polygon;

        if(_geometry.template is<multi_polygon>())
        {
          if constexpr(_enable_::multi_polygon)
          {
            auto nt
              = std::tuple_cat(_tuple, std::make_tuple(_geometry.template cast<multi_polygon>()));
            return Call<_T_Op_, _T_ReturnType_, _enable_, decltype(nt), _TArgs_...>::call(
              nt, _other_args...);
          }
          else
          {
            euclid_fatal("Operation not supported on multi_polygon: {}", _geometry);
          }
        }
        if(_geometry.template is<multi_line_string>())
        {
          if constexpr(_enable_::multi_line_string)
          {
            auto nt
              = std::tuple_cat(_tuple, std::make_tuple(_geometry.template cast<multi_polygon>()));
            return Call<_T_Op_, _T_ReturnType_, _enable_, decltype(nt), _TArgs_...>::call(
              nt, _other_args...);
          }
          else
          {
            euclid_fatal("Operation not supported on multi_line_string: {}", _geometry);
          }
        }
        if(_geometry.template is<multi_point>())
        {
          if constexpr(_enable_::multi_point)
          {
            auto nt
              = std::tuple_cat(_tuple, std::make_tuple(_geometry.template cast<multi_point>()));
            return Call<_T_Op_, _T_ReturnType_, _enable_, decltype(nt), _TArgs_...>::call(
              nt, _other_args...);
          }
          else
          {
            euclid_fatal("Operation not supported on multi_point: {}", _geometry);
          }
        }
        if(_geometry.template is<multi_geometry>())
        {
          if constexpr(_enable_::multi_geometry)
          {
            auto nt
              = std::tuple_cat(_tuple, std::make_tuple(_geometry.template cast<multi_geometry>()));
            return Call<_T_Op_, _T_ReturnType_, _enable_, decltype(nt), _TArgs_...>::call(
              nt, _other_args...);
          }
          else
          {
            euclid_fatal("Operation not supported on multi_geometry: {}", _geometry);
          }
        }
        if(_geometry.template is<polygon>())
        {
          if constexpr(_enable_::polygon)
          {
            auto nt = std::tuple_cat(_tuple, std::make_tuple(_geometry.template cast<polygon>()));
            return Call<_T_Op_, _T_ReturnType_, _enable_, decltype(nt), _TArgs_...>::call(
              nt, _other_args...);
          }
          else
          {
            euclid_fatal("Operation not supported on polygon: {}", _geometry);
          }
        }
        if(_geometry.template is<linear_ring>())
        {
          if constexpr(_enable_::linear_ring)
          {
            auto nt
              = std::tuple_cat(_tuple, std::make_tuple(_geometry.template cast<linear_ring>()));
            return Call<_T_Op_, _T_ReturnType_, _enable_, decltype(nt), _TArgs_...>::call(
              nt, _other_args...);
          }
          else
          {
            euclid_fatal("Operation not supported on linear_ring: {}", _geometry);
          }
        }
        if(_geometry.template is<line_string>())
        {
          if constexpr(_enable_::line_string)
          {
            auto nt
              = std::tuple_cat(_tuple, std::make_tuple(_geometry.template cast<line_string>()));
            return Call<_T_Op_, _T_ReturnType_, _enable_, decltype(nt), _TArgs_...>::call(
              nt, _other_args...);
          }
          else
          {
            euclid_fatal("Operation not supported on line_string: {}", _geometry);
          }
        }
        if(_geometry.template is<point>())
        {
          if constexpr(_enable_::point)
          {
            auto nt = std::tuple_cat(_tuple, std::make_tuple(_geometry.template cast<point>()));
            return Call<_T_Op_, _T_ReturnType_, _enable_, decltype(nt), _TArgs_...>::call(
              nt, _other_args...);
          }
          else
          {
            euclid_fatal("Operation not supported on point: {}", _geometry);
          }
        }
        std::abort();
      }
    };
    template<typename _T_Op_, typename _T_ReturnType_, typename _enable_, typename _tuple_>
    struct Call<_T_Op_, _T_ReturnType_, _enable_, _tuple_>
    {
      static _T_ReturnType_ call(const _tuple_& _tuple)
      {
        return (_T_ReturnType_)std::make_from_tuple<_T_Op_>(_tuple);
      }
    };

  } // namespace details

  /**
   * @ingroup euclid_operation
   * allow to specifiy for which type of geometries the dispatch is supported
   */
  template<template<typename> class... _T_>
  struct enable_for
  {
    static constexpr bool point = details::contains_v<euclid::geometry::point, _T_...>;
    static constexpr bool line_string = details::contains_v<euclid::geometry::line_string, _T_...>;
    static constexpr bool linear_ring = details::contains_v<euclid::geometry::linear_ring, _T_...>;
    static constexpr bool polygon = details::contains_v<euclid::geometry::polygon, _T_...>;
    static constexpr bool multi_geometry
      = details::contains_v<euclid::geometry::multi_geometry, _T_...>;
    static constexpr bool multi_point = details::contains_v<euclid::geometry::multi_point, _T_...>;
    static constexpr bool multi_line_string
      = details::contains_v<euclid::geometry::multi_line_string, _T_...>;
    static constexpr bool multi_polygon
      = details::contains_v<euclid::geometry::multi_polygon, _T_...>;
  };

  /**
   * @ingroup euclid_operation
   * enable a dispatch operation for all type of geometries
   */
  struct enable_for_all
      : public enable_for<euclid::geometry::point, euclid::geometry::line_string,
                          euclid::geometry::linear_ring, euclid::geometry::polygon,
                          euclid::geometry::multi_geometry, euclid::geometry::multi_point,
                          euclid::geometry::multi_line_string, euclid::geometry::multi_polygon>
  {
  };
  /**
   * @ingroup euclid_operation
   * enable a dispatch operation for all type of collections
   */
  struct enable_for_all_collections
      : public enable_for<euclid::geometry::multi_geometry, euclid::geometry::multi_point,
                          euclid::geometry::multi_line_string, euclid::geometry::multi_polygon>
  {
  };

  /**
   * @ingroup euclid_operation
   * This class allows to dispatch a generic \ref euclid::geometry::geomtry to the
   * specific implementation of an operation (i.e. the implementation for a
   * \ref euclid:geometry::point, \ref euclid:geometry::line_string ...)
   */
  template<typename _T_Op_, typename _enable_ = enable_for_all,
           typename _T_ReturnType_ = typename _T_Op_::return_type>
  class dispatch : public operation<dispatch<_T_Op_, _enable_, _T_ReturnType_>, _T_ReturnType_>
  {
    using return_type = _T_ReturnType_;
    using operation_base = operation<dispatch<_T_Op_, _enable_, _T_ReturnType_>, return_type>;
  public:
    using operation_base::operation_base;

    template<typename... _TArgs>
    return_type operator()(const _TArgs&... _args)
    {
      return details::Call<_T_Op_, return_type, _enable_, std::tuple<>, _TArgs...>::call(
        std::tuple<>(), _args...);
    }
  };

#if 0
  template<typename _T_Op_, typename _enable_, typename _T_ReturnType_ >
  class dispatch_transform : public operation<dispatch_transform<_T_Op_, _enable_>, _T_ReturnType_ >
  {
    using return_type = _T_ReturnType_;
    using operation_base = operation<dispatch_transform<_T_Op_, _enable_>, return_type >;

  public:
    using operation_base::operation_base;

    template<typename... _TArgs>
    return_type operator()(const _TArgs&... _args)
    {
      return details::Call<_T_Op_, _enable_, std::tuple<>, _TArgs...>::call(std::tuple<>(), _args...);
    }
  };
#endif
} // namespace euclid::operation
