#pragma once

#include <memory>

namespace euclid::internal::utils
{
  /**
   * @private
   * Create a std::shared_ptr which has dependencies that needs to be kept around until the
   * pointer is deleted.
   */
  template<bool _delete_, typename _T_, typename... _TDep_>
  inline std::shared_ptr<_T_> make_shared_from_ptr(_T_* _t, _TDep_... _dep)
  {
    std::tuple<_TDep_...> dep = std::make_tuple(_dep...);
    return std::shared_ptr<_T_>(_t,
                                [dep](_T_* _ptr)
                                {
                                  if(_delete_)
                                  {
                                    delete _ptr;
                                  }
                                  (void)dep; // no unused warnings
                                });
  }
} // namespace euclid::internal::utils
