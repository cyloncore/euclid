#pragma once
#include <memory>

#include <gdal/cpl_conv.h>
#include <gdal/ogr_geometry.h>

#include <euclid/geometry>
#include <euclid/io>

namespace euclid
{
  // BEGIN forward declaration and data types
  namespace details::gdal
  {
    struct interface;
    // BEGIN geometry data
    using geometry_data = std::shared_ptr<const OGRGeometry>;
    using point_data = std::shared_ptr<const OGRPoint>;
    using line_string_data = std::shared_ptr<const OGRLineString>;
    using linear_ring_data = std::shared_ptr<const OGRLinearRing>;
    using polygon_data = std::shared_ptr<const OGRPolygon>;
    // END geometry data

    // BEGIN collection data
    using multi_geometry_data = std::shared_ptr<const OGRGeometryCollection>;
    using multi_point_data = std::shared_ptr<const OGRMultiPoint>;
    using multi_line_string_data = std::shared_ptr<const OGRMultiLineString>;
    using multi_polygon_data = std::shared_ptr<const OGRMultiPolygon>;
    // END collection data

    template<typename _T_>
    struct traits;
  } // namespace details::gdal
  // END forward declaration and data types

  struct gdal : public euclid::geometry::system_base<gdal, details::gdal::interface>
  {
  };

  // BEGIN Interface with gdal objects
  template<template<typename> class _T_>
  const OGRGeometry* to_gdal(const _T_<euclid::gdal>& _t);
  template<typename _T_>
  _T_ temporary_from_gdal(const typename details::gdal::traits<_T_>::data::element_type* _value);
  // END Interface with gdal objects
  namespace details::gdal
  {
    using geometry = euclid::gdal::geometry;
    using point = euclid::gdal::point;
    using line_string = euclid::gdal::line_string;
    using linear_ring = euclid::gdal::linear_ring;
    using polygon = euclid::gdal::polygon;

    using multi_geometry = euclid::gdal::multi_geometry;
    using multi_point = euclid::gdal::multi_point;
    using multi_line_string = euclid::gdal::multi_line_string;
    using multi_polygon = euclid::gdal::multi_polygon;

    template<>
    struct traits<geometry>
    {
      using data = geometry_data;
      using collection_data = multi_geometry_data;
    };
    template<>
    struct traits<point>
    {
      using data = point_data;
      using collection_data = multi_point_data;
    };
    template<>
    struct traits<line_string>
    {
      using data = line_string_data;
      using collection_data = multi_line_string_data;
    };
    template<>
    struct traits<linear_ring>
    {
      using data = linear_ring_data;
    };
    template<>
    struct traits<polygon>
    {
      using data = polygon_data;
      using collection_data = multi_polygon_data;
    };
    template<>
    struct traits<multi_geometry>
    {
      using data = multi_geometry_data;
    };
    template<>
    struct traits<multi_point>
    {
      using data = multi_point_data;
    };
    template<>
    struct traits<multi_line_string>
    {
      using data = multi_line_string_data;
    };
    template<>
    struct traits<multi_polygon>
    {
      using data = multi_polygon_data;
    };
    template<typename _T_>
    struct traits<euclid::gdal::point_iterator<_T_>>
    {
      using data = typename traits<_T_>::data;
    };

    template<typename _CT_>
    struct container_data_to_containee_data;
    template<>
    struct container_data_to_containee_data<multi_point_data>
    {
      using type = point_data;
    };
    template<>
    struct container_data_to_containee_data<multi_line_string_data>
    {
      using type = line_string_data;
    };
    template<>
    struct container_data_to_containee_data<multi_polygon_data>
    {
      using type = polygon_data;
    };
    template<>
    struct container_data_to_containee_data<multi_geometry_data>
    {
      using type = multi_geometry_data;
    };

    template<typename _CT_>
    using container_data_to_containee_data_t =
      typename container_data_to_containee_data<_CT_>::type;

    struct interface
    {
      friend geometry;
      friend line_string;
      friend linear_ring;
      friend point;
      friend polygon;
      template<typename _T_>
      friend class euclid::geometry::details::points_helper;
      template<typename _T_>
      friend class euclid::geometry::point_iterator;
      friend class euclid::geometry::details::line_string_builder<euclid::gdal>;
      friend class euclid::geometry::details::linear_ring_builder<euclid::gdal>;
      friend class euclid::geometry::details::polygon_builder<euclid::gdal>;
      template<typename _T_>
      friend class euclid::geometry::details::collection_builder;
      template<typename _T_>
      friend class euclid::geometry::details::collection;

      template<typename _T_>
      using traits = traits<_T_>;
      using system = euclid::gdal;
      template<template<typename> class _T_>
      friend const OGRGeometry* euclid::to_gdal(const _T_<euclid::gdal>& _t);
      template<typename _T_>
      friend _T_ euclid::temporary_from_gdal(
        const typename details::gdal::traits<_T_>::data::element_type* _value);
    private:
      template<bool _close_, typename _T_>
      static inline void
        append_points_to(_T_ _container, const euclid::geometry::details::vector_of_points& _points,
                         std::size_t _dimensions);
      template<typename _TData_>
      static inline std::size_t get_dimensions(const _TData_& _data);
      template<template<typename> class _T_>
      static inline const OGRGeometry* to_gdal(const _T_<euclid::gdal>& _t)
      {
        return _t.m_data.get();
      }
      template<typename _T_, bool _delete_>
      static inline _T_
        from_gdal(const typename details::gdal::traits<_T_>::data::element_type* _value)
      {
        return _T_(euclid::internal::utils::make_shared_from_ptr<_delete_>(_value));
      }
      // Interface with euclid::geometry
      // BEGIN geometry interface
      template<typename _T_>
      static inline bool geometry_is(const geometry_data& _data_type);
      template<typename _T_>
      static inline typename traits<_T_>::data geometry_cast(const geometry_data& _data_type);
      // END geometry interface
      // BEGIN point interface
      static inline point_data create_point(double _x, double _y, double _z);
      static inline double point_get_x(point_data _data);
      static inline double point_get_y(point_data _data);
      static inline double point_get_z(point_data _data);
      // END point interface
      // BEGIN line string interface
      static inline line_string_data
        create_line_string(const euclid::geometry::details::vector_of_points& _points,
                           std::size_t _dimensions);
      // END line string interface
      // BEGIN linear ring interface
      static inline linear_ring_data
        create_linear_ring(const euclid::geometry::details::vector_of_points& _points,
                           std::size_t _dimensions);
      // END linear ring interface
      // BEGIN polygon interface
      static inline polygon_data create_polygon(
        const euclid::geometry::details::vector_of_points& _shell,
        const std::vector<const euclid::geometry::details::vector_of_points*>& _holes,
        std::size_t _dimensions);
      static inline polygon_data
        create_polygon(const linear_ring_data& _exterior_ring_data,
                       const std::vector<linear_ring_data>& _interior_ring_datas);
      static inline linear_ring_data polygon_get_exterior_ring(polygon_data _data);
      static inline std::vector<linear_ring_data> polygon_get_interior_rings(polygon_data _data);
      // END polygon interface
      // BEGIN collection interface
      template<typename _DT_>
      static inline std::size_t collection_size(const _DT_& _data);
      template<typename _CDT_>
      static inline container_data_to_containee_data_t<_CDT_> collection_at(const _CDT_& _data,
                                                                            std::size_t _index);
      static inline typename traits<euclid::gdal::point>::collection_data create_collection(
        const std::shared_ptr<euclid::geometry::details::vector_of_points>& _points);
      template<typename _T_>
      static inline typename traits<_T_>::collection_data
        create_collection(const std::vector<_T_>& _data);
      // END collection interface
      // BEGIN points
      template<typename _DT_>
      static inline std::size_t points_size(const _DT_& _data);
      template<typename _DT_>
      static inline euclid::simple::point points_at(const _DT_& _data, std::size_t _index);
      // END points
    public:
      // BEGIN IO
      template<class _T_>
      static inline std::string to_wkt(const _T_& _object, const euclid::io::wkt_options& _options);
      template<class _T_>
      static inline _T_ from_wkt(const std::string& _wkt);
      // END IO
      // BEGIN OPS
#define EUCLID_GDAL_OP_ASSERT(_T1_, _T2_)                                                          \
  static_assert(false and std::is_same_v<_T1_, _T2_>,                                              \
                "GDAL ops are not supported, they are usually implemented as"                      \
                "a slow conversion to an other library, use euclid to convert"                     \
                "to a different library!")
#define EUCLID_GDAL_BINARY_OP(_NAME_)                                                              \
  template<class _T1_, class _T2_>                                                                 \
  static inline geometry _NAME_(const _T1_& _object1, const _T2_& _object2)                        \
  {                                                                                                \
    EUCLID_GDAL_OP_ASSERT(_T1_, _T2_);                                                             \
    return geometry();                                                                             \
  }
#define EUCLID_GDAL_UNARY_OP(_NAME_, _RET_)                                                        \
  template<class _T_>                                                                              \
  static inline _RET_ _NAME_(const _T_& _object)                                                   \
  {                                                                                                \
    EUCLID_GDAL_OP_ASSERT(_T_, _T_);                                                               \
    return _RET_();                                                                                \
  }
      EUCLID_GDAL_BINARY_OP(intersection)
      EUCLID_GDAL_UNARY_OP(area, double)
      EUCLID_GDAL_UNARY_OP(is_valid, bool)
#undef EUCLID_GDAL_BINARY_OP
#undef EUCLID_GDAL_UNARY_OP
      // END OPS
    };

    // BEGIN interface implementation
    template<bool _close_, typename _T_>
    void interface::append_points_to(_T_ _container,
                                     const euclid::geometry::details::vector_of_points& _points,
                                     std::size_t _dimensions)
    {
      for(const euclid::simple::point& pt : _points)
      {
        switch(_dimensions)
        {
        case 2:
          _container->addPoint(pt.x(), pt.y());
          break;
        case 3:
          _container->addPoint(pt.x(), pt.y(), pt.z());
          break;
        default:
          euclid_fatal("Unsupported dimension: %{}", _dimensions);
        }
      }
      if(_close_)
      {
        const euclid::simple::point& pt = *_points.begin();
        switch(_dimensions)
        {
        case 2:
          _container->addPoint(pt.x(), pt.y());
          break;
        case 3:
          _container->addPoint(pt.x(), pt.y(), pt.z());
          break;
        default:
          euclid_fatal("Unsupported dimension: %{}", _dimensions);
        }
      }
    }
    template<typename _TData_>
    std::size_t interface::get_dimensions(const _TData_& _data)
    {
      return _data->getCoordinateDimension();
    }

    // BEGIN geometry interface
    template<typename _T_>
    bool interface::geometry_is(const geometry_data& _data_type)
    {
      return std::dynamic_pointer_cast<typename traits<_T_>::data::element_type>(_data_type)
             != nullptr;
    }
    template<typename _T_>
    typename traits<_T_>::data interface::geometry_cast(const geometry_data& _data_type)
    {
      return std::static_pointer_cast<typename traits<_T_>::data::element_type>(_data_type);
    }
    // END geometry interface
    // BEGIN point interface
    point_data interface::create_point(double _x, double _y, double _z)
    {
      return std::isnan(_z) ? std::make_shared<OGRPoint>(_x, _y)
                            : std::make_shared<OGRPoint>(_x, _y, _z);
    }
    double interface::point_get_x(point_data _data) { return _data->getX(); }
    double interface::point_get_y(point_data _data) { return _data->getY(); }
    double interface::point_get_z(point_data _data)
    {
      return _data->getCoordinateDimension() == 2 ? NAN : _data->getZ();
    }
    // END point interface
    // BEGIN line string interface
    line_string_data
      interface::create_line_string(const euclid::geometry::details::vector_of_points& _points,
                                    std::size_t _dimensions)
    {
      std::shared_ptr<OGRLineString> lsd = std::make_shared<OGRLineString>();
      append_points_to<false>(lsd, _points, _dimensions);
      return lsd;
    }
    // END line string interface
    // BEGIN linear ring interface
    linear_ring_data
      interface::create_linear_ring(const euclid::geometry::details::vector_of_points& _points,
                                    std::size_t _dimensions)
    {
      std::shared_ptr<OGRLinearRing> lrd = std::make_shared<OGRLinearRing>();
      append_points_to<true>(lrd, _points, _dimensions);
      return lrd;
    }
    // END linear ring interface
    // BEGIN polygon interface
    polygon_data interface::create_polygon(
      const euclid::geometry::details::vector_of_points& _shell,
      const std::vector<const euclid::geometry::details::vector_of_points*>& _holes,
      std::size_t _dimensions)
    {
      std::shared_ptr<OGRPolygon> pd = std::make_shared<OGRPolygon>();

      OGRLinearRing* lr = new OGRLinearRing;
      append_points_to<true>(lr, _shell, _dimensions);
      pd->addRingDirectly(lr);

      for(const euclid::geometry::details::vector_of_points* hole : _holes)
      {
        OGRLinearRing* lr = new OGRLinearRing;
        append_points_to<true>(lr, *hole, _dimensions);
        pd->addRingDirectly(lr);
      }
      return pd;
    }
    polygon_data
      interface::create_polygon(const linear_ring_data& _exterior_ring_data,
                                const std::vector<linear_ring_data>& _interior_ring_datas)
    {
      std::shared_ptr<OGRPolygon> pd = std::make_shared<OGRPolygon>();
      pd->addRing(const_cast<OGRLinearRing*>(_exterior_ring_data.get()));
      for(const linear_ring_data& hole : _interior_ring_datas)
      {
        pd->addRing(const_cast<OGRLinearRing*>(hole.get()));
      }
      return pd;
    }
    linear_ring_data interface::polygon_get_exterior_ring(polygon_data _data)
    {
      return euclid::internal::utils::make_shared_from_ptr<false>(_data->getExteriorRing(), _data);
    }
    std::vector<linear_ring_data> interface::polygon_get_interior_rings(polygon_data _data)
    {
      std::vector<linear_ring_data> lrds;
      for(int i = 0; i < _data->getNumInteriorRings(); ++i)
      {
        lrds.push_back(
          euclid::internal::utils::make_shared_from_ptr<false>(_data->getInteriorRing(i), _data));
      }
      return lrds;
    }
    // END polygon interface
    // BEGIN collection interface
    template<typename _DT_>
    std::size_t interface::collection_size(const _DT_& _data)
    {
      return _data->getNumGeometries();
    }
    template<typename _CDT_>
    container_data_to_containee_data_t<_CDT_> interface::collection_at(const _CDT_& _data,
                                                                       std::size_t _index)
    {
      using ogr_type = typename container_data_to_containee_data_t<_CDT_>::element_type;
      return euclid::internal::utils::make_shared_from_ptr<false>(
        static_cast<ogr_type*>(_data->getGeometryRef(_index)));
    }
    typename traits<euclid::gdal::point>::collection_data interface::create_collection(
      const std::shared_ptr<euclid::geometry::details::vector_of_points>& _points)
    {
      OGRMultiPoint* omp = new OGRMultiPoint;

      for(const euclid::simple::point& pt : *_points)
      {
        switch(pt.dimensions())
        {
        case 2:
          omp->addGeometryDirectly(new OGRPoint(pt.x(), pt.y()));
          break;
        case 3:
          omp->addGeometryDirectly(new OGRPoint(pt.x(), pt.y(), pt.z()));
          break;
        default:
          euclid_fatal("Invalid point dimension");
        }
      }

      return std::shared_ptr<OGRMultiPoint>(omp);
    }
    template<typename _T_>
    typename traits<_T_>::collection_data
      interface::create_collection(const std::vector<_T_>& _data)
    {
      using collection_data = typename traits<_T_>::collection_data;
      using ogr_type = std::remove_const_t<typename collection_data::element_type>;
      ogr_type* col = new ogr_type;
      for(const _T_& elt : _data)
      {
        col->addGeometryDirectly(elt.m_data->clone());
      }
      return collection_data(col);
    }
    // END collection interface
    // BEGIN points iterator
    template<typename _DT_>
    std::size_t interface::points_size(const _DT_& _data)
    {
      return _data->getNumPoints();
    }
    template<typename _DT_>
    euclid::simple::point interface::points_at(const _DT_& _data, std::size_t _index)
    {
      switch(_data->getCoordinateDimension())
      {
      case 2:
        return {_data->getX(_index), _data->getY(_index)};
      case 3:
        return {_data->getX(_index), _data->getY(_index), _data->getZ(_index)};
      default:
        euclid_fatal("Unsupported dimension: %{}", _data->getCoordinateDimension());
      }
    }
    // END points iterator
    // BEGIN IO
    template<class _T_>
    std::string interface::to_wkt(const _T_& _object, const euclid::io::wkt_options&)
    {
      char* txt;
      if(_object.m_data->exportToWkt(&txt) == OGRERR_NONE)
      {
        std::string qtxt(txt);
        CPLFree(txt);
        return qtxt;
      }
      return std::string();
    }
    template<class _T_>
    _T_ interface::from_wkt(const std::string& _wkt)
    {
#if(GDAL_VERSION_MAJOR >= 2 && GDAL_VERSION_MINOR >= 3) || GDAL_VERSION_MAJOR > 2
      const char* data_str = _wkt.c_str();
#else
      char* data_str = const_cast<char*>(_wkt.c_str());
#endif
      using ogr_type = typename traits<_T_>::data_type::element_type;
      //       std::shared_ptr<OGRGeometry> geom = std::make_shared<OGRGeometry>();
      OGRGeometry* geom;
      OGRGeometryFactory::createFromWkt(&data_str, nullptr, &geom);
      ogr_type* geom_ot = dynamic_cast<ogr_type*>(geom);
      if(geom_ot)
      {
        return _T_(std::shared_ptr<ogr_type>(geom_ot));
      }
      else
      {
        return _T_();
      }
    }
    // END IO

    // END interface implementation

  } // namespace details::gdal
  // BEGIN Interface with gdal objects
  template<template<typename> class _T_>
  const OGRGeometry* to_gdal(const _T_<euclid::gdal>& _t)
  {
    return gdal::interface::to_gdal(_t);
  }
  /**
   * Create a wrap from GDAL, where the resulting euclid object do not own a copy of the GDAL object
   */
  template<typename _T_>
  _T_ temporary_from_gdal(const typename details::gdal::traits<_T_>::data::element_type* _value)
  {
    return gdal::interface::from_gdal<_T_, false>(_value);
  }
  // END Interface with gdal objects
} // namespace euclid
