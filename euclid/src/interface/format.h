
namespace euclid::format
{
  struct base_formatter
  {
    template<typename FormatContext>
    constexpr FormatContext::iterator parse(FormatContext& ctx)
    {
      auto it = ctx.begin(), end = ctx.end();

      if(it != end && *it != '}')
        throw ::std::format_error("invalid format");

      return it;
    }
  };
  template<typename _T_>
  struct formatter
  {
    template<typename format_parse_context>
    constexpr format_parse_context::iterator parse(format_parse_context& ctx)
    {
      auto it = ctx.begin(), end = ctx.end();

      if(it != end and *it == '.')
      {
        precision = 0;

        ++it;

        while(it != end and *it >= '0' and *it < '9')
        {
          precision = (*it - '0') + 10 * precision;
          ++it;
        }
      }
      if(it != end and *it == '#')
      {
        ++it;

        dim = (*it - '0');

        if(dim != 2 and dim != 3)
        {
          throw ::std::format_error("invalid dimension, should be 2 or 3");
        }
        ++it;
      }

      if(it != end and *it != '}')
      {
        throw ::std::format_error("invalid format");
      }

      return it;
    }
    template<typename FormatContext>
    FormatContext::iterator format(const _T_& p, FormatContext& ctx) const
    {
      return ::std::formatter<std::string>().format(euclid::io::to_wkt(p, {precision, dim}), ctx);
    }
  private:
    int precision = 2;
    int dim = 2;
  };
} // namespace euclid::format

#define EUCLID_DEFINE_FORMATTER(_KLASS_)                                                           \
  template<typename _system_>                                                                      \
  struct std::formatter<euclid::geometry::_KLASS_<_system_>>                                       \
      : public euclid::format::formatter<euclid::geometry::_KLASS_<_system_>>                      \
  {                                                                                                \
  };

EUCLID_DEFINE_FORMATTER(geometry)
EUCLID_DEFINE_FORMATTER(polygon)
EUCLID_DEFINE_FORMATTER(line_string)
EUCLID_DEFINE_FORMATTER(linear_ring)
EUCLID_DEFINE_FORMATTER(point)
EUCLID_DEFINE_FORMATTER(multi_point)
EUCLID_DEFINE_FORMATTER(multi_geometry)
EUCLID_DEFINE_FORMATTER(multi_line_string)

template<>
struct std::formatter<euclid::simple::point> : public euclid::format::base_formatter
{
  template<typename FormatContext>
  FormatContext::iterator format(const euclid::simple::point& p, FormatContext& ctx) const
  {
    if(std::isnan(p.z()))
    {
      return format_to(ctx.out(), "({}, {})", p.x(), p.y());
    }
    else
    {
      return format_to(ctx.out(), "({}, {}, {})", p.x(), p.y(), p.z());
    }
  }
};

template<>
struct std::formatter<euclid::simple::segment> : public euclid::format::base_formatter
{
  template<typename FormatContext>
  FormatContext::iterator format(const euclid::simple::segment& s, FormatContext& ctx) const
  {
    return format_to(ctx.out(), "[{}, {}]", s.first(), s.second());
  }
};

#undef EUCLID_DEFINE_FORMATTER
