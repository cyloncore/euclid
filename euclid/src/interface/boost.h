#include <sstream>
#include <variant>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/geometries.hpp>

#include <euclid/geometry>
#include <euclid/io>

#include <euclid/src/ops/impl/get_dimensions.h>

namespace euclid::details::boost
{
  // declare the boost interface
  struct interface;
} // namespace euclid::details::boost

// Some required boost models
namespace boost
{
  template<typename _T_>
  struct range_value<euclid::geometry::details::collection_builder<_T_>>
  {
    using type = _T_;
  };
  namespace geometry::core_dispatch
  {
    template<>
    struct point_order<void, euclid::geometry::geometry<euclid::details::boost::interface>>
        : public detail::point_order::clockwise
    {
    };

  } // namespace geometry::core_dispatch
} // namespace boost

namespace euclid
{
  namespace details::boost
  {
    template<typename _T_>
    struct traits;

    struct boost_geometry_data;

    // Geometry data
    using boost_point
      = ::boost::geometry::model::point<double, 3, ::boost::geometry::cs::cartesian>;
    using boost_point_data = std::shared_ptr<boost_point>;
    using boost_line_string = ::boost::geometry::model::linestring<boost_point>;
    using boost_line_string_data = std::shared_ptr<boost_line_string>;
    using boost_linear_ring = ::boost::geometry::model::ring<boost_point>;
    using boost_linear_ring_data = std::shared_ptr<boost_linear_ring>;
    using boost_polygon = ::boost::geometry::model::polygon<boost_point>;
    using boost_polygon_data = std::shared_ptr<boost_polygon>;

    // Iterator data
    using boost_point_iterator_data = std::vector<boost_point>::const_iterator;

    // Collection data
    template<typename _T_>
    using boost_collection_base = std::vector<std::shared_ptr<_T_>>;
    template<typename _T_>
    using boost_collection_base_data = std::shared_ptr<boost_collection_base<_T_>>;

    using boost_collection_data = std::shared_ptr<std::vector<boost_geometry_data>>;
    using boost_multi_point = ::boost::geometry::model::multi_point<boost_point>;
    using boost_multi_point_data = std::shared_ptr<boost_multi_point>;
    using boost_multi_line_string = ::boost::geometry::model::multi_linestring<boost_line_string>;
    using boost_multi_line_string_data = std::shared_ptr<boost_multi_line_string>;
    using boost_multi_polygon = ::boost::geometry::model::multi_polygon<boost_polygon>;
    using boost_multi_polygon_data = std::shared_ptr<boost_multi_polygon>;

    // Geometry data
    using boost_geometry_variant_data
      = std::variant<boost_point_data, boost_line_string_data, boost_linear_ring_data,
                     boost_polygon_data, boost_collection_data, boost_multi_point_data,
                     boost_multi_line_string_data, boost_multi_polygon_data>;

    struct boost_geometry_data : public boost_geometry_variant_data
    {
      using boost_geometry_variant_data::boost_geometry_variant_data;
      using boost_geometry_variant_data::operator=;
      boost_geometry_data(const std::shared_ptr<boost_geometry_data>& _data)
          : boost_geometry_variant_data(*_data)
      {
      }
    };
  } // namespace details::boost

  struct boost : public euclid::geometry::system_base<boost, details::boost::interface>
  {
  };

  namespace details::boost
  {
    using geometry = euclid::boost::geometry;
    using point = euclid::boost::point;
    using line_string = euclid::boost::line_string;
    using linear_ring = euclid::boost::linear_ring;
    using polygon = euclid::boost::polygon;

    using multi_geometry = euclid::boost::multi_geometry;
    using multi_point = euclid::boost::multi_point;
    using multi_line_string = euclid::boost::multi_line_string;
    using multi_polygon = euclid::boost::multi_polygon;

    template<>
    struct traits<geometry>
    {
      using data = boost_geometry_data;
      using collection_data = boost_collection_data;
    };
    template<>
    struct traits<point>
    {
      using data = boost_point_data;
      using collection_data = boost_multi_point_data;
    };
    template<>
    struct traits<line_string>
    {
      using data = boost_line_string_data;
      using collection_data = boost_multi_line_string_data;
    };
    template<>
    struct traits<linear_ring>
    {
      using data = boost_linear_ring_data;
    };
    template<>
    struct traits<polygon>
    {
      using data = boost_polygon_data;
      using collection_data = boost_multi_polygon_data;
    };
    template<>
    struct traits<multi_geometry>
    {
      using data = boost_collection_data;
    };
    template<>
    struct traits<multi_point>
    {
      using data = boost_multi_point_data;
    };
    template<>
    struct traits<multi_line_string>
    {
      using data = boost_multi_line_string_data;
    };
    template<>
    struct traits<multi_polygon>
    {
      using data = boost_multi_polygon_data;
    };
    template<typename _T_>
    struct traits<euclid::boost::point_iterator<_T_>>
    {
      using data = typename traits<_T_>::data;
    };

    struct interface
    {
      friend geometry;
      friend line_string;
      friend linear_ring;
      friend point;
      friend polygon;
      template<typename _T_>
      friend class euclid::geometry::details::points_helper;
      template<typename _T_>
      friend class euclid::geometry::point_iterator;
      friend class euclid::geometry::details::line_string_builder<euclid::boost>;
      friend class euclid::geometry::details::linear_ring_builder<euclid::boost>;
      friend class euclid::geometry::details::polygon_builder<euclid::boost>;
      template<typename _T_>
      friend class euclid::geometry::details::collection_builder;
      template<typename _T_>
      friend class euclid::geometry::details::collection;

      template<typename _T_>
      using traits = euclid::details::boost::traits<_T_>;
      using system = euclid::boost;
    private:
      template<bool _close_, typename _T_>
      static inline void
        append_points_to(_T_ _container,
                         const euclid::geometry::details::vector_of_points& _points);
      template<typename _TData_>
      static inline std::size_t get_dimensions(const std::shared_ptr<_TData_>& _data);
      // BEGIN Interface with euclid::geometry
      template<typename _T_>
      static inline bool geometry_is(const boost_geometry_data& _data_type);
      template<typename _T_>
      static inline typename traits<_T_>::data geometry_cast(const boost_geometry_data& _data_type);
      // END Interface with euclid::geometry
      //  point interface
      static inline boost_point_data create_point(double _x, double _y, double _z);
      static inline double point_get_x(boost_point_data _data);
      static inline double point_get_y(boost_point_data _data);
      static inline double point_get_z(boost_point_data _data);
      // line string interface
      static inline boost_line_string_data
        create_line_string(const euclid::geometry::details::vector_of_points& _points,
                           std::size_t _dimensions);
      // linear ring interface
      static inline boost_linear_ring_data
        create_linear_ring(const euclid::geometry::details::vector_of_points& _points,
                           std::size_t _dimensions);
      // polygon interface
      static inline boost_polygon_data create_polygon(
        const euclid::geometry::details::vector_of_points& _shell,
        const std::vector<const euclid::geometry::details::vector_of_points*>& _holes,
        std::size_t _dimensions);
      static inline boost_polygon_data
        create_polygon(const boost_linear_ring_data& _exterior_ring_data,
                       const std::vector<boost_linear_ring_data>& _interior_ring_datas);
      static inline boost_linear_ring_data polygon_get_exterior_ring(boost_polygon_data _data);
      static inline std::vector<boost_linear_ring_data>
        polygon_get_interior_rings(boost_polygon_data _data);
      // collection interface
      // BEGIN collection interface
      template<typename _DT_>
      static inline std::size_t collection_size(const _DT_& _data);
      template<typename _T_, template<typename> class _C_>
      static inline std::shared_ptr<_T_> collection_at(const std::shared_ptr<_C_<_T_>>& _data,
                                                       std::size_t _index);
      static inline typename traits<euclid::boost::point>::collection_data create_collection(
        const std::shared_ptr<euclid::geometry::details::vector_of_points>& _points);
      template<typename _T_>
      static inline typename traits<_T_>::collection_data
        create_collection(const std::vector<_T_>& _data);
      // END collection interface
      // BEGIN points
      template<typename _DT_>
      static inline std::size_t points_size(const _DT_& _data);
      template<typename _DT_>
      static inline euclid::simple::point points_at(const _DT_& _data, std::size_t _index);
      // END points
    public:
      // IO
      template<class _T_>
      static inline std::string to_wkt(const _T_& _object, const euclid::io::wkt_options& _options);
      template<class _T_>
      static inline _T_ from_wkt(const std::string& _wkt);
      // BEGIN OPS
#define EUCLID_BOOST_BINARY_OP(_NAME_, _BOOST_CALL_)                                               \
  template<class _T1_, class _T2_>                                                                 \
  static inline geometry _NAME_(const _T1_& _object1, const _T2_& _object2)                        \
  {                                                                                                \
    multi_geometry::builder r;                                                                     \
    ::boost::geometry::_BOOST_CALL_(*_object1.m_data, *_object2.m_data, r);                        \
    return geometry(boost_geometry_data(r.create().m_data));                                       \
  }

#define EUCLID_BOOST_UNARY_FN_OP(_NAME_, _RET_)                                                    \
  template<class _T_>                                                                              \
  static inline double _NAME_(const _T_& _object)

#define EUCLID_BOOST_UNARY_OP(_NAME_, _BOOST_CALL_, _RET_)                                         \
  EUCLID_BOOST_UNARY_FN_OP(_NAME_, _RET_)                                                          \
  {                                                                                                \
    return ::boost::geometry::_BOOST_CALL_(*_object.m_data);                                       \
  }
      EUCLID_BOOST_BINARY_OP(intersection, intersection)
      //     EUCLID_BOOST_UNARY_OP(area, area, double)
      EUCLID_BOOST_UNARY_FN_OP(is_valid, bool)
      {
        ::boost::geometry::validity_failure_type failure;
        return ::boost::geometry::is_valid(_object, failure);
      }
#undef EUCLID_BOOST_BINARY_OP
#undef EUCLID_BOOST_UNARY_FN_OP
#undef EUCLID_BOOST_UNARY_OP
      // END OPS
    };

    // BEGIN Interface with euclid::geometry
    template<typename _T_>
    bool interface::geometry_is(const boost_geometry_data& _data_type)
    {
      return std::holds_alternative<typename traits<_T_>::data>(_data_type);
    }
    template<typename _T_>
    typename traits<_T_>::data interface::geometry_cast(const boost_geometry_data& _data_type)
    {
      return std::get<typename traits<_T_>::data>(_data_type);
    }
    // END Interface with euclid::geometry

    // Implementation: Point
    boost_point_data interface::create_point(double _x, double _y, double _z)
    {
      return std::make_shared<boost_point>(_x, _y, _z);
    }
    double interface::point_get_x(boost_point_data _data) { return _data->get<0>(); }
    double interface::point_get_y(boost_point_data _data) { return _data->get<1>(); }
    double interface::point_get_z(boost_point_data _data) { return _data->get<2>(); }
    // Implementation
    template<bool _close_, typename _T_>
    void interface::append_points_to(_T_ _container,
                                     const euclid::geometry::details::vector_of_points& _points)
    {
      for(auto [x, y, z] : _points)
      {
        _container->push_back({x, y, z});
      }

      if(_close_)
      {
        auto [x, y, z] = *_points.begin();
        _container->push_back({x, y, z});
      }
    }
    template<typename _T_>
    std::size_t interface::get_dimensions(const std::shared_ptr<_T_>& _data)
    {
      return euclid::ops::impl::get_dimensions(euclid::boost::geometry(
        _data)); // TODO ugly, do not use euclid::boost::geometry, but the correct holding type,
                 // which requires to have the mapping from data to type
    }
    // Implementation: line_string
    boost_line_string_data
      interface::create_line_string(const euclid::geometry::details::vector_of_points& _points,
                                    std::size_t /*_dimensions*/)
    {
      boost_line_string_data lr = std::make_shared<boost_line_string>();
      append_points_to<false>(lr, _points);
      return lr;
    }
    // Implementation: linear_ring
    boost_linear_ring_data
      interface::create_linear_ring(const euclid::geometry::details::vector_of_points& _points,
                                    std::size_t /*_dimensions*/)
    {
      boost_linear_ring_data lr = std::make_shared<boost_linear_ring>();
      append_points_to<true>(lr, _points);
      return lr;
    }
    // Implementation: polygon
    boost_polygon_data interface::create_polygon(
      const euclid::geometry::details::vector_of_points& _shell,
      const std::vector<const euclid::geometry::details::vector_of_points*>& _holes,
      std::size_t /*_dimensions*/)
    {
      boost_polygon_data p = std::make_shared<boost_polygon>();
      append_points_to<true>(&p->outer(), _shell);
      std::transform(_holes.begin(), _holes.end(), std::back_inserter(p->inners()),
                     [](const euclid::geometry::details::vector_of_points* _vp)
                     {
                       boost_polygon::ring_type ring;
                       append_points_to<true>(&ring, *_vp);
                       return ring;
                     });
      return p;
    }
    boost_polygon_data
      interface::create_polygon(const boost_linear_ring_data& _exterior_ring_data,
                                const std::vector<boost_linear_ring_data>& _interior_ring_datas)
    {
      boost_polygon_data p = std::make_shared<boost_polygon>();
      std::copy(_exterior_ring_data->begin(), _exterior_ring_data->end(),
                std::back_inserter(p->outer()));
      std::transform(_interior_ring_datas.begin(), _interior_ring_datas.end(),
                     std::back_inserter(p->inners()),
                     [](const boost_linear_ring_data& _vp) { return *_vp; });

      return p;
    }
    boost_linear_ring_data interface::polygon_get_exterior_ring(boost_polygon_data _data)
    {
      return std::make_shared<boost_linear_ring>(_data->outer());
    }
    std::vector<boost_linear_ring_data>
      interface::polygon_get_interior_rings(boost_polygon_data _data)
    {
      std::vector<boost_linear_ring_data> hs;

      for(const boost_polygon::ring_type& h : _data->inners())
      {
        hs.push_back(std::make_shared<boost_linear_ring>(h));
      }

      return hs;
    }
    // BEGIN Implementation: collection
    template<typename _DT_>
    std::size_t interface::collection_size(const _DT_& _data)
    {
      return _data->size();
    }
    template<typename _T_, template<typename> class _C_>
    std::shared_ptr<_T_> interface::collection_at(const std::shared_ptr<_C_<_T_>>& _data,
                                                  std::size_t _index)
    {
      return std::make_shared<_T_>((*_data)[_index]);
    }
    typename interface::traits<euclid::boost::point>::collection_data interface::create_collection(
      const std::shared_ptr<euclid::geometry::details::vector_of_points>& _points)
    {
      using collection_data = typename traits<euclid::boost::point>::collection_data;
      collection_data data = std::make_shared<typename collection_data::element_type>();

      for(const euclid::simple::point& t : *_points)
      {
        data->push_back(boost_point(t.x(), t.y(), t.z()));
      }

      return data;
    }
    template<typename _T_>
    typename interface::traits<_T_>::collection_data
      interface::create_collection(const std::vector<_T_>& _data)
    {
      using collection_data = typename traits<_T_>::collection_data;
      collection_data data = std::make_shared<typename collection_data::element_type>();

      for(const _T_& t : _data)
      {
        data->push_back(*t.m_data);
      }

      return data;
    }
    // END Implementation: collection
    // BEGIN points
    template<typename _DT_>
    std::size_t interface::points_size(const _DT_& _data)
    {
      return _data->size();
    }
    template<typename _DT_>
    euclid::simple::point interface::points_at(const _DT_& _data, std::size_t _index)
    {
      return euclid::simple::point((*_data)[_index].template get<0>(),
                                   (*_data)[_index].template get<1>(),
                                   (*_data)[_index].template get<2>());
    }
    // END points
    // BEGIN Implementation: IO
    template<class _T_>
    std::string interface::to_wkt(const _T_& _object, const euclid::io::wkt_options& _options)
    {
      std::stringstream ss;
      ss << std::setprecision(_options.precision) << ::boost::geometry::wkt(*_object.m_data);
      return ss.str();
    }
    template<class _T_>
    _T_ interface::from_wkt(const std::string& _wkt)
    {
      _T_ r(std::make_shared<typename traits<_T_>::data::element_type>());
      ::boost::geometry::read_wkt(_wkt, *r.m_data);
      return r;
    }
    // END IO
  } // namespace details::boost
} // namespace euclid
