#include <functional>
#include <iostream>
#include <memory>

#include <geos_c.h>

#include <euclid/geometry>
#include <euclid/io>

#include <euclid/src/ops/impl/envelope.h>

#include "features.h"

#if GEOS_VERSION_MAJOR > 3 || (GEOS_VERSION_MAJOR == 3 && GEOS_VERSION_MINOR >= 9)
#define EUCLID_GEOS_HAS_MAXIMUM_INSCRIBED_CIRCLE
#endif

#if GEOS_VERSION_MAJOR > 3 || (GEOS_VERSION_MAJOR == 3 && GEOS_VERSION_MINOR >= 10)
#define __EUCLID_GEOS_HAS_JSON_VALUE__ true
#define EUCLID_GEOS_HAS_JSON
#else
#define __EUCLID_GEOS_HAS_JSON_VALUE__ false
#endif

namespace euclid
{
  // BEGIN Forward details / data
  namespace details::geos
  {
    struct interface;
    // Data
    using data = std::shared_ptr<const GEOSGeometry>;
    struct point_iterator_data
    {
      const GEOSCoordSequence* sequence;
      unsigned int index;
      bool operator==(const point_iterator_data& _rhs) const
      {
        return sequence == _rhs.sequence and index == _rhs.index;
      }
    };

    // Forward
    template<typename _T_>
    struct common_traits;
    template<typename _T_>
    struct container_common_traits;
    template<typename _T_>
    struct traits;
  } // namespace details::geos
  // END Forward details / data

  // BEGIN Declare geos
  struct geos : public euclid::geometry::system_base<geos, details::geos::interface>
  {
    using features = details::features<__EUCLID_GEOS_HAS_JSON_VALUE__>;
    // BEGIN context definition
    struct context
    {
      friend interface;
      template<typename _T_>
      friend struct details::geos::common_traits;
      template<typename _T_>
      friend struct details::geos::container_common_traits;
      template<typename _T_>
      friend struct details::geos::traits;
      context() : m_data(std::make_shared<data>()) {}
      using system = euclid::geos;
      static context get_context()
      {
        static thread_local context s_context;
        // Check if the local context is still valid
        if(s_context.m_data->contextHandle)
        {
          return s_context;
        }
        else
        {
          return context();
        }
      }
    private:
      static GEOSContextHandle_t get() { return get_context(); }
      operator GEOSContextHandle_t() const { return m_data->contextHandle; }
      struct data
      {
        data()
        {
          contextHandle = GEOS_init_r();
          GEOSContext_setNoticeMessageHandler_r(contextHandle, &data::print_message, nullptr);
          GEOSContext_setErrorMessageHandler_r(contextHandle, &data::print_message, nullptr);
        }
        ~data()
        {
          GEOS_finish_r(contextHandle);
          contextHandle = nullptr;
        }
        static void print_message(const char* _message, void*) { euclid_error(_message); }
        GEOSContextHandle_t contextHandle;
      };
      std::shared_ptr<data> m_data;
    };
    // END context
  };
  // END Declare geos

  // BEGIN Forward Interface with geos objects
  template<template<typename> class _T_>
  std::tuple<GEOSContextHandle_t, const GEOSGeometry*> to_geos(const _T_<euclid::geos>& _t);
  GEOSContextHandle_HS* to_geos(const euclid::geos::context& _t);
  euclid::geos::geometry from_geos(GEOSGeometry* _geometry);
  // END Forward Interface with geos objects

  // BEGIN traits and some other details
  namespace details::geos
  {
    using context_type = euclid::geos::context;

    template<typename _T_>
    struct private_traits;
    template<int _T_id_>
    struct base_private_traits
    {
      static constexpr int geos_id = _T_id_;
    };
    template<int _T_id_, typename _T_E_>
    struct base_private_container_traits : base_private_traits<_T_id_>
    {
      using element_type = _T_E_;
    };

    template<>
    struct private_traits<euclid::geos::point> : base_private_traits<GEOS_POINT>
    {
    };
    template<>
    struct private_traits<euclid::geos::line_string> : base_private_traits<GEOS_LINESTRING>
    {
    };
    template<>
    struct private_traits<euclid::geos::linear_ring> : base_private_traits<GEOS_LINEARRING>
    {
    };
    template<>
    struct private_traits<euclid::geos::polygon> : base_private_traits<GEOS_POLYGON>
    {
    };
    template<>
    struct private_traits<euclid::geos::multi_geometry>
        : base_private_traits<GEOS_GEOMETRYCOLLECTION>
    {
    };
    template<>
    struct private_traits<euclid::geometry::details::collection<euclid::geos::geometry>>
        : public private_traits<euclid::geos::multi_geometry>
    {
    };
    template<>
    struct private_traits<euclid::geos::multi_point>
        : base_private_container_traits<GEOS_MULTIPOINT, euclid::geos::point>
    {
    };
    template<>
    struct private_traits<euclid::geos::multi_line_string>
        : base_private_container_traits<GEOS_MULTILINESTRING, euclid::geos::line_string>
    {
    };
    template<>
    struct private_traits<euclid::geos::multi_polygon>
        : base_private_container_traits<GEOS_MULTIPOLYGON, euclid::geos::polygon>
    {
    };

    struct base_traits
    {
      using data = details::geos::data;
    };
    template<typename _T_>
    struct common_traits : public base_traits
    {
      friend euclid::geos::interface;
    private:
      static bool accept_geometry(const GEOSGeometry* _geometry)
      {
        return GEOSGeomTypeId_r(euclid::geos::context::get(), _geometry)
               == private_traits<_T_>::geos_id;
      }
    };
    template<typename _T_>
    struct container_common_traits : public base_traits
    {
      friend euclid::geos::interface;
      using element_type = typename private_traits<_T_>::element_type;
    private:
      static bool accept_geometry(const GEOSGeometry* _geometry)
      {
        int id = GEOSGeomTypeId_r(euclid::geos::context::get(), _geometry);
        return id == private_traits<_T_>::geos_id or id == private_traits<element_type>::geos_id;
      }
    };
    template<>
    struct traits<euclid::geos::geometry> : public base_traits
    {
      friend struct interface;
    private:
      static bool accept_geometry(const GEOSGeometry*) { return true; }
    };
    template<>
    struct traits<euclid::geos::point> : public common_traits<euclid::geos::point>
    {
    };
    template<>
    struct traits<euclid::geos::line_string> : public common_traits<euclid::geos::line_string>
    {
    };
    template<>
    struct traits<euclid::geos::linear_ring> : public common_traits<euclid::geos::linear_ring>
    {
    };
    template<>
    struct traits<euclid::geos::polygon> : public common_traits<euclid::geos::polygon>
    {
    };
    template<>
    struct traits<euclid::geos::multi_geometry> : public base_traits
    {
      friend struct interface;
    private:
      static bool accept_geometry(const GEOSGeometry* _geometry)
      {
        switch(GEOSGeomTypes(GEOSGeomTypeId_r(euclid::geos::context::get(), _geometry)))
        {
        case GEOS_POINT:
        case GEOS_LINESTRING:
        case GEOS_LINEARRING:
        case GEOS_POLYGON:
          return false;

        case GEOS_MULTIPOINT:
        case GEOS_MULTILINESTRING:
        case GEOS_MULTIPOLYGON:
        case GEOS_GEOMETRYCOLLECTION:
          return true;
#if(GEOS_VERSION_MAJOR >= 3 && GEOS_VERSION_MINOR >= 13) || GEOS_VERSION_MAJOR > 3
        case GEOS_COMPOUNDCURVE:
        case GEOS_CURVEPOLYGON:
        case GEOS_MULTICURVE:
        case GEOS_MULTISURFACE:
        case GEOS_CIRCULARSTRING:
          return false;
#endif
        }

        return false;
      }
    };
    template<>
    struct traits<euclid::geos::multi_point>
        : public container_common_traits<euclid::geos::multi_point>
    {
    };
    template<>
    struct traits<euclid::geos::multi_line_string>
        : public container_common_traits<euclid::geos::multi_line_string>
    {
    };
    template<>
    struct traits<euclid::geos::multi_polygon>
        : public container_common_traits<euclid::geos::multi_polygon>
    {
    };
    template<typename _T_>
    struct traits<euclid::geos::point_iterator<_T_>> : public base_traits
    {
    };
    struct interface
    {
      friend euclid::geos::geometry;
      friend euclid::geos::line_string;
      friend euclid::geos::linear_ring;
      friend euclid::geos::point;
      friend euclid::geos::polygon;
      friend euclid::geos::multi_geometry;
      friend class euclid::geometry::details::line_string_builder<euclid::geos>;
      friend class euclid::geometry::details::linear_ring_builder<euclid::geos>;
      friend class euclid::geometry::details::polygon_builder<euclid::geos>;
      template<typename _T_>
      friend class euclid::geometry::details::collection_builder;
      template<typename _T_>
      friend class euclid::geometry::details::points_helper;
      template<typename _T_>
      friend class euclid::geometry::point_iterator;
      template<typename _T_>
      friend class euclid::geometry::details::collection;

      using data_type = euclid::details::geos::data;
      using context_type = euclid::geos::context;
      template<typename _T_>
      using traits = euclid::details::geos::traits<_T_>;
      using system = euclid::geos;

      template<template<typename> class _T_>
      friend std::tuple<GEOSContextHandle_t, const GEOSGeometry*>
        euclid::to_geos(const _T_<euclid::geos>&);
      friend GEOSContextHandle_HS* euclid::to_geos(const euclid::geos::context& _t);
      friend euclid::geos::geometry euclid::from_geos(GEOSGeometry* _geometry);
    private:
      template<template<typename> class _T_>
      static inline std::tuple<GEOSContextHandle_t, const GEOSGeometry*>
        to_geos(const _T_<system>& _t)
      {
        return std::make_tuple(euclid::geos::context::get(), _t.m_data.get());
      }
      static inline GEOSContextHandle_t to_geos(const context_type& _t) { return _t; }
      static inline euclid::geos::geometry from_geos(GEOSGeometry* _geometry)
      {
        return euclid::geos::geometry(geos::interface::make_data(_geometry));
      }
      // Internal API
      static inline data_type make_data(GEOSGeometry* _geometry);
      static inline data_type make_data_clone(const GEOSGeometry* _geometry);
      template<bool _close>
      static inline GEOSCoordSequence*
        create_sequence(const euclid::geometry::details::vector_of_points& _points,
                        std::size_t _dimensions);
      static inline GEOSGeometry*
        create_linear_ring_geos(const euclid::geometry::details::vector_of_points& _points,
                                std::size_t _dimensions);
    private:
      // Interface with euclid::geometry
      static inline std::size_t get_dimensions(const data_type& _data);
      static inline bool is_empty(const data_type& _data);
      // BEGIN geometry interface
      template<typename _T_>
      static inline bool geometry_is(const data_type& _data_type);
      static inline bool geometry_is_collection(const data_type& _data_type);
      template<typename _T_>
      static inline data_type geometry_cast(const data_type& _data_type);
      // END geometry interface
      //  point interface
      static inline data_type create_point(double _x, double _y, double _z);
      static inline double point_get_x(const data_type& _data);
      static inline double point_get_y(const data_type& _data);
      static inline double point_get_z(const data_type& _data);
      // line string interface
      static inline data_type
        create_line_string(const euclid::geometry::details::vector_of_points& _points,
                           std::size_t _dimensions);
      // linear ring interface
      static inline data_type
        create_linear_ring(const euclid::geometry::details::vector_of_points& _points,
                           std::size_t _dimensions);
      // BEGIN polygon interface
      static inline data_type create_polygon(
        const euclid::geometry::details::vector_of_points& _shell,
        const std::vector<const euclid::geometry::details::vector_of_points*>& _holes,
        std::size_t _dimensions);
      static inline data_type create_polygon(const data_type& _exterior_ring_data,
                                             const std::vector<data_type>& _interior_ring_datas);
      static inline data_type polygon_get_exterior_ring(const data_type& _data);
      static inline std::vector<data_type> polygon_get_interior_rings(const data_type& _data);
      static inline std::size_t polygon_get_interior_rings_count(const data_type& _data);
      // END polygon interface
      // BEGIN collection interface
      static inline std::size_t collection_size(const data_type& _data);
      static inline data_type collection_at(const data_type& _data, std::size_t _index);
      static inline data_type create_empty_collection();
      static inline data_type create_collection(
        const std::shared_ptr<euclid::geometry::details::vector_of_points>& _points);
      template<typename _T_>
      static inline data_type create_collection(const std::vector<_T_>& _elements);
      // END collection interface
      // BEGIN points
      static inline std::size_t points_size(const data_type& _data);
      static inline euclid::simple::point points_at(const data_type& _data, std::size_t _index);
      // END points
    public:
      // BEGIN IO
      template<class _T_>
      static inline std::string to_wkt(const _T_& _object, const euclid::io::wkt_options& _options);
      template<class _T_>
      static inline _T_ from_wkt(const std::string& _wkt);
#ifdef EUCLID_GEOS_HAS_JSON
      template<class _T_>
      static inline std::string to_json(const _T_& _object,
                                        const euclid::io::json_options& _options);
      template<class _T_>
      static inline _T_ from_json(const std::string& _wkt);
#endif
      // END IO
      // BEGIN OPS
#define EUCLID_GEOS_BINARY_OP(_NAME_, _GEOS_CALL_)                                                 \
  template<class _T1_, class _T2_>                                                                 \
  static inline euclid::geos::geometry _NAME_(const _T1_& _object1, const _T2_& _object2)          \
  {                                                                                                \
    return euclid::geos::geometry(make_data(                                                       \
      _GEOS_CALL_(euclid::geos::context::get(), _object1.m_data.get(), _object2.m_data.get())));   \
  }

#define EUCLID_GEOS_BINARY_BOOL_OP(_NAME_, _GEOS_CALL_)                                            \
  template<class _T1_, class _T2_>                                                                 \
  static inline bool _NAME_(const _T1_& _object1, const _T2_& _object2)                            \
  {                                                                                                \
    return _GEOS_CALL_(euclid::geos::context::get(), _object1.m_data.get(),                        \
                       _object2.m_data.get());                                                     \
  }

#define EUCLID_GEOS_BINARY_BOOL_TOL_OP(_NAME_, _GEOS_CALL_)                                        \
  template<class _T1_, class _T2_>                                                                 \
  static inline bool _NAME_(const _T1_& _object1, const _T2_& _object2, double _tol)               \
  {                                                                                                \
    return _GEOS_CALL_(euclid::geos::context::get(), _object1.m_data.get(), _object2.m_data.get(), \
                       _tol);                                                                      \
  }

#define EUCLID_GEOS_UNARY_MEASUREMENT_OP(_NAME_, _GEOS_CALL_)                                      \
  template<class _T_>                                                                              \
  static inline double _NAME_(const _T_& _object)                                                  \
  {                                                                                                \
    double r;                                                                                      \
    _GEOS_CALL_(euclid::geos::context::get(), _object.m_data.get(), &r);                           \
    return r;                                                                                      \
  }

      EUCLID_GEOS_BINARY_OP(difference, GEOSDifference_r)
      EUCLID_GEOS_BINARY_OP(intersection, GEOSIntersection_r)
      EUCLID_GEOS_BINARY_OP(union_, GEOSUnion_r)
      EUCLID_GEOS_BINARY_BOOL_OP(contains, GEOSContains_r)
      EUCLID_GEOS_BINARY_BOOL_OP(equals, GEOSEquals_r)
      EUCLID_GEOS_BINARY_BOOL_OP(intersects, GEOSIntersects_r)
      EUCLID_GEOS_BINARY_BOOL_OP(disjoint, GEOSDisjoint_r)
      EUCLID_GEOS_BINARY_BOOL_OP(overlaps, GEOSOverlaps_r)
      EUCLID_GEOS_BINARY_BOOL_OP(touches, GEOSTouches_r)
      EUCLID_GEOS_BINARY_BOOL_OP(within, GEOSWithin_r)
      EUCLID_GEOS_BINARY_BOOL_TOL_OP(near_equals, GEOSEqualsExact_r)
      EUCLID_GEOS_UNARY_MEASUREMENT_OP(area, GEOSArea_r)
      EUCLID_GEOS_UNARY_MEASUREMENT_OP(length, GEOSLength_r)

      template<class _T1_>
      static inline euclid::geos::point centroid(const _T1_& _object);

      template<class _T_>
      static inline euclid::geos::geometry offsetting(const _T_& _object, double _amount);
      template<class _T_>
      static inline euclid::simple::box envelope(const _T_& _object);
      template<class _T1_>
      static inline euclid::geos::multi_geometry delaunay_triangulation(const _T1_& _t1,
                                                                        double _tol);
      static inline euclid::geos::multi_geometry
        voronoi_diagram(const euclid::geos::multi_point& _source,
                        const euclid::geos::geometry& _envelope, double _tol);
      template<class _T1_>
      static inline _T1_ simplify(const _T1_& _source, double _tol);

      template<class _T1_, class _T2_>
      static inline bool distance(const _T1_& _object1, const _T2_& _object2)
      {
        double d;
        GEOSDistance_r(euclid::geos::context::get(), _object1.m_data.get(), _object2.m_data.get(),
                       &d);
        return d;
      }

      template<class _T_>
      static inline euclid::simple::circle maximum_inscribed_circle(const _T_& _object,
                                                                    double _tol);
      template<class _T_>
      static inline euclid::simple::circle minimum_enclosing_circle(const _T_& _object);
      template<class _T_>
      static inline euclid::geos::polygon minimum_rotated_rectangle(const _T_& _object);

#undef EUCLID_GEOS_BINARY_OP
#undef EUCLID_GEOS_BINARY_BOOL_OP
#undef EUCLID_GEOS_UNARY_MEASUREMENT_OP
      template<class _T_>
      static inline bool is_valid(const _T_& _object)
      {
        return GEOSisValid_r(euclid::geos::context::get(), _object.m_data.get()) == 1;
      }
      // END OPS
    };

    // BEGIN Implementation
    interface::data_type geos::interface::make_data(GEOSGeometry* _geometry)
    {
      return interface::data_type(_geometry,
                                  [](const GEOSGeometry* _geometry) {
                                    GEOSGeom_destroy_r(euclid::geos::context::get(),
                                                       const_cast<GEOSGeometry*>(_geometry));
                                  });
    }
    interface::data_type interface::make_data_clone(const GEOSGeometry* _geometry)
    {
      return make_data(GEOSGeom_clone_r(euclid::geos::context::get(), _geometry));
    }
    template<bool _close>
    GEOSCoordSequence*
      interface::create_sequence(const euclid::geometry::details::vector_of_points& _points,
                                 std::size_t _dimensions)
    {
      GEOSCoordSequence* seq = GEOSCoordSeq_create_r(
        euclid::geos::context::get(), _points.size() + (_close ? 1 : 0), _dimensions);

      for(std::size_t i = 0; i < _points.size(); ++i)
      {
        auto [x, y, z] = _points[i];
        GEOSCoordSeq_setX_r(euclid::geos::context::get(), seq, i, x);
        GEOSCoordSeq_setY_r(euclid::geos::context::get(), seq, i, y);
        GEOSCoordSeq_setZ_r(euclid::geos::context::get(), seq, i, z);
      }

      if(_close)
      {
        auto [x, y, z] = _points[0];
        GEOSCoordSeq_setX_r(euclid::geos::context::get(), seq, _points.size(), x);
        GEOSCoordSeq_setY_r(euclid::geos::context::get(), seq, _points.size(), y);
        if(not std::isnan(z))
        {
          GEOSCoordSeq_setZ_r(euclid::geos::context::get(), seq, _points.size(), z);
        }
      }

      return seq;
    }
    std::size_t interface::get_dimensions(const data_type& _data)
    {
      return GEOSGeom_getCoordinateDimension_r(euclid::geos::context::get(), _data.get());
    }
    bool interface::is_empty(const data_type& _data)
    {
      return GEOSisEmpty_r(euclid::geos::context::get(), _data.get());
    }
    // BEGIN Implementation: geometry
    template<typename _T_>
    bool interface::geometry_is(const interface::data_type& _data)
    {
      return GEOSGeomTypeId_r(euclid::geos::context::get(), _data.get())
             == details::geos::private_traits<_T_>::geos_id;
    }
    template<>
    inline bool interface::geometry_is<euclid::geos::geometry>(const interface::data_type&)
    {
      return true;
    }
    bool interface::geometry_is_collection(const data_type& _data)
    {
      int geos_id = GEOSGeomTypeId_r(euclid::geos::context::get(), _data.get());
      return geos_id == GEOS_GEOMETRYCOLLECTION or geos_id == GEOS_MULTIPOLYGON
             or geos_id == GEOS_MULTILINESTRING or geos_id == GEOS_MULTIPOINT;
    }
    template<typename _T_>
    interface::data_type interface::geometry_cast(const interface::data_type& _data_type)
    {
      return _data_type;
    }
    // END Implementation: geometry

    // BEGIN Implementation: Point
    interface::data_type interface::create_point(double _x, double _y, double _z)
    {
      GEOSCoordSequence* seq
        = GEOSCoordSeq_create_r(euclid::geos::context::get(), 1, std::isnan(_z) ? 2 : 3);
      GEOSCoordSeq_setX_r(euclid::geos::context::get(), seq, 0, _x);
      GEOSCoordSeq_setY_r(euclid::geos::context::get(), seq, 0, _y);
      GEOSCoordSeq_setZ_r(euclid::geos::context::get(), seq, 0, _z);
      return make_data(GEOSGeom_createPoint_r(euclid::geos::context::get(), seq));
    }
    double interface::point_get_x(const data_type& _data)
    {
      double r;
      GEOSGeomGetX_r(euclid::geos::context::get(), _data.get(), &r);
      return r;
    }
    double interface::point_get_y(const data_type& _data)
    {
      double r;
      GEOSGeomGetY_r(euclid::geos::context::get(), _data.get(), &r);
      return r;
    }
    double interface::point_get_z(const data_type& _data)
    {
      double r;
      const GEOSCoordSequence* seq
        = GEOSGeom_getCoordSeq_r(euclid::geos::context::get(), _data.get());
      GEOSCoordSeq_getZ_r(euclid::geos::context::get(), seq, 0, &r);
      return r;
    }
    // END Implementation: Point
    // BEGIN Implementation: line_string
    interface::data_type
      interface::create_line_string(const euclid::geometry::details::vector_of_points& _points,
                                    std::size_t _dimensions)
    {
      return make_data(GEOSGeom_createLineString_r(euclid::geos::context::get(),
                                                   create_sequence<false>(_points, _dimensions)));
    }
    // END
    //  Implementation: polygon
    interface::data_type
      interface::create_linear_ring(const euclid::geometry::details::vector_of_points& _points,
                                    std::size_t _dimensions)
    {
      return make_data(create_linear_ring_geos(_points, _dimensions));
    }
    GEOSGeometry*
      interface::create_linear_ring_geos(const euclid::geometry::details::vector_of_points& _points,
                                         std::size_t _dimensions)
    {
      return GEOSGeom_createLinearRing_r(euclid::geos::context::get(),
                                         create_sequence<true>(_points, _dimensions));
    }
    // Implementation: polygon
    interface::data_type interface::create_polygon(
      const euclid::geometry::details::vector_of_points& _shell,
      const std::vector<const euclid::geometry::details::vector_of_points*>& _holes,
      std::size_t _dimensions)
    {
      std::vector<GEOSGeometry*> holes;
      std::transform(_holes.begin(), _holes.end(), std::back_inserter(holes),
                     [_dimensions](const euclid::geometry::details::vector_of_points* _vp)
                     { return create_linear_ring_geos(*_vp, _dimensions); });
      return make_data(GEOSGeom_createPolygon_r(euclid::geos::context::get(),
                                                create_linear_ring_geos(_shell, _dimensions),
                                                holes.data(), holes.size()));
    }
    interface::data_type
      interface::create_polygon(const interface::data_type& _exterior_ring_data,
                                const std::vector<interface::data_type>& _interior_ring_datas)
    {
      std::vector<GEOSGeometry*> holes;
      std::transform(_interior_ring_datas.begin(), _interior_ring_datas.end(),
                     std::back_inserter(holes), [](const data_type& _dt)
                     { return GEOSGeom_clone_r(euclid::geos::context::get(), _dt.get()); });
      return make_data(GEOSGeom_createPolygon_r(
        euclid::geos::context::get(),
        GEOSGeom_clone_r(euclid::geos::context::get(), _exterior_ring_data.get()), holes.data(),
        holes.size()));
    }
    interface::data_type interface::polygon_get_exterior_ring(const data_type& _data)
    {
      return make_data_clone(GEOSGetExteriorRing_r(euclid::geos::context::get(), _data.get()));
    }
    std::vector<interface::data_type> interface::polygon_get_interior_rings(const data_type& _data)
    {
      std::vector<interface::data_type> hs;

      for(int i = 0; i < GEOSGetNumInteriorRings_r(euclid::geos::context::get(), _data.get()); ++i)
      {
        hs.push_back(
          make_data_clone(GEOSGetInteriorRingN_r(euclid::geos::context::get(), _data.get(), i)));
      }

      return hs;
    }
    std::size_t interface::polygon_get_interior_rings_count(const data_type& _data)
    {
      return GEOSGetNumInteriorRings_r(euclid::geos::context::get(), _data.get());
    }
    // BEGIN collection interface
    std::size_t interface::collection_size(const data_type& _data)
    {
      return GEOSGetNumGeometries_r(euclid::geos::context::get(), _data.get());
    }
    interface::data_type interface::collection_at(const data_type& _data, std::size_t _index)
    {
      return make_data_clone(GEOSGetGeometryN_r(euclid::geos::context::get(), _data.get(), _index));
    }
    interface::data_type interface::create_empty_collection()
    {
      return make_data(
        GEOSGeom_createEmptyCollection_r(euclid::geos::context::get(), GEOS_GEOMETRYCOLLECTION));
    }
    interface::data_type interface::create_collection(
      const std::shared_ptr<euclid::geometry::details::vector_of_points>& _points)
    {
      std::vector<GEOSGeometry*> geometry;
      for(const euclid::simple::point& elt : *_points)
      {
        GEOSCoordSequence* seq
          = GEOSCoordSeq_create_r(euclid::geos::context::get(), 1, std::isnan(elt.z()) ? 2 : 3);
        GEOSCoordSeq_setX_r(euclid::geos::context::get(), seq, 0, elt.x());
        GEOSCoordSeq_setY_r(euclid::geos::context::get(), seq, 0, elt.y());
        GEOSCoordSeq_setZ_r(euclid::geos::context::get(), seq, 0, elt.z());
        geometry.push_back(GEOSGeom_createPoint_r(euclid::geos::context::get(), seq));
      }
      return make_data(GEOSGeom_createCollection_r(euclid::geos::context::get(), GEOS_MULTIPOINT,
                                                   geometry.data(), geometry.size()));
    }
    template<typename _T_>
    interface::data_type interface::create_collection(const std::vector<_T_>& _elements)
    {
      std::vector<GEOSGeometry*> geometry;
      for(const _T_& elt : _elements)
      {
        geometry.push_back(GEOSGeom_clone_r(euclid::geos::context::get(), elt.m_data.get()));
      }
      return make_data(GEOSGeom_createCollection_r(
        euclid::geos::context::get(),
        private_traits<euclid::geometry::details::collection<_T_>>::geos_id, geometry.data(),
        geometry.size()));
    }
    // END collection interface
    // BEGIN Implementation point_iterator
    std::size_t interface::points_size(const data_type& _data)
    {
      const GEOSCoordSequence* sequence
        = GEOSGeom_getCoordSeq_r(euclid::geos::context::get(), _data.get());
      unsigned int s;
      GEOSCoordSeq_getSize_r(euclid::geos::context::get(), sequence, &s);
      return s;
    }
    euclid::simple::point interface::points_at(const data_type& _data, std::size_t _index)
    {
      const GEOSCoordSequence* sequence
        = GEOSGeom_getCoordSeq_r(euclid::geos::context::get(), _data.get());
      double x, y, z;
      GEOSCoordSeq_getX_r(euclid::geos::context::get(), sequence, _index, &x);
      GEOSCoordSeq_getY_r(euclid::geos::context::get(), sequence, _index, &y);
      GEOSCoordSeq_getZ_r(euclid::geos::context::get(), sequence, _index, &z);
      return euclid::simple::point(x, y, z);
    }
    // END point_iterator
    // BEGIN Implementation IO
    template<class _T_>
    std::string interface::to_wkt(const _T_& _object, const euclid::io::wkt_options& _options)
    {
      GEOSWKTWriter* writter = GEOSWKTWriter_create_r(euclid::geos::context::get());
      GEOSWKTWriter_setRoundingPrecision_r(euclid::geos::context::get(), writter,
                                           _options.precision);
      GEOSWKTWriter_setOutputDimension_r(euclid::geos::context::get(), writter,
                                         _options.dimensions);
      char* str
        = GEOSWKTWriter_write_r(euclid::geos::context::get(), writter, _object.m_data.get());
      std::string r = str;
      GEOSWKTWriter_destroy_r(euclid::geos::context::get(), writter);
      GEOSFree_r(euclid::geos::context::get(), str);
      return r;
    }
    template<class _T_>
    _T_ interface::from_wkt(const std::string& _wkt)
    {
      GEOSWKTReader* reader = GEOSWKTReader_create_r(euclid::geos::context::get());
      GEOSGeometry* geometry
        = GEOSWKTReader_read_r(euclid::geos::context::get(), reader, _wkt.data());
      GEOSWKTReader_destroy_r(euclid::geos::context::get(), reader);

      if(interface::traits<_T_>::accept_geometry(geometry))
      {
        return _T_(make_data(geometry));
      }
      else
      {
        return _T_();
      }
    }
#ifdef EUCLID_GEOS_HAS_JSON
    template<class _T_>
    std::string interface::to_json(const _T_& _object, const euclid::io::json_options& _options)
    {
      GEOSGeoJSONWriter* writter = GEOSGeoJSONWriter_create_r(euclid::geos::context::get());
      char* str = GEOSGeoJSONWriter_writeGeometry_r(euclid::geos::context::get(), writter,
                                                    _object.m_data.get(), _options.indent);
      std::string r = str;
      GEOSGeoJSONWriter_destroy_r(euclid::geos::context::get(), writter);
      GEOSFree_r(euclid::geos::context::get(), str);
      return r;
    }
    template<class _T_>
    _T_ interface::from_json(const std::string& _json)
    {
      GEOSGeoJSONReader* reader = GEOSGeoJSONReader_create_r(euclid::geos::context::get());
      GEOSGeometry* geometry
        = GEOSGeoJSONReader_readGeometry_r(euclid::geos::context::get(), reader, _json.data());
      GEOSGeoJSONReader_destroy_r(euclid::geos::context::get(), reader);

      if(interface::traits<_T_>::accept_geometry(geometry))
      {
        return _T_(make_data(geometry));
      }
      else
      {
        return _T_();
      }
    }
#endif
    // END Implementation IO
    // BEGIN Implementation OPS
    template<class _T_>
    inline euclid::geos::point interface::centroid(const _T_& _object)
    {
      return euclid::geos::point(
        make_data(GEOSGetCentroid_r(euclid::geos::context::get(), _object.m_data.get())));
    }
    template<class _T_>
    inline euclid::simple::box interface::envelope(const _T_& _object)
    {
      return euclid::ops::impl::envelope(euclid::geos::geometry(
        make_data(GEOSEnvelope_r(euclid::geos::context::get(), _object.m_data.get()))));
    }
    template<class _T_>
    inline euclid::geos::geometry interface::offsetting(const _T_& _object, double _amount)
    {
      return euclid::geos::geometry(
        make_data(GEOSBuffer_r(euclid::geos::context::get(), _object.m_data.get(), _amount, 16)));
    }
    template<class _T1_>
    inline euclid::geos::multi_geometry interface::delaunay_triangulation(const _T1_& _t1,
                                                                          double _tol)
    {
      return euclid::geos::multi_geometry(make_data(
        GEOSDelaunayTriangulation_r(euclid::geos::context::get(), _t1.m_data.get(), _tol, 0)));
    }
    inline euclid::geos::multi_geometry
      interface::voronoi_diagram(const euclid::geos::multi_point& _source,
                                 const euclid::geos::geometry& _envelope, double _tol)
    {
      return euclid::geos::multi_geometry(make_data(GEOSVoronoiDiagram_r(
        euclid::geos::context::get(), _source.m_data.get(), _envelope.m_data.get(), _tol, 0)));
    }
    template<class _T1_>
    inline _T1_ interface::simplify(const _T1_& _source, double _tol)
    {
      return _T1_(
        make_data(GEOSSimplify_r(euclid::geos::context::get(), _source.m_data.get(), _tol)));
    }
    template<class _T_>
    inline euclid::simple::circle interface::maximum_inscribed_circle(const _T_& _object,
                                                                      double _tol)
    {
#ifdef EUCLID_GEOS_HAS_MAXIMUM_INSCRIBED_CIRCLE
      GEOSGeometry* geom
        = GEOSMaximumInscribedCircle_r(euclid::geos::context::get(), _object.m_data.get(), _tol);
      if(geom)
      {
        double xc, yc, zc;
        const GEOSCoordSequence* sequence
          = GEOSGeom_getCoordSeq_r(euclid::geos::context::get(), geom);
        GEOSCoordSeq_getX_r(euclid::geos::context::get(), sequence, 0, &xc);
        GEOSCoordSeq_getY_r(euclid::geos::context::get(), sequence, 0, &yc);
        GEOSCoordSeq_getZ_r(euclid::geos::context::get(), sequence, 0, &zc);
        double xb, yb, zb;
        GEOSCoordSeq_getX_r(euclid::geos::context::get(), sequence, 1, &xb);
        GEOSCoordSeq_getY_r(euclid::geos::context::get(), sequence, 1, &yb);
        GEOSCoordSeq_getZ_r(euclid::geos::context::get(), sequence, 1, &zb);

        euclid::simple::point center(xc, yc, zc);
        euclid::simple::point other(xb, yb, zb);
        GEOSFree_r(euclid::geos::context::get(), geom);
        return euclid::simple::circle(center, euclid::simple::distance<2>(center, other));
      }
      else
      {
        euclid_error("Failed to compute maximum inscribed circle for %{}", _object);
        return euclid::simple::circle();
      }
#else
      (void)_object;
      (void)_tol;
      euclid_fatal("maximum_inscribed_circle is not available with geos < 3.9.0");
      return euclid::simple::circle();
#endif
    }

    template<class _T_>
    inline euclid::simple::circle interface::minimum_enclosing_circle(const _T_& _object)
    {
      double radius;
      GEOSGeometry* center;
      GEOSFree_r(euclid::geos::context::get(),
                 GEOSMinimumBoundingCircle_r(euclid::geos::context::get(), _object.m_data.get(),
                                             &radius, &center));
      double x, y, z;
      GEOSGeomGetX_r(euclid::geos::context::get(), center, &x);
      GEOSGeomGetY_r(euclid::geos::context::get(), center, &y);
      GEOSGeomGetZ_r(euclid::geos::context::get(), center, &z);
      euclid::simple::circle c{{x, y, z}, radius};
      GEOSFree_r(euclid::geos::context::get(), center);
      return c;
    }
    template<class _T_>
    inline euclid::geos::polygon interface::minimum_rotated_rectangle(const _T_& _object)
    {
      return euclid::geos::polygon(make_data(
        GEOSMinimumRotatedRectangle_r(euclid::geos::context::get(), _object.m_data.get())));
    }

    // END Implementation OPS
    // END Implementation
  } // namespace details::geos
  // END details
  // BEGIN Interface with geos objects
  template<template<typename> class _T_>
  inline std::tuple<GEOSContextHandle_t, const GEOSGeometry*> to_geos(const _T_<euclid::geos>& _t)
  {
    return geos::interface::to_geos(_t);
  }
  inline GEOSContextHandle_HS* to_geos(const euclid::geos::context& _t)
  {
    return geos::interface::to_geos(_t);
  }
  /**
   * This function creates a euclid::geos::geometry object from a GEOSGeometry* object, and it
   * takes ownership of \p _geometry
   */
  inline euclid::geos::geometry from_geos(GEOSGeometry* _geometry)
  {
    return geos::interface::from_geos(_geometry);
  }
  // END Interface with gdal objects
} // namespace euclid

#undef __EUCLID_GEOS_HAS_JSON_VALUE__
