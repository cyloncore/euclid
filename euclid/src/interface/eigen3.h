#ifndef EUCLID_EIGEN3_HEADER
#include <Eigen/Core>
#include <euclid/simple>
#endif

namespace euclid::eigen3
{
  template<typename _T_>
  inline Eigen::Vector3d to_vector(const _T_& _pt)
  {
    return Eigen::Vector3d(_pt.x(), _pt.y(), _pt.z());
  }
  template<typename _T_>
  inline _T_ to_point(const Eigen::Vector2d& _v)
  {
    return _T_(_v.x(), _v.y());
  }
  template<typename _T_>
  inline _T_ to_point(const Eigen::Vector3d& _v)
  {
    return _T_(_v.x(), _v.y(), _v.z());
  }
  template<typename _T_>
  inline euclid::simple::segment to_segment(const _T_& _p1, const _T_& _p2)
  {
    return euclid::simple::segment(to_point<euclid::simple::point>(_p1),
                                   to_point<euclid::simple::point>(_p2));
  }
} // namespace euclid::eigen3
