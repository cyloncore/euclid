#ifndef EUCLID_GEOMETRY_HEADER

#include <proj.h>

#include <euclid/extra/transform>
#include <euclid/logging>

#endif

namespace euclid::proj
{
  /**
   * Apply the transformation between \p _P_src and \p _P_dst on the points of \p _geometry.
   * \p _swap indicates whether longitude and latitude should be swapped.
   */
  template<typename _T_>
  inline _T_ transform(const _T_& _geometry, PJ* _P_src, PJ* _P_dst, bool _swap = false)
  {
    PJ* P = proj_create_crs_to_crs_from_pj(PJ_DEFAULT_CTX, _P_src, _P_dst, NULL, NULL);

    if(P == NULL)
    {
      euclid_error("Failed to create transformation.");
      return _T_();
    }
    if(_swap)
    {
      PJ* nP = proj_normalize_for_visualization(PJ_DEFAULT_CTX, P);
      std::swap(nP, P);
      proj_destroy(nP);
    }

    _T_ t
      = euclid::extra::transform(_geometry,
                                 [_P_src, _P_dst, P](const euclid::simple::point& _pt)
                                 {
                                   double x = _pt.x();
                                   double y = _pt.y();
                                   double z = _pt.z();

                                   PJ_COORD c_in;
                                   c_in.xyzt.x = x;
                                   c_in.xyzt.y = y;
                                   c_in.xyzt.z = std::isnan(z) ? 0.0 : z;
                                   c_in.xyzt.t = 0.0;

                                   PJ_COORD c_out = proj_trans(P, PJ_FWD, c_in);

                                   return euclid::simple::point(c_out.xyzt.x, c_out.xyzt.y,
                                                                std::isnan(z) ? NAN : c_out.xyzt.z);
                                 });

    proj_destroy(P);

    return t;
  }
  template<typename _T_>
  inline _T_ transform(const _T_& _geometry, const std::string& _src_description,
                       const std::string& _dst_description, bool _swap = false)
  {
    PJ* P_src = proj_create(PJ_DEFAULT_CTX, _src_description.c_str());
    PJ* P_dst = proj_create(PJ_DEFAULT_CTX, _dst_description.c_str());
    _T_ t = transform(_geometry, P_src, P_dst, _swap);
    proj_destroy(P_src);
    proj_destroy(P_dst);
    return t;
  }

} // namespace euclid::proj
