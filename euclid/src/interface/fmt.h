
namespace euclid::fmt
{
  struct base_formatter
  {
    template<typename FormatContext>
    constexpr auto parse(FormatContext& ctx)
    {
      auto it = ctx.begin(), end = ctx.end();

      if(it != end && *it != '}')
        throw ::fmt::format_error("invalid format");

      return it;
    }
  };
  template<typename _T_>
  struct formatter
  {
    template<typename format_parse_context>
    constexpr auto parse(format_parse_context& ctx)
    {
      auto it = ctx.begin(), end = ctx.end();

      if(it != end and *it == '.')
      {
        precision = 0;

        ++it;

        while(it != end and *it >= '0' and *it < '9')
        {
          precision = (*it - '0') + 10 * precision;
          ++it;
        }
      }
      if(it != end and *it == '#')
      {
        ++it;

        dim = (*it - '0');

        if(dim != 2 and dim != 3)
        {
          throw ::fmt::format_error("invalid dimension, should be 2 or 3");
        }
        ++it;
      }

      if(it != end and *it != '}')
      {
        throw ::fmt::format_error("invalid format");
      }

      return it;
    }
    template<typename FormatContext>
    auto format(const _T_& p, FormatContext& ctx) -> decltype(ctx.out())
    {
      return ::fmt::formatter<std::string>().format(euclid::io::to_wkt(p, {precision, dim}), ctx);
    }
  private:
    int precision = 2;
    int dim = 2;
  };
} // namespace euclid::fmt

#define EUCLID_DEFINE_FORMATTER(_KLASS_)                                                           \
  template<typename _system_>                                                                      \
  struct fmt::formatter<euclid::geometry::_KLASS_<_system_>>                                       \
      : public euclid::fmt::formatter<euclid::geometry::_KLASS_<_system_>>                         \
  {                                                                                                \
  };

EUCLID_DEFINE_FORMATTER(geometry)
EUCLID_DEFINE_FORMATTER(polygon)
EUCLID_DEFINE_FORMATTER(line_string)
EUCLID_DEFINE_FORMATTER(linear_ring)
EUCLID_DEFINE_FORMATTER(point)
EUCLID_DEFINE_FORMATTER(multi_point)
EUCLID_DEFINE_FORMATTER(multi_geometry)
EUCLID_DEFINE_FORMATTER(multi_line_string)

template<>
struct fmt::formatter<euclid::simple::point> : public euclid::fmt::base_formatter
{
  template<typename FormatContext>
  auto format(const euclid::simple::point& p, FormatContext& ctx) -> decltype(ctx.out())
  {
    if(std::isnan(p.z()))
    {
      return format_to(ctx.out(), "({}, {})", p.x(), p.y());
    }
    else
    {
      return format_to(ctx.out(), "({}, {}, {})", p.x(), p.y(), p.z());
    }
  }
};

template<>
struct fmt::formatter<euclid::simple::segment> : public euclid::fmt::base_formatter
{
  template<typename FormatContext>
  auto format(const euclid::simple::segment& s, FormatContext& ctx) -> decltype(ctx.out())
  {
    return format_to(ctx.out(), "[{}, {}]", s.first(), s.second());
  }
};

#undef EUCLID_DEFINE_FORMATTER
