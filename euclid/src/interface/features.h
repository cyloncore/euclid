#pragma once

namespace euclid::details
{
  template<bool _has_json_>
  struct features
  {
    static const constexpr bool has_json = _has_json_;
  };
}; // namespace euclid::details
