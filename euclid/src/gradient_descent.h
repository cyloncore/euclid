namespace euclid::internal
{
  enum class gradient_descent_status
  {
    not_started = -2,
    running = -1,
    improper_input_parameters = 0,
    gamma_too_small = 4,
    too_many_function_evaluation = 5,
    x_tol_too_small = 7,
    user_asked = 9
  };

  template<typename FunctorType, typename Scalar = double>
  class gradient_descent
  {
    static Scalar sqrt_epsilon() { return std::sqrt(Eigen::NumTraits<Scalar>::epsilon()); }
  public:
    gradient_descent(FunctorType& _functor) : functor(_functor) { nfev = njev = iter = 0; }
    typedef Eigen::DenseIndex Index;
    struct Parameters
    {
      Parameters()
          : maxfev(400), xtol(sqrt_epsilon()), gamma(1), gammaAdaptation(0.9), momentum(0.0),
            nesterov(false), lineSearch(false)
      {
      }
      Index maxfev; // maximum number of function evaluation
      Scalar xtol;
      Scalar gamma;
      Scalar gammaAdaptation;
      Scalar momentum;
      bool nesterov;
      bool lineSearch;
    };

    typedef Eigen::Matrix<Scalar, Eigen::Dynamic, 1> FVectorType;
    typedef Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> DFVectorType;

    gradient_descent_status minimise(FVectorType& x);
    gradient_descent_status minimise_init(FVectorType& x);
    gradient_descent_status minimise_one_step(FVectorType& x);

    Parameters parameters;
    Scalar fvalue;
    FVectorType delta, prevErr;
    DFVectorType dfvec;
    Index iter;
    Index nfev, njev;
    Scalar gamma;
  private:
    FunctorType& functor;
    Index n;
  };

  template<typename FunctorType, typename Scalar>
  gradient_descent_status gradient_descent<FunctorType, Scalar>::minimise(FVectorType& x)
  {
    gradient_descent_status status = minimise_init(x);
    if(status == gradient_descent_status::improper_input_parameters)
      return status;
    do
    {
      status = minimise_one_step(x);
    } while(status == gradient_descent_status::running);
    return status;
  }

  template<typename FunctorType, typename Scalar>
  gradient_descent_status gradient_descent<FunctorType, Scalar>::minimise_init(FVectorType& x)
  {
    n = x.size();
    euclid_assert(functor.values() == 1); // Gradient descent only works with 1-dimension values

    njev = 0;
    nfev = 0;

    gamma = parameters.gamma;
    delta.resize(n, 1);

    dfvec.resize(1, n);

    if(gamma < 0 || parameters.gammaAdaptation < 0)
    {
      return gradient_descent_status::improper_input_parameters;
    }

    if(parameters.lineSearch)
    {
      prevErr.resize(1, 1);
      functor(x, prevErr);
      ++nfev;
    }

    /*     initialize iteration counter. */
    iter = 1;

    return gradient_descent_status::not_started;
  }

  template<typename FunctorType, typename Scalar>
  gradient_descent_status gradient_descent<FunctorType, Scalar>::minimise_one_step(FVectorType& x)
  {
    euclid_assert(x.size() == n); // check the caller is not cheating us

    /* calculate the derivative. */
    Index df_ret = functor.df(parameters.nesterov ? x + parameters.momentum * delta : x, dfvec);
    if(df_ret < 0)
      return gradient_descent_status::user_asked;
    if(df_ret > 0)
      // numerical diff, we evaluated the function df_ret times
      nfev += df_ret;
    else
      njev++;

    if(parameters.lineSearch)
    {
      FVectorType nx;
      FVectorType err;
      err.resize(1, 1);
      while(nfev < parameters.maxfev)
      {
        delta = parameters.momentum * delta - gamma * dfvec.transpose();
        nx = x + delta;
        if(gamma < Eigen::NumTraits<Scalar>::epsilon())
        {
          return gradient_descent_status::gamma_too_small;
        }
        functor(nx, err);
        ++nfev;
        if(err(0) < prevErr(0))
        {
          std::swap(prevErr, err);
          break;
        }
        else
        {
          gamma *= parameters.gammaAdaptation;
        }
      }
      if(nfev >= parameters.maxfev)
        return gradient_descent_status::too_many_function_evaluation;
      x = nx;
    }
    else
    {
      delta = parameters.momentum * delta - gamma * dfvec.transpose();
      gamma *= parameters.gammaAdaptation;
      if(gamma < Eigen::NumTraits<Scalar>::epsilon())
      {
        return gradient_descent_status::gamma_too_small;
      }
      x += delta;
    }
    ++iter;
    Scalar deltan = delta.stableNorm();
    if(deltan < parameters.xtol)
      return gradient_descent_status::user_asked;
    else if(nfev >= parameters.maxfev)
      return gradient_descent_status::too_many_function_evaluation;
    else
      return gradient_descent_status::running;
  }
} // namespace euclid::internal
