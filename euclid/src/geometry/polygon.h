#ifndef EUCLID_GEOMETRY_HEADER
#include <algorithm>

#include "linear_ring.h"
#endif

namespace euclid::geometry
{
  template<typename _system_>
  class polygon : object<_system_, polygon>
  {
    EUCLID_CLASS_DEFINITION_COMMON_GEOMETRY_GEOMETRY_BUILDER(polygon, system)
    using linear_ring = euclid::geometry::linear_ring<system>;
    using linear_ring_data = typename interface::template traits<linear_ring>::data;
    static std::vector<typename linear_ring::data_type>
      get_m_data(const std::vector<linear_ring>& _interior_rings)
    {
      std::vector<typename linear_ring::data_type> r;
      std::transform(_interior_rings.begin(), _interior_rings.end(), std::back_inserter(r),
                     [](const linear_ring& _lr) { return _lr.m_data; });
      return r;
    }
  public:
    polygon(const linear_ring& _exterior_ring,
            const std::vector<linear_ring>& _interior_rings = std::vector<linear_ring>())
        : object_type(interface::create_polygon(_exterior_ring.m_data, get_m_data(_interior_rings)))
    {
    }
    polygon(const polygon& _rhs) : object_type(_rhs) {}
    polygon& operator=(const polygon& _rhs)
    {
      object_type::operator=(_rhs);
      return *this;
    }
    ~polygon() {}
    linear_ring exterior_ring() const
    {
      return linear_ring(interface::polygon_get_exterior_ring(object_type::m_data));
    }
    std::vector<linear_ring> interior_rings() const
    {
      std::vector<linear_ring_data> hs = interface::polygon_get_interior_rings(object_type::m_data);
      std::vector<linear_ring> res;
      std::transform(hs.begin(), hs.end(), std::back_inserter(res),
                     [](const linear_ring_data& _lrd) { return linear_ring(_lrd); });
      return res;
    }
    std::size_t interior_rings_count() const
    {
      return interface::polygon_get_interior_rings_count(object_type::m_data);
    }
    std::size_t dimensions() const { return interface::get_dimensions(object_type::m_data); }
    bool is_empty() const { return interface::is_empty(object_type::m_data); }
  };
  namespace details
  {
    template<typename _system_>
    class polygon_builder
    {
    public:
      using polygon = euclid::geometry::polygon<_system_>;
      using linear_ring_builder = typename linear_ring<_system_>::builder;
    public:
      polygon_builder() : m_data(new data) {}
      ~polygon_builder() {}
      polygon create() const
      {
        std::vector<const vector_of_points*> holes;
        std::size_t dimensions = m_data->shell.m_dimensions;
        for(const linear_ring_builder& lrb : m_data->holes)
        {
          holes.push_back(lrb.m_data.get());
          dimensions = std::max(lrb.m_dimensions, dimensions);
        }
        return polygon(
          _system_::interface::create_polygon(*m_data->shell.m_data, holes, dimensions));
      }
      linear_ring_builder& shell() { return m_data->shell; }
      linear_ring_builder& create_interior_ring()
      {
        m_data->holes.push_back(linear_ring_builder());
        return *(--m_data->holes.end());
      }
      polygon_builder& add_point(double _x, double _y, double _z = NAN)
      {
        m_data->current.add_point(_x, _y, _z);
        return *this;
      }
      template<typename _T_>
      polygon_builder& add_point(const _T_& _pt)
      {
        m_data->current.add_point(_pt);
        return *this;
      }
      polygon_builder& start_interior_ring()
      {
        m_data->current = create_interior_ring();
        return *this;
      }
    private:
      struct data
      {
        data() : shell(), current(shell) {}
        linear_ring_builder shell;
        std::vector<linear_ring_builder> holes;
        linear_ring_builder current;
      };
      std::shared_ptr<data> m_data;
    };
  } // namespace details
} // namespace euclid::geometry
