#ifndef EUCLID_GEOMETRY_HEADER
#include "object.h"
#include <euclid/simple>
#endif

namespace euclid::geometry
{
  template<typename _T_>
  class point_iterator : object<typename _T_::system, point_iterator, _T_>
  {
  public:
    using geometry_type = _T_;
  public: // iterators
    using difference_type = std::ptrdiff_t;
    using value_type = euclid::simple::point;
    using pointer = value_type*;
    using reference = value_type;
    using iterator_category = std::bidirectional_iterator_tag;
  public:
    EUCLID_CLASS_DEFINITION_COMMON_CONTAINER(point_iterator, _T_)
    friend class details::points_helper<_T_>;
  public:
    euclid::simple::point operator*() const
    {
      return interface::points_at(object_type::m_data, m_index);
    }
    point_iterator& operator++()
    {
      ++m_index;
      return *this;
    }
    point_iterator& operator--()
    {
      --m_index;
      return *this;
    }
    difference_type operator-(const point_iterator& _rhs) const { return m_index - _rhs.m_index; }
    bool operator==(const point_iterator& _rhs) const
    {
      return object_type::m_data == _rhs.m_data and m_index == _rhs.m_index;
    }
    bool operator!=(const point_iterator& _rhs) const { return not(*this == _rhs); }
  private:
    point_iterator set_index(const std::size_t _index)
    {
      m_index = _index;
      return *this;
    }
    std::size_t m_index;
  };
  template<typename _system_>
  using point_circular_iterator
    = details::circular_iterator<euclid::geometry::point_iterator<linear_ring<_system_>>>;
} // namespace euclid::geometry
