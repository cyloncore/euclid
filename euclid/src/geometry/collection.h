#ifndef EUCLID_GEOMETRY_HEADER
#include <memory>
#include <tuple>
#include <variant>

#include "point.h"
#endif

namespace euclid::geometry
{
  namespace details
  {
    template<typename _T_>
    class collection_iterator
    {
    public: // iterators
      using difference_type = long;
      using value_type = _T_;
      using pointer = value_type*;
      using reference = value_type;
      using iterator_category = std::bidirectional_iterator_tag;
    public:
      collection_iterator() {}
      collection_iterator(const collection<_T_>& _data, std::size_t _index)
          : m_data(_data), m_index(_index)
      {
      }
      ~collection_iterator() {}
      _T_ operator*() const { return m_data.at(m_index); }
      collection_iterator& operator++()
      {
        ++m_index;
        return *this;
      }
      collection_iterator& operator--()
      {
        --m_index;
        return *this;
      }
      bool operator==(const collection_iterator& _rhs) const
      {
        return m_data.m_data == _rhs.m_data.m_data and m_index == _rhs.m_index;
      }
      bool operator!=(const collection_iterator& _rhs) const { return not(*this == _rhs); }
    private:
      collection<_T_> m_data;
      std::size_t m_index;
    };

    template<typename _Target_>
    class multi_geometry_iterator
    {
      using system = typename _Target_::system;
    public: // iterators
      using difference_type = long;
      using value_type = _Target_;
      using pointer = value_type*;
      using reference = value_type;
      using iterator_category = std::bidirectional_iterator_tag;
    public:
      multi_geometry_iterator() {}
      multi_geometry_iterator(const collection<geometry<system>>& _data, std::size_t _index,
                              std::size_t _end)
          : m_data(_data), m_index(_index), m_end(_end)
      {
      }
      ~multi_geometry_iterator() {}
      value_type operator*() const { return m_data.at(m_index).template cast<value_type>(); }
      multi_geometry_iterator& operator++()
      {
        while(m_index < m_end)
        {
          ++m_index;
          if(m_index == m_end or m_data.at(m_index).template is<value_type>())
            break;
        }
        return *this;
      }
      multi_geometry_iterator& operator--()
      {
        while(m_index >= 0)
        {
          --m_index;
          if(m_index < 0 or m_data.at(m_index).template is<value_type>())
            break;
        }
        return *this;
      }
      bool operator==(const multi_geometry_iterator& _rhs) const
      {
        return m_data.m_data == _rhs.m_data.m_data and m_index == _rhs.m_index;
      }
      bool operator!=(const multi_geometry_iterator& _rhs) const { return not(*this == _rhs); }
    private:
      collection<geometry<system>> m_data;
      std::size_t m_index;
      std::size_t m_end;
    };
    template<typename _T_>
    class collection : object<typename _T_::system, collection, _T_>
    {
      EUCLID_CLASS_DEFINITION_COMMON_CONTAINER_BUILDER(collection, _T_)
      friend class multi_geometry<system>;
    public:
      using value_type = _T_;
      using iterator = details::collection_iterator<_T_>;
      friend iterator;
    public:
      using object_type::object_type;
      static collection create_empty() { return collection(interface::create_empty_collection()); }
      ~collection() {}
      std::size_t size() const { return interface::collection_size(object_type::m_data); }
      _T_ at(std::size_t _index) const
      {
        return _T_(interface::collection_at(object_type::m_data, _index));
      }
      iterator begin() const { return iterator(*this, 0); }
      iterator end() const { return iterator(*this, size()); }
      std::size_t dimensions() const { return interface::get_dimensions(object_type::m_data); }
      bool is_empty() const { return interface::is_empty(object_type::m_data); }
    };
  } // namespace details
  template<typename _system_>
  class multi_geometry : public details::collection<geometry<_system_>>
  {
    using super = details::collection<geometry<_system_>>;
  public:
    template<typename _T_>
    using geometry_iterator = details::multi_geometry_iterator<_T_>;
  public:
    using super::super;
    static multi_geometry create_empty()
    {
      return multi_geometry(super::interface::create_empty_collection());
    }
    multi_geometry(const super& _super) : super(_super) {}
    ~multi_geometry() {}

    using super::begin;
    using super::end;

    template<typename _T_>
    geometry_iterator<_T_> begin() const
    {
      return geometry_iterator<_T_>(*this, 0, this->size());
    }
    template<typename _T_>
    geometry_iterator<_T_> end() const
    {
      std::size_t s = this->size();
      return geometry_iterator<_T_>(*this, s, s);
    }
  };

  namespace details
  {
    template<typename _T_>
    class collection_builder
    {
      using system = typename _T_::system;
      using interface = typename system::interface;
      using T_builder = typename _T_::builder;
      using data = std::vector<std::variant<T_builder, _T_>>;
    public:
      collection_builder() : m_data(std::shared_ptr<data>(new data)) {}
      ~collection_builder() {}
      collection<_T_> create() const
      {
        std::vector<_T_> elements;

        for(const std::variant<T_builder, _T_>& var : *m_data)
        {
          if(std::holds_alternative<T_builder>(var))
          {
            elements.push_back(std::get<T_builder>(var).create());
          }
          else
          {
            elements.push_back(std::get<_T_>(var));
          }
        }

        return collection<_T_>(interface::create_collection(elements));
      }
      T_builder create_element()
      {
        T_builder b;
        m_data->push_back(b);
        return b;
      }
      collection_builder& add_element(const _T_& _element)
      {
        m_data->push_back(_element);
        return *this;
      }
    private:
      std::shared_ptr<data> m_data;
    };

    template<typename _system_>
    class collection_builder<point<_system_>>
    {
      using system = _system_;
      using interface = typename system::interface;
    public:
      collection_builder() : m_data(new details::vector_of_points) {}
      ~collection_builder() {}
      collection<point<system>> create() const
      {
        return collection<point<system>>(interface::create_collection(m_data));
      }
      collection_builder& add_point(double _x, double _y, double _z = NAN)
      {
        m_data->push_back(euclid::simple::point(_x, _y, _z));
        return *this;
      }
      template<typename _T_>
      collection_builder& add_element(const _T_& _t)
      {
        m_data->push_back(_t);
        return *this;
      }
    private:
      std::shared_ptr<details::vector_of_points> m_data;
    };

    template<typename _system_>
    class collection_builder<geometry<_system_>>
    {
      using system = _system_;
      using interface = typename system::interface;
    public:
      collection_builder() : m_data(std::make_shared<std::vector<geometry<system>>>()) {}
      ~collection_builder() {}
      collection<geometry<system>> create() const
      {
        return collection<geometry<system>>(interface::create_collection(*m_data));
      }
      collection_builder& add_element(const geometry<system>& _element)
      {
        m_data->push_back(_element);
        return *this;
      }
    private:
      std::shared_ptr<std::vector<geometry<system>>> m_data;
    };

  } // namespace details
} // namespace euclid::geometry
