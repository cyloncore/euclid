namespace euclid::geometry
{
  template<typename _T_>
  class point_iterator;
  template<typename _T_>
  class segment_iterator;
} // namespace euclid::geometry

namespace euclid::geometry::details
{
  template<typename _It_>
  struct get_helper;
  template<typename _T_>
  struct get_helper<point_iterator<_T_>>
  {
    using iterator = point_iterator<_T_>;
    using geometry_type = typename iterator::geometry_type;
    auto operator()(const geometry_type& _g) { return _g.points(); }
    static constexpr int end_offset = 1;
  };
  template<typename _T_>
  struct get_helper<segment_iterator<_T_>>
  {
    using iterator = point_iterator<_T_>;
    using geometry_type = typename iterator::geometry_type;
    auto operator()(const geometry_type& _g) { return _g.segments(); }
    static constexpr int end_offset = 0;
  };
  template<typename _It_>
  class circular_iterator
  {
    using iterator = _It_;
  public: // iterators
    using difference_type = long;
    using value_type = typename iterator::value_type;
    using pointer = value_type*;
    using reference = value_type&;
    using iterator_category = std::bidirectional_iterator_tag;
  public:
    circular_iterator(const iterator& _begin, const iterator& _end)
        : m_begin(_begin), m_end(_end), m_current(_begin)
    {
    }
    template<typename _geometry_type_>
    circular_iterator(const _geometry_type_& _geometry)
        : circular_iterator(
            get_helper<_It_>()(_geometry).begin(),
            std::prev(get_helper<_It_>()(_geometry).end(), get_helper<_It_>::end_offset))
    {
      static_assert(std::is_same_v<_geometry_type_, typename iterator::geometry_type>);
    }
    value_type operator*() const { return *m_current; }
    circular_iterator& operator++()
    {
      ++m_current;
      if(m_current == m_end)
      {
        m_current = m_begin;
        ++m_full_circles;
      }
      return *this;
    }
    circular_iterator& operator--()
    {
      if(m_current == m_begin)
      {
        m_current = m_end;
        --m_full_circles;
      }
      --m_current;
      return *this;
    }

    bool operator==(const circular_iterator& _rhs) const { return m_current == _rhs.m_current; }
    bool operator!=(const circular_iterator& _rhs) const { return not(*this == _rhs); }
    difference_type operator-(const circular_iterator& _rhs) const
    {
      difference_type total_length = m_end - m_begin;
      return (m_full_circles - _rhs.m_full_circles) * total_length + (m_current - _rhs.m_current);
    }
    int full_circles() const { return m_full_circles; }
    operator iterator() const { return m_current; }
  private:
    iterator m_begin, m_end, m_current;
    int m_full_circles = 0;
  };
} // namespace euclid::geometry::details
