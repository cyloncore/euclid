namespace euclid::geometry
{
  template<typename _DT_, typename _ST_>
  _DT_ cast(const _ST_& _st);
  namespace details
  {
    template<typename _DT_, typename _ST_, class _Enable_ = void>
    struct cast
    {
      _DT_ operator()(const _ST_&) { return _DT_(); }
    };
    template<typename _T_>
    struct cast<_T_, _T_>
    {
      _T_ operator()(const _T_& _t) { return _t; }
    };
    template<typename _DT_>
      requires(traits::is_collection_v<_DT_>)
    struct cast<_DT_, euclid::geometry::multi_geometry<typename _DT_::system>>
    {
      _DT_ operator()(const euclid::geometry::multi_geometry<typename _DT_::system>& _geometry)
      {
        typename _DT_::builder dt;
        for(auto it = _geometry.begin(); it != _geometry.end(); ++it)
        {
          dt.add_element((*it).template cast<typename _DT_::value_type>());
        }
        return dt.create();
      }
    };
    template<typename _DT_>
      requires(not traits::is_geometry_v<_DT_>)
    struct cast<_DT_, euclid::geometry::geometry<typename _DT_::system>>
    {
      using system = typename _DT_::system;
      _DT_ operator()(const euclid::geometry::geometry<system>& _geometry)
      {
        if(_geometry.template is<_DT_>())
          return _geometry.template cast<_DT_>();
        if(_geometry.template is<multi_geometry<system>>())
          return euclid::geometry::cast<_DT_>(_geometry.template cast<multi_geometry<system>>());
        return _DT_();
      }
    };
    template<typename _ST_>
      requires(not traits::is_geometry_v<_ST_>)
    struct cast<euclid::geometry::geometry<typename _ST_::system>, _ST_>
    {
      euclid::geometry::geometry<typename _ST_::system> operator()(const _ST_& _geometry)
      {
        return euclid::geometry::geometry<typename _ST_::system>(_geometry);
      }
    };
  } // namespace details
  template<typename _DT_, typename _ST_>
  _DT_ cast(const _ST_& _st)
  {
    static_assert(std::is_same_v<typename _DT_::system, typename _ST_::system>);
    return details::cast<_DT_, _ST_>()(_st);
  }

  template<typename _DT_>
  class try_cast_to
  {
  public:
    template<typename _ST_>
    try_cast_to(const _ST_& _st)
    {
      static_assert(std::is_same_v<typename _DT_::system, typename _ST_::system>);
      if(_st.template is<_DT_>())
      {
        m_success = true;
        m_t = _st.template cast<_DT_>();
      }
      else
      {
        m_success = false;
      }
    }
    operator bool() { return m_success; }
    const _DT_& value() const
    {
      euclid_assert(m_success);
      return m_t;
    }
    _DT_& value()
    {
      euclid_assert(m_success);
      return m_t;
    }
    operator const _DT_&() const { return value(); }
    _DT_* operator->() { return &value(); }
    _DT_* operator->() const { return &value(); }
  private:
    bool m_success;
    _DT_ m_t;
  };
} // namespace euclid::geometry
