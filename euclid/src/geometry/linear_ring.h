#ifndef EUCLID_GEOMETRY_HEADER
#include <cmath>
#include <memory>

#include "object.h"
#endif

namespace euclid::geometry
{
  template<typename _system_>
  class linear_ring : object<_system_, linear_ring>
  {
    EUCLID_CLASS_DEFINITION_COMMON_GEOMETRY_GEOMETRY_BUILDER(linear_ring, system)
    using polygon = euclid::geometry::polygon<system>;
    friend polygon;
  public:
    linear_ring(const linear_ring& _rhs) : object_type(_rhs) {}
    linear_ring& operator=(const linear_ring& _rhs)
    {
      object_type::operator=(_rhs);
      return *this;
    }
    ~linear_ring() {}
    details::points_helper<linear_ring<system>> points() const
    {
      return details::points_helper<linear_ring<system>>(object_type::m_data);
    }
    details::segments_helper<linear_ring<system>> segments() const
    {
      return details::segments_helper<linear_ring<system>>(object_type::m_data);
    }
    std::size_t dimensions() const { return interface::get_dimensions(object_type::m_data); }
    bool is_empty() const { return interface::is_empty(object_type::m_data); }
  };
  namespace details
  {
    template<typename _system_>
    class linear_ring_builder
    {
      friend class details::polygon_builder<_system_>;
    public:
      using linear_ring = euclid::geometry::linear_ring<_system_>;
    public:
      linear_ring_builder() : m_data(new details::vector_of_points) {}
      ~linear_ring_builder() {}
      linear_ring create() const
      {
        return linear_ring(_system_::interface::create_linear_ring(*m_data, m_dimensions));
      }
      linear_ring_builder& add_point(double _x, double _y, double _z = NAN)
      {
        if(not std::isnan(_z))
          m_dimensions = 3;
        return add_point(euclid::simple::point(_x, _y, _z));
      }
      template<typename _T_>
      linear_ring_builder& add_point(const _T_& _pt)
      {
        m_dimensions = std::max(m_dimensions, _pt.dimensions());
        m_data->push_back(_pt);
        return *this;
      }
      template<typename _T_>
      linear_ring_builder& add_points(const std::vector<_T_>& _pts)
      {
        for(const _T_& t : _pts)
        {
          add_point(t);
        }
        return *this;
      }
    private:
      std::shared_ptr<details::vector_of_points> m_data;
      std::size_t m_dimensions = 2;
    };
  } // namespace details
} // namespace euclid::geometry
