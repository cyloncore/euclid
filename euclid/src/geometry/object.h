#ifndef EUCLID_GEOMETRY_HEADER
#include "global.h"
#include "macros.h"
#endif

namespace euclid::geometry
{
  namespace details
  {
    template<typename _interface_, template<typename...> class _derived_, typename... _args_>
    struct data_type
    {
      using type = typename _interface_::template traits<_derived_<_args_...>>::data;
    };
    template<typename _interface_, typename _T_>
    struct data_type<_interface_, points_helper, _T_>
    {
      using type = typename _interface_::template traits<_T_>::data;
    };
    template<typename _interface_, typename _T_>
    struct data_type<_interface_, segments_helper, _T_>
    {
      using type = typename _interface_::template traits<_T_>::data;
    };
    template<typename _interface_>
    struct data_type<_interface_, details::collection, geometry<typename _interface_::system>>
    {
      using type =
        typename _interface_::template traits<typename _interface_::system::multi_geometry>::data;
    };
    template<typename _interface_, template<typename...> class _derived_, typename... _args_>
    using data_type_t = typename data_type<_interface_, _derived_, _args_...>::type;
  } // namespace details
  template<typename _system_, template<typename...> class _derived_, typename _arg_1_ = _system_,
           typename... _args_>
  class object
  {
  protected:
    using system = _system_;
    using interface = typename system::interface;
    friend interface;
    using geometry_type = euclid::geometry::geometry<_system_>;
    friend geometry_type;
    using data_type = details::data_type_t<interface, _derived_, _arg_1_, _args_...>;
    object(const data_type& _data) : m_data(_data) {}
  public:
    object() {}
    object(const object& _rhs) : m_data(_rhs.m_data) {}
    object& operator=(const object& _rhs)
    {
      m_data = _rhs.m_data;
      return *this;
    }
    ~object() {}
    bool is_null() const { return not m_data; }
  protected:
    data_type m_data;
  };
} // namespace euclid::geometry
