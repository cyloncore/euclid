#ifndef EUCLID_GEOMETRY_HEADER
#include "object.h"
#endif

namespace euclid::geometry
{
  template<typename _system_>
  class geometry : object<_system_, geometry>
  {
    EUCLID_CLASS_DEFINITION_COMMON_GEOMETRY(geometry, system)
    template<class _TOther_>
    void assign(const _TOther_& _rhs)
    {
      object_type::m_data = _rhs.m_data;
    }
    using point = euclid::geometry::point<system>;
    using line_string = euclid::geometry::line_string<system>;
    using linear_ring = euclid::geometry::linear_ring<system>;
    using polygon = euclid::geometry::polygon<system>;
    using multi_geometry = euclid::geometry::multi_geometry<system>;
    using multi_point = euclid::geometry::multi_point<system>;
    using multi_line_string = euclid::geometry::multi_line_string<system>;
    using multi_polygon = euclid::geometry::multi_polygon<system>;
  public:
    template<template<typename _I_> class _TOther_>
      requires traits::is_a_geometry_v<_TOther_<_system_>>
    geometry(const _TOther_<_system_>& _rhs)
    {
      assign(_rhs);
    }
    template<template<typename _T_> class _C_, template<typename _I_> class _TOther_>
      requires traits::is_a_geometry_v<_TOther_<_system_>>
    geometry(const _C_<_TOther_<_system_>>& _rhs)
    {
      assign(_rhs);
    }
    geometry(const geometry& _rhs) : object_type(_rhs) {}
    geometry& operator=(const geometry& _rhs)
    {
      object_type::operator=(_rhs);
      return *this;
    }
    template<template<typename _I_> class _TOther_>
    geometry& operator=(const _TOther_<_system_>& _rhs)
    {
      assign(_rhs);
      return *this;
    }
    ~geometry() {}
    const char* type_name() const
    {
      if(is<multi_polygon>())
        return "multi_polygon";
      if(is<multi_line_string>())
        return "multi_line_string";
      if(is<multi_point>())
        return "multi_point";
      if(is<multi_geometry>())
        return "multi_geometry";
      if(is<polygon>())
        return "polygon";
      if(is<linear_ring>())
        return "linear_ring";
      if(is<line_string>())
        return "line_string";
      if(is<point>())
        return "point";
      return "null";
    }
  public:
    template<typename _T_>
    bool is() const
    {
      return object_type::m_data and interface::template geometry_is<_T_>(object_type::m_data);
    }
    bool is_collection() const { return interface::geometry_is_collection(object_type::m_data); }
    template<typename _T_>
    _T_ cast() const
    {
      return _T_(interface::template geometry_cast<_T_>(object_type::m_data));
    }
    std::size_t dimensions() const { return interface::get_dimensions(object_type::m_data); }
    bool is_empty() const { return interface::is_empty(object_type::m_data); }
  };
} // namespace euclid::geometry
