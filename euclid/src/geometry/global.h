#ifndef EUCLID_GEOMETRY_HEADER
#include <vector>
namespace euclid::simple
{
  class point;
}
#endif

namespace euclid::geometry
{
  template<typename _system_>
  class geometry;
  template<typename _system_>
  class line_string;
  template<typename _system_>
  class linear_ring;
  template<typename _system_>
  class polygon;
  template<typename _system_>
  class multi_geometry;
  namespace details
  {
    template<typename _system_>
    class line_string_builder;
    template<typename _system_>
    class linear_ring_builder;
    template<typename _system_>
    class polygon_builder;
    template<typename _T_>
    class collection_builder;
    template<typename _T_>
    class points_helper;
    template<typename _T_>
    class segments_helper;
    template<typename _system_>
    class collection;

    using vector_of_points = std::vector<euclid::simple::point>;
  } // namespace details
  template<typename _system_>
  using multi_point = details::collection<point<_system_>>;
  template<typename _system_>
  using multi_line_string = details::collection<line_string<_system_>>;
  template<typename _system_>
  using multi_polygon = details::collection<polygon<_system_>>;
} // namespace euclid::geometry
