#ifdef EUCLID_SETUP_MACROS

#define EUCLID_CLASS_DEFINITION_COMMON(_KLASS_, _ARG_)                                             \
private:                                                                                           \
  using object_type = euclid::geometry::object<system, _KLASS_, _ARG_>;                            \
private:                                                                                           \
  using data_type = details::data_type_t<interface, _KLASS_, _ARG_>;                               \
  friend interface;                                                                                \
  _KLASS_(const data_type& _data) : object_type(_data)                                             \
  {                                                                                                \
    if constexpr(traits::is_a_geometry_v<_KLASS_>)                                                 \
    {                                                                                              \
      euclid_assert(interface::template geometry_is<_KLASS_>(_data));                              \
    }                                                                                              \
  }                                                                                                \
public:                                                                                            \
  _KLASS_() : object_type() {}                                                                     \
  using object_type::is_null;                                                                      \
private:

#define EUCLID_CLASS_DEFINITION_BUILDER(_KLASS_, _ARG_)                                            \
public:                                                                                            \
  using builder = details::_KLASS_##_builder<_ARG_>;                                               \
private:                                                                                           \
  friend builder;

#define EUCLID_CLASS_DEFINITION_COMMON_CONTAINER(_KLASS_, _ARG_)                                   \
public:                                                                                            \
  using interface = typename _ARG_::interface;                                                     \
  using system = typename interface::system;                                                       \
  EUCLID_CLASS_DEFINITION_COMMON(_KLASS_, _ARG_)

#define EUCLID_CLASS_DEFINITION_COMMON_CONTAINER_BUILDER(_KLASS_, _ARG_)                           \
  EUCLID_CLASS_DEFINITION_COMMON_CONTAINER(_KLASS_, _ARG_)                                         \
  EUCLID_CLASS_DEFINITION_BUILDER(_KLASS_, _ARG_)                                                  \
public:                                                                                            \
  using geometry_type = typename object_type::geometry_type;                                       \
  friend geometry_type;                                                                            \
private:

#define EUCLID_CLASS_DEFINITION_COMMON_GEOMETRY(_KLASS_, _ARG_)                                    \
public:                                                                                            \
  using system = _system_;                                                                         \
  using interface = typename system::interface;                                                    \
  EUCLID_CLASS_DEFINITION_COMMON(_KLASS_, _ARG_)                                                   \
  friend details::collection<_KLASS_>;

#define EUCLID_CLASS_DEFINITION_COMMON_GEOMETRY_GEOMETRY(_KLASS_, _ARG_)                           \
  EUCLID_CLASS_DEFINITION_COMMON_GEOMETRY(_KLASS_, _ARG_)                                          \
private:                                                                                           \
  using geometry_type = typename object_type::geometry_type;                                       \
  friend geometry_type;

#define EUCLID_CLASS_DEFINITION_COMMON_GEOMETRY_GEOMETRY_BUILDER(_KLASS_, _ARG_)                   \
  EUCLID_CLASS_DEFINITION_COMMON_GEOMETRY_GEOMETRY(_KLASS_, _ARG_)                                 \
  EUCLID_CLASS_DEFINITION_BUILDER(_KLASS_, _ARG_)

#else
#ifdef EUCLID_UNSETUP_MACROS
#undef EUCLID_CLASS_DEFINITION_COMMON
#undef EUCLID_CLASS_DEFINITION_COMMON_CONTAINER
#undef EUCLID_CLASS_DEFINITION_COMMON_GEOMETRY
#undef EUCLID_CLASS_DEFINITION_COMMON_GEOMETRY_GEOMETRY
#undef EUCLID_CLASS_DEFINITION_COMMON_GEOMETRY_GEOMETRY_BUILDER
#else
#error "Invalid usage of macros.h"
#endif
#endif
