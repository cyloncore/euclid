#ifndef EUCLID_GEOMETRY_HEADER
#include "object.h"
#include "point_iterator.h"
#include <euclid/simple>
#endif

namespace euclid::geometry
{
  template<typename _T_>
  class segment_iterator
  {
    using point_iterator = euclid::geometry::point_iterator<_T_>;
  public:
    using geometry_type = _T_;
  public: // iterators
    using difference_type = std::ptrdiff_t;
    using value_type = euclid::simple::segment;
    using pointer = value_type*;
    using reference = value_type;
    using iterator_category = std::bidirectional_iterator_tag;
  private:
    friend class details::segments_helper<_T_>;
    segment_iterator(const point_iterator& _first, const point_iterator& _second)
        : m_first(_first), m_second(_second)
    {
    }
  public:
    euclid::simple::segment operator*() const
    {
      return euclid::simple::segment(*m_first, *m_second);
    }
    segment_iterator& operator++()
    {
      ++m_first;
      ++m_second;
      return *this;
    }
    segment_iterator& operator--()
    {
      --m_first;
      --m_second;
      return *this;
    }
    difference_type operator-(const segment_iterator& _rhs) const { return m_first - _rhs.m_first; }
    bool operator==(const segment_iterator& _rhs) const { return m_second == _rhs.m_second; }
    bool operator!=(const segment_iterator& _rhs) const { return not(*this == _rhs); }
  private:
    point_iterator m_first, m_second;
  };
  namespace details
  {
    template<typename _T_>
    class segments_helper : public object<typename _T_::system, segments_helper, _T_>
    {
      EUCLID_CLASS_DEFINITION_COMMON_CONTAINER(segments_helper, _T_)
      friend _T_;
    public:
      segment_iterator<_T_> begin() const
      {
        auto it = points_helper<_T_>(object_type::m_data).begin();
        auto it1 = it;
        return segment_iterator(it, ++it1);
      }
      segment_iterator<_T_> end() const
      {
        auto it = points_helper<_T_>(object_type::m_data).end();
        auto it1 = it;
        return segment_iterator<_T_>(--it1, it);
      }
      operator std::vector<euclid::simple::segment>() const
      {
        std::vector<euclid::simple::segment> r;
        r.insert(r.begin(), begin(), end());
        return r;
      }
    };
  } // namespace details
  template<typename _system_>
  using segment_circular_iterator
    = details::circular_iterator<euclid::geometry::segment_iterator<linear_ring<_system_>>>;
} // namespace euclid::geometry
