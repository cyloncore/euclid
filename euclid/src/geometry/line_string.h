#ifndef EUCLID_GEOMETRY_HEADER
#include "object.h"
#include <euclid/simple>
#endif

namespace euclid::geometry
{
  template<typename _system_>
  class line_string : object<_system_, line_string>
  {
    EUCLID_CLASS_DEFINITION_COMMON_GEOMETRY_GEOMETRY_BUILDER(line_string, system)
  public:
    line_string(const euclid::simple::segment& _segment) : object_type() { assign(_segment); }
    line_string(const line_string& _rhs) : object_type(_rhs) {}
    line_string& operator=(const euclid::simple::segment& _segment)
    {
      assign(_segment);
      return *this;
    }
    line_string& operator=(const line_string& _rhs)
    {
      object_type::operator=(_rhs);
      return *this;
    }
    ~line_string() {}
    details::points_helper<line_string<_system_>> points() const
    {
      return details::points_helper<line_string<_system_>>(object_type::m_data);
    }
    details::segments_helper<line_string<_system_>> segments() const
    {
      return details::segments_helper<line_string<_system_>>(object_type::m_data);
    }
    std::size_t dimensions() const { return interface::get_dimensions(object_type::m_data); }
    bool is_empty() const { return interface::is_empty(object_type::m_data); }
  private:
    void assign(const euclid::simple::segment& _segment);
  };
  namespace details
  {
    template<typename _system_>
    class line_string_builder
    {
      friend class details::polygon_builder<_system_>;
    public:
      using interface = typename _system_::interface;
      using line_string = euclid::geometry::line_string<_system_>;
    public:
      line_string_builder() : m_data(new details::vector_of_points) {}
      ~line_string_builder() {}
      line_string create() const
      {
        return line_string(interface::create_line_string(*m_data, m_dimensions));
      }
      line_string_builder& add_point(double _x, double _y, double _z = NAN)
      {
        if(not std::isnan(_z))
          m_dimensions = 3;
        return add_point(euclid::simple::point(_x, _y, _z));
      }
      template<typename _T_>
      line_string_builder& add_point(const _T_& _pt)
      {
        m_dimensions = std::max(m_dimensions, _pt.dimensions());
        m_data->push_back(_pt);
        return *this;
      }
    private:
      std::shared_ptr<details::vector_of_points> m_data;
      std::size_t m_dimensions = 2;
    };
  } // namespace details
  template<typename _system_>
  void line_string<_system_>::assign(const euclid::simple::segment& _segment)
  {
    builder b;
    b.add_point(_segment.first());
    b.add_point(_segment.second());
    object_type::m_data = b.create().m_data;
  }
} // namespace euclid::geometry
