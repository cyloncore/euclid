#ifndef EUCLID_GEOMETRY_HEADER
#include "line_string.h"
#include "linear_ring.h"
#endif

namespace euclid::geometry::traits
{
  template<class T>
  struct is_geometry : public std::false_type
  {
  };
  template<class _system_>
  struct is_geometry<geometry<_system_>> : public std::true_type
  {
  };
  template<class T>
  struct is_line_string : public std::false_type
  {
  };
  template<class _system_>
  struct is_line_string<line_string<_system_>> : public std::true_type
  {
  };
  template<class T>
  struct is_linear_ring : public std::false_type
  {
  };
  template<class _system_>
  struct is_linear_ring<linear_ring<_system_>> : public std::true_type
  {
  };
  template<class T>
  struct is_collection : public std::false_type
  {
  };
  template<class _system_>
  struct is_collection<multi_geometry<_system_>> : public std::true_type
  {
  };
  template<class _system_>
  struct is_collection<multi_point<_system_>> : public std::true_type
  {
  };
  template<class _system_>
  struct is_collection<multi_line_string<_system_>> : public std::true_type
  {
  };
  template<class _system_>
  struct is_collection<multi_polygon<_system_>> : public std::true_type
  {
  };

  template<class _TSrc_, class _T_Dst>
  struct is_convertible : public std::false_type
  {
  };
  template<class _T_>
  struct is_convertible<_T_, _T_> : public std::true_type
  {
  };
  template<template<class> class _TSrc_, class _T_System_>
  struct is_convertible<_TSrc_<_T_System_>, geometry<_T_System_>> : public std::true_type
  {
  };

  template<class _TSrc_, class _T_Dst>
  struct might_be_convertible : public std::false_type
  {
  };
  template<class _T_>
  struct might_be_convertible<_T_, _T_> : public std::true_type
  {
  };
  template<template<class> class _TSrc_, class _T_System_>
  struct might_be_convertible<_TSrc_<_T_System_>, geometry<_T_System_>> : public std::true_type
  {
  };
  template<template<class> class _TDst_, class _T_System_>
  struct might_be_convertible<geometry<_T_System_>, _TDst_<_T_System_>> : public std::true_type
  {
  };

  template<class _T_>
  struct is_a_geometry : public std::false_type
  {
  };

#define EUCLID_IS_A_GEOMETRY(_type_)                                                               \
  template<class _system_>                                                                         \
  struct is_a_geometry<_type_<_system_>> : public std::true_type                                   \
  {                                                                                                \
  }

  EUCLID_IS_A_GEOMETRY(geometry);
  EUCLID_IS_A_GEOMETRY(point);
  EUCLID_IS_A_GEOMETRY(line_string);
  EUCLID_IS_A_GEOMETRY(linear_ring);
  EUCLID_IS_A_GEOMETRY(polygon);
  EUCLID_IS_A_GEOMETRY(multi_geometry);
  EUCLID_IS_A_GEOMETRY(multi_point);
  EUCLID_IS_A_GEOMETRY(multi_line_string);
  EUCLID_IS_A_GEOMETRY(multi_polygon);

#undef EUCLID_IS_A_GEOMETRY

  template<class T>
  inline constexpr bool is_geometry_v = is_geometry<T>::value;
  template<class T>
  inline constexpr bool is_line_string_v = is_line_string<T>::value;
  template<class T>
  inline constexpr bool is_linear_ring_v = is_linear_ring<T>::value;
  template<class _TSrc_, class _T_Dst>
  inline constexpr bool might_be_convertible_v = might_be_convertible<_TSrc_, _T_Dst>::value;
  template<class _TSrc_, class _T_Dst>
  inline constexpr bool is_convertible_v = is_convertible<_TSrc_, _T_Dst>::value;
  template<class T>
  inline constexpr bool is_a_geometry_v = is_a_geometry<T>::value;
  template<class T>
  inline constexpr bool is_collection_v = is_collection<T>::value;
} // namespace euclid::geometry::traits
