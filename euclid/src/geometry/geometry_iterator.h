namespace euclid::geometry
{
  namespace details
  {
    template<typename _T_>
    struct geometry_iterator_base
    {
      virtual ~geometry_iterator_base() {}
      virtual geometry_iterator_base* clone() const = 0;
      virtual void next() = 0;
      virtual void prev() = 0;
      virtual _T_ get() const = 0;
      virtual bool equals(geometry_iterator_base* _rhs) const = 0;
    };
    template<typename _T_, typename _C_>
    struct geometry_iterator_impl : public geometry_iterator_base<_T_>
    {
      using iterator = typename _C_::iterator;
      geometry_iterator_impl(const iterator& _it) : m_it(_it) {}
      ~geometry_iterator_impl() {}
      geometry_iterator_base<_T_>* clone() const override
      {
        return new geometry_iterator_impl<_T_, _C_>(m_it);
      }
      void next() override { ++m_it; }
      void prev() override { --m_it; }
      _T_ get() const override { return euclid::geometry::cast<_T_>(*m_it); }
      bool equals(geometry_iterator_base<_T_>* _rhs) const override
      {
        geometry_iterator_impl* rhs = dynamic_cast<geometry_iterator_impl*>(_rhs);
        return rhs and m_it == rhs->m_it;
      }
    private:
      iterator m_it;
    };
  } // namespace details

  template<typename _T_>
  class geometry_iterator
  {
  public: // iterators
    using difference_type = long;
    using value_type = _T_;
    using pointer = value_type*;
    using reference = value_type;
    using iterator_category = std::bidirectional_iterator_tag;
  public:
    geometry_iterator() : geometry_iterator(nullptr) {}
    geometry_iterator(details::geometry_iterator_base<_T_>* _it) : m_it(_it) {}
    geometry_iterator(const geometry_iterator& _rhs) : m_it(_rhs.m_it->clone()) {}
    geometry_iterator& operator=(const geometry_iterator& _rhs)
    {
      delete m_it;
      m_it = _rhs.m_it->clone();
      return *this;
    }
    ~geometry_iterator() { delete m_it; }
    geometry_iterator& operator++()
    {
      m_it->next();
      return *this;
    }
    geometry_iterator& operator--()
    {
      m_it->prev();
      return *this;
    }
    _T_ operator*() const { return m_it->get(); }
    bool operator==(const geometry_iterator& _rhs) { return m_it->equals(_rhs.m_it); }
    bool operator!=(const geometry_iterator& _rhs) { return not(*this == _rhs); }
  private:
    details::geometry_iterator_base<_T_>* m_it;
  };

  template<typename _T_, typename _CT_>
  geometry_iterator<_T_> begin(const details::collection<_CT_>& _geometry)
  {
    //     static_assert(traits::might_be_convertible_v<_CT_, _T_>, "Not possible to convert");
    return geometry_iterator<_T_>(
      new details::geometry_iterator_impl<_T_, details::collection<_CT_>>(_geometry.begin()));
  }
  template<typename _T_, typename _CT_>
  geometry_iterator<_T_> end(const details::collection<_CT_>& _geometry)
  {
    //     static_assert(traits::might_be_convertible_v<_CT_, _T_>, "Not possible to convert");
    return geometry_iterator<_T_>(
      new details::geometry_iterator_impl<_T_, details::collection<_CT_>>(_geometry.end()));
  }

  template<typename _T_>
  geometry_iterator<_T_> begin(const geometry<typename _T_::system>& _geometry)
  {
    using system = typename _T_::system;
    using multi_geometry = typename system::multi_geometry;
    using multi_point = typename system::multi_point;
    using multi_line_string = typename system::multi_line_string;
    using multi_polygon = typename system::multi_polygon;

    if(_geometry.template is<multi_polygon>())
      return begin<_T_>(_geometry.template cast<multi_polygon>());
    if(_geometry.template is<multi_line_string>())
      return begin<_T_>(_geometry.template cast<multi_line_string>());
    if(_geometry.template is<multi_point>())
      return begin<_T_>(_geometry.template cast<multi_point>());
    if(_geometry.template is<multi_geometry>())
      return begin<_T_>(_geometry.template cast<multi_geometry>());
    euclid_fatal("Not a container!");
  }
  template<typename _T_>
  geometry_iterator<_T_> end(const geometry<typename _T_::system>& _geometry)
  {
    using system = typename _T_::system;
    using multi_geometry = typename system::multi_geometry;
    using multi_point = typename system::multi_point;
    using multi_line_string = typename system::multi_line_string;
    using multi_polygon = typename system::multi_polygon;

    if(_geometry.template is<multi_polygon>())
      return end<_T_>(_geometry.template cast<multi_polygon>());
    if(_geometry.template is<multi_line_string>())
      return end<_T_>(_geometry.template cast<multi_line_string>());
    if(_geometry.template is<multi_point>())
      return end<_T_>(_geometry.template cast<multi_point>());
    if(_geometry.template is<multi_geometry>())
      return end<_T_>(_geometry.template cast<multi_geometry>());
    euclid_fatal("Not a container!");
  }
} // namespace euclid::geometry
