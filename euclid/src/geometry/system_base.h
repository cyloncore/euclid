
namespace euclid::geometry
{
  /**
   * Base definition for an euclid system.
   *
   * Typically defined as follow:
   * \code
   * struct gdal : public euclid::geometry::system_base<gdal, details::gdal::interface> {};
   * \endcode
   */
  template<typename _system_, typename _interface_>
  struct system_base
  {
    using interface = _interface_;

    // import geometric objects
    using geometry = euclid::geometry::geometry<_system_>;
    using point = euclid::geometry::point<_system_>;
    using line_string = euclid::geometry::line_string<_system_>;
    using line_string_builder = euclid::geometry::details::line_string_builder<_system_>;
    using linear_ring = euclid::geometry::linear_ring<_system_>;
    using linear_ring_builder = euclid::geometry::details::linear_ring_builder<_system_>;
    using polygon = euclid::geometry::polygon<_system_>;
    using polygon_builder = euclid::geometry::details::polygon_builder<_system_>;

    using multi_geometry = euclid::geometry::multi_geometry<_system_>;
    using multi_point = euclid::geometry::multi_point<_system_>;
    using multi_line_string = euclid::geometry::multi_line_string<_system_>;
    using multi_polygon = euclid::geometry::multi_polygon<_system_>;

    // iterators
    template<typename _T_>
    using point_iterator = euclid::geometry::point_iterator<_T_>;
    template<typename _T_>
    using segment_iterator = euclid::geometry::segment_iterator<_T_>;

    using point_circular_iterator = euclid::geometry::point_circular_iterator<_system_>;
    using segment_circular_iterator = euclid::geometry::segment_circular_iterator<_system_>;

    // try cast
    using try_cast_to_point = euclid::geometry::try_cast_to<point>;
    using try_cast_to_line_string = euclid::geometry::try_cast_to<line_string>;
    using try_cast_to_linear_ring = euclid::geometry::try_cast_to<linear_ring>;
    using try_cast_to_polygon = euclid::geometry::try_cast_to<polygon>;
    using try_cast_to_multi_geometry = euclid::geometry::try_cast_to<multi_geometry>;
    using try_cast_to_multi_point = euclid::geometry::try_cast_to<multi_point>;
    using try_cast_to_multi_line_string = euclid::geometry::try_cast_to<multi_line_string>;
    using try_cast_to_multi_polygon = euclid::geometry::try_cast_to<multi_polygon>;
  };
} // namespace euclid::geometry
