#ifndef EUCLID_GEOMETRY_HEADER
#include <cmath>

#include "object.h"
#endif

namespace euclid::geometry
{
  template<typename _system_>
  class point : object<_system_, point>
  {
    EUCLID_CLASS_DEFINITION_COMMON_GEOMETRY_GEOMETRY(point, system)
  public:
    point(const euclid::simple::point& _pt) : point(_pt.x(), _pt.y(), _pt.z()) {}
    point(double _x, double _y, double _z = NAN) : object_type(interface::create_point(_x, _y, _z))
    {
    }
    point(const point& _rhs) : object_type(_rhs) {}
    point& operator=(const point& _rhs)
    {
      object_type::operator=(_rhs);
      return *this;
    }
    ~point() {}
    double x() const { return interface::point_get_x(object_type::m_data); }
    double y() const { return interface::point_get_y(object_type::m_data); }
    double z() const { return interface::point_get_z(object_type::m_data); }
    std::size_t dimensions() const { return interface::get_dimensions(object_type::m_data); }
    bool is_empty() const { return interface::is_empty(object_type::m_data); }
    bool operator==(const point& _rhs) const
    {
      return object_type::m_data == _rhs.m_data
             or (x() == _rhs.x() and y() == _rhs.y()
                 and ((std::isnan(z()) and std::isnan(_rhs.z())) or z() == _rhs.z()));
    }
    bool operator!=(const point& _rhs) const { return not(*this == _rhs); }
  };
} // namespace euclid::geometry
