#ifndef EUCLID_GEOMETRY_HEADER
#include "point_iterator.h"
#endif

namespace euclid::geometry::details
{
  template<typename _T_>
  class points_helper : public object<typename _T_::system, points_helper, _T_>
  {
    friend class segments_helper<_T_>;
    EUCLID_CLASS_DEFINITION_COMMON_CONTAINER(points_helper, _T_)
    friend _T_;
  public:
    point_iterator<_T_> begin() const
    {
      return point_iterator<_T_>(object_type::m_data).set_index(0);
    }
    point_iterator<_T_> end() const
    {
      return point_iterator<_T_>(object_type::m_data).set_index(size());
    }
    std::reverse_iterator<point_iterator<_T_>> rbegin() const
    {
      return std::reverse_iterator<point_iterator<_T_>>(end());
    }
    std::reverse_iterator<point_iterator<_T_>> rend() const
    {
      return std::reverse_iterator<point_iterator<_T_>>(begin());
    }
    operator std::vector<euclid::simple::point>() const
    {
      std::vector<euclid::simple::point> r;
      r.insert(r.begin(), begin(), end());
      return r;
    }
    std::size_t size() const { return interface::points_size(object_type::m_data); }
    euclid::simple::point operator[](std::size_t _index)
    {
      return interface::points_at(object_type::m_data, _index);
    }
  };
} // namespace euclid::geometry::details
