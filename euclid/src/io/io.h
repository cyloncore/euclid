#include <euclid/geometry>

namespace euclid::io
{
  struct wkt_options
  {
    int precision = 2;
    int dimensions = 2;
  };
  template<template<typename _I_> class _T_, typename _I_>
  inline std::string to_wkt(const _T_<_I_>& _object, const wkt_options& _options = wkt_options())
  {
    return _I_::interface::to_wkt(_object, _options);
  }
  template<class _T_>
  inline _T_ from_wkt(const std::string& _wkt)
  {
    return _T_::interface::template from_wkt<_T_>(_wkt);
  }
  struct json_options
  {
    int indent = 2;
  };
  template<template<typename _I_> class _T_, typename _I_>
    requires(_I_::features::has_json)
  inline std::string to_json(const _T_<_I_>& _object, const json_options& _options = json_options())
  {
    return _I_::interface::to_json(_object, _options);
  }
  template<class _T_>
    requires(_T_::system::features::has_json)
  inline _T_ from_json(const std::string& _wkt)
  {
    return _T_::interface::template from_json<_T_>(_wkt);
  }
} // namespace euclid::io
