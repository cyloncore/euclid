
namespace euclid::geometry
{
  template<class _TGeom_>
    requires traits::is_a_geometry_v<_TGeom_>
  inline std::ostream& operator<<(std::ostream& stream, const _TGeom_& _geom)
  {
    if(not _geom.is_null())
    {
      stream << euclid::io::to_wkt(_geom, {int(stream.precision())});
    }
    else
    {
      stream << "[null geometry]";
    }
    return stream;
  }
} // namespace euclid::geometry
