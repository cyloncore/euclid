
namespace euclid::extra::linear_ring
{
  namespace details
  {
    template<typename _T_>
    class CircularList
    {
      inline static int modulo(int a, int b)
      {
        const int result = a % b;
        return result >= 0 ? result : result + b;
      }
    public:
      void append(const _T_& _t) { m_values.push_back(_t); }
      const _T_& operator[](int _i) const { return m_values[modulo(_i, m_values.size())]; }
      int size() const { return m_values.size(); }
    private:
      std::vector<_T_> m_values;
    };
  } // namespace details
  /**
   * @return true if some points are duplicated
   */
  template<typename _system_>
  inline bool has_duplicate_points(const euclid::geometry::linear_ring<_system_>& _linearRing,
                                   double _tol)
  {
    for(std::size_t i = 1; i < _linearRing.points().size() - 1; ++i)
    {
      euclid::simple::point p1 = _linearRing.points()[i];
      for(std::size_t j = 0; j < i; ++j)
      {
        euclid::simple::point p2 = _linearRing.points()[j];
        if(euclid::simple::distance<1>(p1, p2) < _tol)
        {
          return true;
        }
      }
    }
    return false;
  }
  /**
   * @return true of the two linear rings share a common edge
   */
  template<typename _system_>
  inline bool share_edge(const euclid::geometry::linear_ring<_system_>& _linearRing1,
                         const euclid::geometry::linear_ring<_system_>& _linearRing2, double _tol,
                         std::size_t* _edge1, std::size_t* _edge2, bool* _reversed)
  {
    const std::size_t lR1_NP = _linearRing1.points().size();
    const std::size_t lR2_NP = _linearRing2.points().size();
    for(std::size_t i = 0; i < lR1_NP - 1; ++i)
    {
      euclid::simple::point pt1_a = _linearRing1.points()[i];
      euclid::simple::point pt1_b = _linearRing1.points()[i + 1];

      for(std::size_t j = 0; j < lR2_NP - 1; ++j)
      {
        euclid::simple::point pt2_a = _linearRing2.points()[j];
        euclid::simple::point pt2_b = _linearRing2.points()[j + 1];
        if(euclid::simple::distance<1>(pt1_a, pt2_a) < _tol
           and euclid::simple::distance<1>(pt1_b, pt2_b) < _tol)
        {
          if(_edge1)
            *_edge1 = i;
          if(_edge2)
            *_edge2 = j;
          if(_reversed)
            *_reversed = false;
          return true;
        }
        if(euclid::simple::distance<1>(pt1_a, pt2_b) < _tol
           and euclid::simple::distance<1>(pt1_b, pt2_a) < _tol)
        {
          if(_edge1)
            *_edge1 = i;
          if(_edge2)
            *_edge2 = j;
          if(_reversed)
            *_reversed = true;
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Combine two adjacent linear ring into one
   */
  template<typename _system_>
  inline typename _system_::linear_ring
    combine(const euclid::geometry::linear_ring<_system_>& _linearRing1, int _edge1,
            const euclid::geometry::linear_ring<_system_>& _linearRing2, int _edge2, bool _reversed)
  {
    details::CircularList<euclid::simple::point> ring1;
    details::CircularList<euclid::simple::point> ring2;
    const std::size_t lR1_NP = _linearRing1.points().size();
    const std::size_t lR2_NP = _linearRing2.points().size();

    for(std::size_t i = 0; i < lR1_NP - 1; ++i)
    {
      ring1.append(_linearRing1.points()[i]);
    }
    if(_reversed)
    {
      for(std::size_t i = 0; i < lR2_NP - 1; ++i)
      {
        ring2.append(_linearRing2.points()[i]);
      }
    }
    else
    {
      _edge2 = lR2_NP - _edge2 - 2;
      for(int i = lR2_NP - 1; i > 0; --i)
      {
        ring2.append(_linearRing2.points()[i]);
      }
    }

    int edge2_cut_low = ring2.size() + _edge2;
    int edge2_cut_high = _edge2 + 1;
    int edge1_cut_low = ring1.size() + _edge1;
    int edge1_cut_high = _edge1 + 1;

    // Check for more edges toward the low indices of edge2
    while(true)
    {
      int test_edge2 = edge2_cut_low - 1;
      int test_edge1 = edge1_cut_high + 1;
      const euclid::simple::point& pt1 = ring1[test_edge1];
      const euclid::simple::point& pt2 = ring2[test_edge2];

      if(euclid::simple::distance<1>(pt1, pt2) < 0.1)
      {
        edge2_cut_low = test_edge2;
        edge1_cut_high = test_edge1;
      }
      else
      {
        break;
      }
    }
    // Check for more edges toward the high indices of edge2
    while(true)
    {
      int test_edge2 = edge2_cut_high + 1;
      int test_edge1 = edge1_cut_low - 1;
      const euclid::simple::point& pt1 = ring1[test_edge1];
      const euclid::simple::point& pt2 = ring2[test_edge2];

      if(euclid::simple::distance<1>(pt1, pt2) < 0.1)
      {
        edge2_cut_high = test_edge2;
        edge1_cut_low = test_edge1;
      }
      else
      {
        break;
      }
    }

    typename _system_::linear_ring::builder lr_b;

    for(int i = edge1_cut_high; i < edge1_cut_low; ++i)
    {
      lr_b.add_point(ring1[i]);
    }
    for(int j = edge2_cut_high; j < edge2_cut_low; ++j)
    {
      lr_b.add_point(ring2[j]);
    }

    typename _system_::linear_ring lr = lr_b.create();

// Some sanity check
#ifndef NDEBUG
    typename _system_::polygon poly(lr);

    for(std::size_t i = 0; i < lr.points().size() - 1; ++i)
    {
      euclid::simple::point p1 = lr.points()[i];
      for(std::size_t j = 0; j < lr.points().size() - 1; ++j)
      {
        if(j != i and (std::abs((int)j - (int)i) <= 2 or (i == 1 and j == lr.points().size() - 2)))
        {
          euclid::simple::point p2 = lr.points()[j];
          euclid_assert(euclid::simple::distance<1>(p1, p2) > 0.001);
        }
      }
    }
//         // TODO no isPointOnRingBoundary in euclid
//         for(euclid::simple::point pt : _linearRing1.points())
//         {
//           euclid_assert(lr->isPointOnRingBoundary(&pt) or euclid::ops::contains(poly, pt));
//         }
//         for(euclid::simple::point pt : _linearRing2.points())
//         {
//           euclid_assert(lr->isPointOnRingBoundary(&pt) or euclid::ops::contains(poly, pt));
//         }
#endif
    return lr;
  }

  /**
   * Combine two adjacent linear ring into one
   */
  template<typename _system_>
  inline typename _system_::linear_ring
    combine(const euclid::geometry::linear_ring<_system_>& _linearRing1,
            const euclid::geometry::linear_ring<_system_>& _linearRing2, double _tol)
  {
    std::size_t edge1;
    std::size_t edge2;
    bool reversed;
    if(shareEdge(_linearRing1, _linearRing2, _tol, &edge1, &edge2, &reversed))
    {
      typename _system_::linear_ring combined
        = combine(_linearRing1, edge1, _linearRing2, edge2, reversed);
      if(not euclid::extra::is_clock_wise(combined))
      {
        combined = euclid::extra::reverse_order(combined);
      }
      return combined;
    }
    return typename _system_::linear_ring();
  }

  /**
   * @return the length of @p _index edge in the ring
   */
  template<typename _system_>
  inline double edge_length(const euclid::geometry::linear_ring<_system_>& _ring, int _index)
  {
    euclid::simple::point p1 = _ring.points()[_index];
    euclid::simple::point p2 = _ring.points()[_index + 1];
    return euclid::simple::distance<2>(p1, p2);
  }
  /**
   * @return the min, max, median, average standard deviation of angles of a curve
   */
  struct angle_statistics : public std::tuple<double, double, double, double, double>
  {
    enum class index
    {
      min,
      max,
      median,
      average,
      std_dev
    };
    template<typename _system_>
    angle_statistics(const euclid::geometry::linear_ring<_system_>& _curve)
    {
      double avg_sum = 0.0;
      QList<double> angles;
      for(typename _system_::segment_circular_iterator sit(_curve); sit.full_circles() == 0; ++sit)
      {
        euclid::simple::segment s1 = *sit;
        euclid::simple::segment s2 = *std::next(sit, 1);
        euclid::simple::point p1 = s1.first();
        euclid::simple::point p2 = s1.second();
        euclid_assert(p2 == s2.first());
        euclid::simple::point p3 = s2.second();

        double d = euclid::simple::__internal__::angle(p1, p2, p3);
        d = d < 0 ? d + 2 * M_PI : d;
        if(d < 2 * M_PI)
        {
          avg_sum += d;
          angles.append(d);
        }
      }

      double avg = avg_sum / angles.size();
      std::sort(angles.begin(), angles.end());

      double std_dev_sum = 0;
      double min = std::numeric_limits<double>::max();
      double max = -std::numeric_limits<double>::max();

      for(double a : angles)
      {
        std_dev_sum += (a - avg) * (a - avg);
        min = std::min(min, a);
        max = std::max(max, a);
      }
      double std_dev = std::sqrt(std_dev_sum / (angles.size() - 1));
      double median = angles[angles.size() / 2];

      *static_cast<std::tuple<double, double, double, double, double>*>(this)
        = std::make_tuple(min, max, median, avg, std_dev);
    }
    double min() const { return get_<index::min>(); }
    double max() const { return get_<index::max>(); }
    double median() const { return get_<index::median>(); }
    double average() const { return get_<index::average>(); }
    double std_dev() const { return get_<index::std_dev>(); }
  private:
    template<index _index_>
    double get_() const
    {
      return std::get<int(_index_)>(*this);
    }
  };
} // namespace euclid::extra::linear_ring

namespace std
{
  template<>
  struct tuple_size<euclid::extra::linear_ring::angle_statistics>
      : std::integral_constant<size_t, 5>
  {
  };

  template<std::size_t _index_>
  struct tuple_element<_index_, euclid::extra::linear_ring::angle_statistics>
  {
    using type = double;
  };
} // namespace std
