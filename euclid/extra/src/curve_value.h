#ifndef EUCLID_EXTRA_HEADER

#include <euclid/geometry>

#endif

namespace euclid::extra
{
  /**
   * Compute the point at a given distance along a curve.
   *
   * Example of use:
   *
   * @code
   * euclid::simple::point pt = euclid::extra::curve_value(line_string, 2.0);
   * @endcode
   */
  class curve_value : public euclid::operation::operation<curve_value, euclid::simple::point>
  {
  public:
    using euclid::operation::operation<curve_value, euclid::simple::point>::operation;
    template<class _T_>
    inline euclid::simple::point operator()(const _T_& _t, double _length)
    {
      static_assert(euclid::geometry::traits::is_line_string_v<_T_>
                      or euclid::geometry::traits::is_linear_ring_v<_T_>,
                    "Can only compute curve_value on a line string or a linear ring");

      double cl = _length;
      euclid_assert(not std::isnan(cl));

      for(euclid::geometry::segment_iterator<_T_> it = _t.segments().begin();
          it != _t.segments().end(); ++it)
      {
        euclid::simple::segment s = *it;
        double sl = euclid::simple::length(s);
        euclid_assert(not std::isnan(sl));
        if(cl < sl and sl > 0.0)
        {
          namespace es__i__ = euclid::simple::__internal__;
          return es__i__::add(s.first(),
                              es__i__::mul(cl / sl, es__i__::sub(s.second(), s.first())));
        }
        else
        {
          cl -= sl;
        }
      }
      if(euclid::geometry::traits::is_linear_ring_v<_T_>)
      {
        return curve_value(_t, cl);
      }
      else
      {
        return euclid::simple::point();
      }
    }
    template<typename _system_>
    inline euclid::simple::point operator()(const geometry::geometry<_system_>& _geometry,
                                            double _length)
    {
      return euclid::operation::dispatch<
        curve_value, euclid::operation::enable_for<euclid::geometry::line_string,
                                                   euclid::geometry::linear_ring>>()(_geometry,
                                                                                     _length);
    }
  };
} // namespace euclid::extra
