#ifndef EUCLID_EXTRA_HEADER

#include <euclid/operation>

#endif

namespace euclid::extra
{
  /**
   * @ingroup euclid_extra
   * Apply a filteration on the point of the geometry as defined by the \p _filteration function.
   */
  template<typename _system_>
  class filter_op : public euclid::operation::transform<_system_, filter_op<_system_>>
  {
  public:
    using euclid::operation::transform<_system_, filter_op<_system_>>::transform;

    // BEGIN collection
    template<template<typename> class _geometry_t_>
    inline geometry::details::collection<_geometry_t_<_system_>> operator()(
      const geometry::details::collection<_geometry_t_<_system_>>& _collection,
      const std::function<bool(const euclid::geometry::geometry<_system_>&)>& _filteration)
    {
      typename geometry::details::collection<_geometry_t_<_system_>>::builder builder;
      for(std::size_t i = 0; i < _collection.size(); ++i)
      {
        _geometry_t_<_system_> g = _collection.at(i);
        if(_filteration(g))
        {
          builder.add_element(g);
        }
      }
      return builder.create();
    }
    // END collection
    // BEGIN geometry
    inline typename _system_::geometry operator()(
      const geometry::geometry<_system_>& _geometry,
      const std::function<bool(const euclid::geometry::geometry<_system_>&)>& _filteration)
    {
      return operation::dispatch<filter_op<_system_>, operation::enable_for_all_collections,
                                 geometry::geometry<_system_>>(_geometry, _filteration);
    }
    // END geometry
  };
  /**
   * @ingroup euclid_extra
   * Apply a filteration on the point of the geometry as defined by the \p _filteration function.
   */
  template<typename _T_>
  inline _T_
    filter(const _T_& _t,
           const std::function<bool(const euclid::geometry::geometry<typename _T_::system>&)>&
             _filteration)
  {
    return filter_op<typename _T_::system>(_t, _filteration);
  }
} // namespace euclid::extra
