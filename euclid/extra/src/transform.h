#ifndef EUCLID_EXTRA_HEADER

#include <euclid/operation>

#endif

namespace euclid::extra
{
  inline euclid::simple::point transform(
    const euclid::simple::point& _point,
    const std::function<euclid::simple::point(const euclid::simple::point&)>& _transformation)
  {
    return _transformation(_point);
  }
  inline euclid::simple::segment transform(
    const euclid::simple::segment& _segment,
    const std::function<euclid::simple::point(const euclid::simple::point&)>& _transformation)
  {
    return euclid::simple::segment(_transformation(_segment.first()),
                                   _transformation(_segment.second()));
  }
  template<typename _T_>
  inline _T_ transform(
    const _T_& _t,
    const std::function<euclid::simple::point(const euclid::simple::point&)>& _transformation);

  /**
   * @ingroup euclid_extra
   * Apply a transformation on the point of the geometry as defined by the \p _transformation
   * function.
   */
  template<typename _system_>
  class transform_op : public euclid::operation::transform<_system_, transform_op<_system_>>
  {
  public:
    using euclid::operation::transform<_system_, transform_op<_system_>>::transform;

    // BEGIN point
    inline typename _system_::point operator()(
      const geometry::point<_system_>& _point,
      const std::function<euclid::simple::point(const euclid::simple::point&)>& _transformation)
    {
      euclid::simple::point pt = _transformation(_point);
      return typename _system_::point(pt.x(), pt.y(), pt.z());
    }
    // END point
    // BEGIN line_string
    inline typename _system_::line_string operator()(
      const geometry::line_string<_system_>& _ring,
      const std::function<euclid::simple::point(const euclid::simple::point&)>& _transformation)
    {
      typename _system_::line_string::builder builder;
      return transform_points<0>(builder, _ring, _transformation).create();
    }
    // END line_string
    // BEGIN linear_ring
    inline typename _system_::linear_ring operator()(
      const geometry::linear_ring<_system_>& _ring,
      const std::function<euclid::simple::point(const euclid::simple::point&)>& _transformation)
    {
      typename _system_::linear_ring::builder builder;
      return transform_points<1>(builder, _ring, _transformation).create();
    }
    // END linear_ring
    // BEGIN polygon
    typename _system_::polygon operator()(
      const geometry::polygon<_system_>& _polygon,
      const std::function<euclid::simple::point(const euclid::simple::point&)>& _transformation)
    {
      typename _system_::polygon::builder builder;
      transform_points<1>(builder, _polygon.exterior_ring(), _transformation);
      for(const geometry::linear_ring<_system_>& ring : _polygon.interior_rings())
      {
        builder.start_interior_ring();
        transform_points<1>(builder, ring, _transformation);
      }
      return builder.create();
    }
    // END polygon
    // BEGIN collection
    template<template<typename> class _geometry_t_>
    inline geometry::details::collection<_geometry_t_<_system_>> operator()(
      const geometry::details::collection<_geometry_t_<_system_>>& _collection,
      const std::function<euclid::simple::point(const euclid::simple::point&)>& _transformation)
    {
      typename geometry::details::collection<_geometry_t_<_system_>>::builder builder;
      for(std::size_t i = 0; i < _collection.size(); ++i)
      {
        builder.add_element(transform(_collection.at(i), _transformation));
      }
      return builder.create();
    }
    // END collection
    // BEGIN geometry
    inline typename _system_::geometry operator()(
      const geometry::geometry<_system_>& _geometry,
      const std::function<euclid::simple::point(const euclid::simple::point&)>& _transformation)
    {
      return operation::dispatch<transform_op<_system_>, operation::enable_for_all,
                                 geometry::geometry<_system_>>(_geometry, _transformation);
    }
    // END geometry
  private:
    template<std::size_t skip, typename _Builder_, typename _GeometryT_>
    inline _Builder_& transform_points(
      _Builder_& _builder, const _GeometryT_& _geometry,
      const std::function<euclid::simple::point(const euclid::simple::point&)>& _transformation)
    {
      for(auto it = _geometry.points().begin(); it != std::prev(_geometry.points().end(), skip);
          ++it)
      {
        _builder.add_point(_transformation(*it));
      }
      return _builder;
    }
  };
  /**
   * @ingroup euclid_extra
   * Apply a transformation on the point of the geometry as defined by the \p _transformation
   * function.
   */
  template<typename _T_>
  inline _T_ transform(
    const _T_& _t,
    const std::function<euclid::simple::point(const euclid::simple::point&)>& _transformation)
  {
    return transform_op<typename _T_::system>(_t, _transformation);
  }
  // BEGIN translation scale transformation
  /**
   * @ingroup euclid_extra
   * Apply a translation and scale to a geometry.
   */
  template<typename _T_>
  inline _T_ transform(const _T_& _geometry, const euclid::simple::point& _translation,
                       double _scale)
  {
    return transform(_geometry,
                     [_translation, _scale](const euclid::simple::point& _pt)
                     {
                       namespace esi = euclid::simple::__internal__;
                       return esi::mul(_scale, esi::sub(_pt, _translation));
                     });
  }

  // END translation scale transformation
} // namespace euclid::extra
