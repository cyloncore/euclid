#ifndef EUCLID_EXTRA_HEADER

#include <euclid/extra/transform>
#include <euclid/ops>

#endif

namespace euclid::extra
{
  /**
   * This operation takes a geometry (which can be either a polygon or a multi_polygon), and
   * decompose it into a list of convex polygon.
   *
   * This operation computes a delaunay triangulation, and then recombine the triangles to form as
   * large as possible convex polygon.
   */
  template<typename _system_>
  inline typename _system_::multi_geometry
    convex_polygon_decomposition(const geometry::geometry<_system_>& _source, double _tol)
  {
    using system = _system_;
    using multi_geometry = typename system::multi_geometry;
    using geometry = typename system::geometry;
    using polygon = typename system::polygon;
    using linear_ring = typename system::linear_ring;
    using line_string = typename system::line_string;
    using point = typename system::point;

    euclid::simple::box envelope = euclid::ops::envelope(_source);

    const double envelope_width = envelope.right() - envelope.left();
    const double envelope_height = envelope.bottom() - envelope.top();

    multi_geometry triangles;

    const bool transfo_enabled = (envelope_width < 10 or envelope_height < 10);
    const double transfo_scale = std::max(10.0 / envelope_width, 10.0 / envelope_height);
    const double transfo_x = -envelope.left();
    const double transfo_y = -envelope.top();

    if(transfo_enabled)
    {
      geometry ts = euclid::extra::transform(_source, euclid::simple::point(transfo_x, transfo_y),
                                             transfo_scale);
      triangles = euclid::ops::delaunay_triangulation(ts, _tol);
    }
    else
    {
      triangles = euclid::ops::delaunay_triangulation(_source, _tol);
    }
    triangles = euclid::extra::element_wise(triangles, _source,
                                            &euclid::ops::intersection<geometry, geometry>);

    std::vector<linear_ring> result;

    for(geometry g : triangles)
    {
      if(g.template is<polygon>())
      {
        polygon p = g.template cast<polygon>();
        result.push_back(p.exterior_ring());
      }
      else if(g.template is<line_string>())
      {
        // Most likely means an empty intersection
        euclid_assert(g.template cast<line_string>().points().size() == 2);
      }
      else if(g.template is<point>())
      {
        // Most likely means an empty intersection
      }
      else
      {
        euclid_fatal("Unsupported type: %", g.type_name());
      }
    }

    while(true)
    {
      linear_ring bestMatch;
      std::pair<int, int> bestMatchIndex;
      double bestStdDevAngle = std::numeric_limits<double>::max();
      for(std::size_t i = 0; i < result.size(); ++i)
      {

        for(std::size_t j = 0; j < result.size(); ++j)
        {
          std::size_t edge1;
          std::size_t edge2;
          bool reversed;
          if(i != j
             and euclid::extra::linear_ring::share_edge(result[i], result[j], _tol, &edge1, &edge2,
                                                        &reversed))
          {
            linear_ring combined
              = euclid::extra::linear_ring::combine(result[i], edge1, result[j], edge2, reversed);
            if(not euclid::extra::is_clock_wise(combined))
            {
              combined = euclid::extra::reverse_order(combined);
            }
            if(euclid::extra::is_convex(combined))
            {
              double aa = euclid::extra::linear_ring::angle_statistics(combined).std_dev();
              if(aa < bestStdDevAngle)
              {
                bestMatch = combined;
                bestMatchIndex = std::pair<int, int>(i, j);
                bestStdDevAngle = aa;
              }
            }
          }
        }
      }
      if(bestMatch.is_null())
      {
        break; // We are done
      }
      else
      {
        result[bestMatchIndex.first] = bestMatch;
        result.erase(result.begin() + bestMatchIndex.second);
      }
    }

    typename multi_geometry::builder collection_builder;
    for(linear_ring r : result)
    {
      if(transfo_enabled)
      {
        linear_ring rt = euclid::extra::transform(
          r, euclid::simple::point(-transfo_x * transfo_scale, -transfo_y * transfo_scale),
          1.0 / transfo_scale);
        collection_builder.add_element(rt);
      }
      else
      {
        collection_builder.add_element(r);
      }
    }
    return collection_builder.create();
  }
} // namespace euclid::extra
