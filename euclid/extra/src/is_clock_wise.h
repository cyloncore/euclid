#ifndef EUCLID_EXTRA_HEADER

#include <euclid/geometry>

#endif

namespace euclid::extra
{
  template<class _system_>
  inline bool is_clock_wise(const euclid::geometry::linear_ring<_system_>& _lr)
  {
    return signed_area(_lr) < 0.0;
  }
} // namespace euclid::extra
