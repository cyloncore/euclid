// #define DEBUG_POLYLINE_FITTING

namespace euclid::extra
{
  namespace details
  {
    struct line_string_fitting_functor
    {
      typedef double Scalar;
      enum
      {
        InputsAtCompileTime = Eigen::Dynamic,
        ValuesAtCompileTime = Eigen::Dynamic
      };
      typedef Eigen::Matrix<Scalar, InputsAtCompileTime, 1> InputType;
      typedef Eigen::Matrix<Scalar, ValuesAtCompileTime, 1> ValueType;
      typedef Eigen::Matrix<Scalar, ValuesAtCompileTime, InputsAtCompileTime> JacobianType;

      line_string_fitting_functor(int _controlPoints,
                                  const std::vector<euclid::simple::point>& _source)
          : m_controlPoints(_controlPoints), m_source(_source)
      {
      }

      int values() const { return 1; }

      int operator()(const InputType& x, ValueType& fvec) const
      {
        /* Use the green theorem http://www.attewode.com/Calculus/AreaMeasurement/area.htm to
         * compute the area, since we have a self intersecting polygon, this compute the difference
         * between the area between both side of the generate polyline
         */
        double area = 0.0;

        for(std::size_t i = 0; i < m_source.size() - 1; ++i)
        {
          area += m_source[i].x() * m_source[i + 1].y() - m_source[i + 1].x() * m_source[i].y();
        }
        euclid::simple::point lastS = m_source.back();
        area += lastS.x() * x[x.size() - 1] - x[x.size() - 2] * lastS.y();
        for(int i = x.size() - 2; i >= 2; i -= 2)
        {
          int j = i - 2;
          euclid_assert(j >= 0);
          area += x[i] * x[j + 1] - x[j] * x[i + 1];
        }
        euclid::simple::point firstS = m_source.front();
        area += x[0] * firstS.y() - firstS.x() * x[1];

        fvec(0) = 0.5 * std::abs(area);

        return 0;
      }

      int m_controlPoints;
      std::vector<euclid::simple::point> m_source;
    };

    struct Segment
    {
      Segment(const euclid::simple::point& _p1, const euclid::simple::point& _p2) : p1(_p1), p2(_p2)
      {
        D = 1.0 / euclid::simple::distance<2>(p1, p2);
        ux = (p2.x() - p1.x()) * D;
        uy = (p2.y() - p1.y()) * D;
      }
      bool isValid() { return std::isfinite(D); }
      euclid::simple::point p1, p2;
      double D, uy, ux;
      inline double signedDistance(const euclid::simple::point& _pt) const
      {
        const double px = _pt.x() - p1.x();
        const double py = _pt.y() - p1.y();

        const double t = (ux * px + uy * py) * D;
        const double cp = uy * px - ux * py;

        if(t < 0.0)
          return std::copysign(euclid::simple::distance<2>(_pt, p1), cp);
        else if(t > 1.0)
          return std::copysign(euclid::simple::distance<2>(_pt, p2), cp);
        return cp;
      }
      static void appendValid(std::vector<Segment>* _segments, const euclid::simple::point& _p1,
                              const euclid::simple::point& _p2)
      {
        Segment s(_p1, _p2);
        if(s.isValid())
        {
          _segments->push_back(s);
        }
      }
    };

    double computeMaxError(const line_string_fitting_functor::InputType& x,
                           const std::vector<euclid::simple::point>& _source)
    {
      double max_error = 0.0;
      std::vector<Segment> x_lines;
      Segment::appendValid(&x_lines, _source.front(), euclid::simple::point(x(0), x(1)));
      for(int i = 0; i < x.size() - 2; i += 2)
      {
        Segment::appendValid(&x_lines, euclid::simple::point(x(i), x(i + 1)),
                             euclid::simple::point(x(i + 2), x(i + 3)));
      }
      Segment::appendValid(&x_lines, euclid::simple::point(x(x.size() - 2), x(x.size() - 1)),
                           _source.back());

      for(std::size_t i = 1; i < _source.size() - 1; ++i)
      {
        const euclid::simple::point& pt = _source[i];
        double abs_best_sd = std::numeric_limits<double>::max();
        for(const Segment& l : x_lines)
        {
          double ad = std::abs(l.signedDistance(pt));
          if(ad < abs_best_sd)
          {
            abs_best_sd = ad;
          }
        }
        max_error = std::max(max_error, abs_best_sd);
      }

      std::vector<Segment> sources_lines;
      for(std::size_t i = 0; i < _source.size() - 1; ++i)
      {
        Segment::appendValid(&sources_lines, _source[i], _source[i + 1]);
      }

      for(int i = 0; i < x.size(); i += 2)
      {
        euclid::simple::point pt(x(i), x(i + 1));
        double abs_best_sd = std::numeric_limits<double>::max();
        for(const Segment& l : sources_lines)
        {
          double ad = std::abs(l.signedDistance(pt));
          if(ad < abs_best_sd)
          {
            abs_best_sd = ad;
          }
        }
        max_error = std::max(max_error, abs_best_sd);
      }
      return max_error;
    }
  } // namespace details
  /**
   * @ingroup euclid_extra
   * Fit a line string on a list of points, while preserving the surface of the seperated area.
   */
  std::vector<euclid::simple::point>
    line_string_fitting(const std::vector<euclid::simple::point>& _source, double _tol)
  {
    euclid_assert(_source.size() >= 2);
    if(_source.size() == 3)
      return _source;

    std::size_t joints = 1;

    while(joints < _source.size() - 2)
    {
      details::line_string_fitting_functor functor(joints, _source);
      Eigen::NumericalDiff<details::line_string_fitting_functor> numDiff(functor);
      euclid::internal::gradient_descent<Eigen::NumericalDiff<details::line_string_fitting_functor>,
                                         double>
        lm(numDiff);
      lm.parameters.maxfev = 2000;
      lm.parameters.xtol = 0.001;

      lm.parameters.gamma = 1;
      lm.parameters.gammaAdaptation = 0.5;
      lm.parameters.lineSearch = true;

      Eigen::VectorXd x = Eigen::VectorXd::Zero(2 * joints);
      double pts_per_joints = _source.size() / joints;

      for(std::size_t i = 0; i < joints; ++i)
      {
        int start = std::max<int>(0, pts_per_joints * (i - 0.3));
        int end = std::min<int>(_source.size(), pts_per_joints * (i + 1.3));
        for(int j = start; j < end; ++j)
        {
          const euclid::simple::point& pt = _source[j];
          x(i * 2) += pt.x();
          x(i * 2 + 1) += pt.y();
        }
        x.segment(i * 2, 2) /= (end - start);
      }
      double max_error = NAN;

#ifdef DEBUG_POLYLINE_FITTING
      std::cout << "Original x: " << x << std::endl;
      details::line_string_fitting_functor::ValueType j(1, 1);
      max_error = details::computeMaxError(x, _source);
      functor(x, j);
      std::cout << "Original error" << j(0) << " " << max_error << std::endl;
#endif

      euclid::internal::gradient_descent_status ret = lm.minimise_init(x);
      do
      {
        ret = lm.minimise_one_step(x);
#ifdef DEBUG_POLYLINE_FITTING
        max_error = details::computeMaxError(x, _source);
        functor(x, j);
        std::cout << "================= \n"
                  << "Iter: " << lm.iter << "\n"
                  << "Nfev: " << lm.nfev << "\n"
                  << "Delta: " << lm.delta.transpose() << "\n"
                  << "Dfvec: " << lm.dfvec << "\n"
                  << "Error: " << j(0) << " " << max_error << "\n"
                  << "x: " << x.transpose() << std::endl;
#endif
      } while(ret == euclid::internal::gradient_descent_status::running);

#ifdef DEBUG_POLYLINE_FITTING
      std::cout << "iter: " << lm.iter << std::endl;
      std::cout << "delta: " << lm.delta << lm.delta.stableNorm() << std::endl;
      std::cout << "ret: " << int(ret) << std::endl;

      functor(x, j);
#endif
      max_error = details::computeMaxError(x, _source);
#ifdef DEBUG_POLYLINE_FITTING
      std::cout << "x that minimizes the function: " << x.transpose() << "with error" << j(0) << " "
                << max_error << std::endl;
#endif

      if(max_error < _tol) // or max_error?
      {
        std::vector<euclid::simple::point> res;

        res.push_back(_source.front());

        for(int i = 0; i < x.size(); i += 2)
        {
          res.push_back(euclid::simple::point(x(i), x(i + 1)));
        }

        res.push_back(_source.back());

        return res;
      }
      ++joints;
    }
    euclid_error("Completely failed to fit a polygon...");
    return _source;
  }

  /**
   * Fit a line string on a set of point in a multi_geometry
   */
  template<typename _system_>
  euclid::geometry::line_string<_system_>
    line_string_fitting(const euclid::geometry::multi_geometry<_system_>& _source, double _tol)
  {
    using point = typename _system_::point;
    std::vector<euclid::simple::point> points;
    for(euclid::geometry::geometry_iterator<point> it = euclid::geometry::begin<point>(_source);
        it != euclid::geometry::end<point>(_source); ++it)
    {
      euclid::simple::point pt = *it;
      points.push_back(euclid::simple::point(pt.x(), pt.y()));
    }
    std::vector<euclid::simple::point> res = line_string_fitting(points, _tol);
    typename _system_::line_string::builder result;
    for(const euclid::simple::point& pt : res)
    {
      result.add_point(pt.x(), pt.y());
    }
    return result.create();
  }
} // namespace euclid::extra
