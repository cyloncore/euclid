#ifndef EUCLID_EXTRA_HEADER

#include <euclid/extra/moment_of_inertia>

#endif

namespace euclid::extra::compactness
{
  /**
   * Base definition for compactness algorithm
   */
  template<typename _Op_>
  using algorithm = euclid::operation::operation<_Op_, double>;
  /**
   * Compute the fatness of a geometry by computing the ration between its length
   * and the circumference of a circle which has the same area.
   */
  struct schwartzberg : public algorithm<schwartzberg>
  {
    using algorithm<schwartzberg>::algorithm;
    template<typename _T_>
    double operator()(const _T_& _lr)
    {
      double area = std::abs(euclid::extra::signed_area(
        _lr)); // use signed_area, since regular area is only defined for polygon
      double length = euclid::ops::length(_lr);
      double radius = std::sqrt(area / M_PI);
      return (2.0 * M_PI * radius) / length;
    }
  };
  /**
   * Reock degree of compactness: compute the fatness of a geometry by computing the
   * ratio between its area and the area of the smallest circle which encloses it.
   */
  struct reock : public algorithm<reock>
  {
    using algorithm<reock>::algorithm;
    template<typename _T_>
    double operator()(const _T_& _lr)
    {
      double area = std::abs(euclid::extra::signed_area(
        _lr)); // use signed_area, since regular area is only defined for polygon
      double area_enclosing = euclid::simple::area(euclid::ops::minimum_enclosing_circle(_lr));
      return area / area_enclosing;
    }
  };
  /**
   * Compute the compactness by computing the minimum rotated rectangle which contains
   * the geometry, and returns the ratio between the lengths of that rectangle.
   */
  struct minimum_rotated_rectangle_ratio : public algorithm<minimum_rotated_rectangle_ratio>
  {
    using algorithm<minimum_rotated_rectangle_ratio>::algorithm;
    template<typename _T_>
    double operator()(const _T_& _lr)
    {
      using system = typename _T_::system;
      typename system::polygon rr = euclid::ops::minimum_rotated_rectangle(_lr);
      euclid::geometry::segment_iterator<typename system::linear_ring> it
        = rr.exterior_ring().segments().begin();
      double length_1 = euclid::simple::length(*it);
      ++it;
      double length_2 = euclid::simple::length(*it);
      ++it;
      return std::min(length_1, length_2) / std::max(length_1, length_2);
    }
  };

  /**
   * Compute the compactness of a geometry by computing the ration between the circumference
   * of the maximum inscribed circle and the circumference of the minimum enclosing circle.
   */
  struct two_balls : public algorithm<two_balls>
  {
    using algorithm<two_balls>::algorithm;
    template<typename _T_>
    double operator()(const _T_& _lr)
    {
      double circumference_inscribed
        = euclid::simple::circumference(euclid::ops::maximum_inscribed_circle(_lr, 1e-3));
      double circumference_enclosing
        = euclid::simple::circumference(euclid::ops::minimum_enclosing_circle(_lr));
      return circumference_inscribed / circumference_enclosing;
    }
  };
  /**
   * Compute the compactness of a geometry by using the polsby–popper test.
   * Which is equivalent to computing the ratio between its length
   * and the circumference of a circle which has the same area.
   */
  struct polsby_popper : public algorithm<polsby_popper>
  {
    using algorithm<polsby_popper>::algorithm;
    template<typename _T_>
    double operator()(const _T_& _lr)
    {
      double area = std::abs(euclid::extra::signed_area(
        _lr)); // use signed_area, since regular area is only defined for polygon
      double length = euclid::ops::length(_lr);
      return (4.0 * M_PI * area) / (length * length);
    }
  };
  /**
   * Compute the compactness of a geometry by using the moment of inertia test.
   * Li, Wenwe, Goodchild, Michael F., and Church, Richard, 2013, An efficient measure of
   * compactness for two-dimensional shapes andits application in regionalization problems.
   * International Journal of Geographical Information Science, 27(6), p. 1227-1250.
   */
  struct moment_of_inertia : public algorithm<moment_of_inertia>
  {
    using algorithm<moment_of_inertia>::algorithm;
    template<typename _T_>
    double operator()(const _T_& _lr)
    {
      double area = std::abs(euclid::extra::signed_area(
        _lr)); // use signed_area, since regular area is only defined for polygon
      double mi = std::abs(euclid::extra::moment_of_inertia(_lr).xy());

      return (area * area) / (2 * M_PI * mi);
    }
  };
} // namespace euclid::extra::compactness
