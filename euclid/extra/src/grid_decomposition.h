
namespace euclid::extra
{
  /**
   * @param _grid_size represent the size of the grid cells
   */
  template<typename _system_, template<typename _I_> class _TOther_>
    requires euclid::geometry::traits::is_a_geometry_v<_TOther_<_system_>>
  euclid::geometry::multi_polygon<_system_> grid_decomposition(const _TOther_<_system_>& _source,
                                                               double _grid_size)
  {
    using geometry = typename _system_::geometry;
    using polygon = typename _system_::polygon;
    typename _system_::multi_polygon::builder result;
    euclid::simple::box envelope = euclid::ops::envelope(_source);

    for(double x = envelope.left(); x <= envelope.right(); x += _grid_size)
    {
      for(double y = envelope.top(); y <= envelope.bottom(); y += _grid_size)
      {
        typename polygon::builder poly_b;
        poly_b.add_point(x, y);
        poly_b.add_point(x + _grid_size, y);
        poly_b.add_point(x + _grid_size, y + _grid_size);
        poly_b.add_point(x, y + _grid_size);

        polygon polygon_b = poly_b.create();

        if(euclid::ops::contains(_source, polygon_b))
        {
          result.add_element(polygon_b);
        }
        else if(euclid::ops::intersects(_source, polygon_b))
        {
          geometry geometry_intersection = euclid::ops::intersection(_source, polygon_b);

          if(geometry_intersection.template is<polygon>())
          {
            if(euclid::ops::area(geometry_intersection) > 0.001)
            {
              result.add_element(polygon_b);
            }
          }
          else if(geometry_intersection.is_collection())
          {
            for(euclid::geometry::geometry_iterator<polygon> it_poly
                = euclid::geometry::begin<polygon>(geometry_intersection);
                it_poly != euclid::geometry::end<polygon>(geometry_intersection); ++it_poly)
            {
              polygon poly = *it_poly;
              if(euclid::ops::area(poly) > 0.001)
              {
                result.add_element(poly);
              }
            }
          }
        }
      }
    }

    return result.create();
  }
} // namespace euclid::extra
