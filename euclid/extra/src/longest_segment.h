#ifndef EUCLID_EXTRA_HEADER

#include <euclid/geometry>

#endif

namespace euclid::extra
{
  /**
   * @ingroup euclid_extra
   *
   * @return an iterator to the longest segment in a polygon or linear_ring.
   */
  template<class _T_>
  inline euclid::geometry::segment_iterator<_T_> longest_segment_iterator(const _T_& _t)
  {
    euclid::geometry::segment_iterator<_T_> best_it = _t.segments().begin();
    double best_length = euclid::simple::length(*best_it);
    euclid::geometry::segment_iterator<_T_> it = best_it;
    ++it;
    for(; it != _t.segments().end(); ++it)
    {
      double length = euclid::simple::length(*it);
      if(length > best_length)
      {
        best_it = it;
        best_length = length;
      }
    }
    return best_it;
  }
  /**
   * @ingroup euclid_extra
   *
   * @return the longest segment in a polygon or linear_ring.
   */
  template<class _T_>
  inline euclid::simple::segment longest_segment(const _T_& _t)
  {
    return *longest_segment_iterator(_t);
  }
} // namespace euclid::extra
