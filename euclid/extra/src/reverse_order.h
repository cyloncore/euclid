#ifndef EUCLID_EXTRA_HEADER

#include <euclid/geometry>

#endif

namespace euclid::extra
{
  /**
   * @ingroup euclid_extra
   * @return a line_string or linear_ring where the points are in reverse order
   */
  template<typename _system_>
  class reverse_order_op : public euclid::operation::transform<_system_, reverse_order_op<_system_>>
  {
  public:
    using euclid::operation::transform<_system_, reverse_order_op<_system_>>::transform;

    template<class _T_>
    inline _T_ operator()(const _T_& _lr)
    {
      static_assert(euclid::geometry::traits::is_line_string_v<_T_>
                      or euclid::geometry::traits::is_linear_ring_v<_T_>,
                    "Can only reverse order of a line string or a linear ring");

      const std::size_t offset = euclid::geometry::traits::is_linear_ring_v<_T_> ? 1 : 0;

      typename _T_::builder t_builder;

      for(std::reverse_iterator<typename _T_::system::template point_iterator<_T_>> it
          = _lr.points().rbegin();
          it != std::prev(_lr.points().rend(), offset); ++it)
      {
        t_builder.add_point(*it);
      }
      return t_builder.create();
    }
    inline typename _system_::geometry operator()(
      const geometry::geometry<_system_>& _geometry,
      const std::function<euclid::simple::point(const euclid::simple::point&)>& _transformation)
    {
      return operation::dispatch<
        reverse_order_op,
        operation::enable_for<euclid::geometry::line_string, euclid::geometry::linear_ring>,
        geometry::geometry<_system_>>(_geometry, _transformation);
    }
  };
  template<typename _T_>
  inline _T_ reverse_order(const _T_& _t)
  {
    return reverse_order_op<typename _T_::system>(_t);
  }
} // namespace euclid::extra
