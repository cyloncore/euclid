#ifndef EUCLID_EXTRA_HEADER

#include <euclid/geometry>

#endif

namespace euclid::extra
{
  template<class _system_, template<typename> class _CT_, template<typename> class _OT_,
           typename _OP_>
  inline euclid::geometry::multi_geometry<_system_>
    element_wise(const _CT_<_system_>& _c, const _OT_<_system_>& _o, const _OP_& _op)
  {
    using geometry = euclid::geometry::geometry<_system_>;
    static_assert(euclid::geometry::traits::is_collection_v<_CT_<_system_>>);
    typename euclid::geometry::multi_geometry<_system_>::builder result_builder;

    for(auto v : _c)
    {
      geometry r = _op(v, _o);
      if(not r.is_null())
      {
        if(r.is_collection())
        {
          for(auto it = euclid::geometry::begin<geometry>(r);
              it != euclid::geometry::end<geometry>(r); ++it)
          {
            result_builder.add_element(*it);
          }
        }
        else
        {
          result_builder.add_element(r);
        }
      }
    }

    return result_builder.create();
  }
} // namespace euclid::extra
