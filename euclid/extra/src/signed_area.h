#ifndef EUCLID_EXTRA_HEADER

#include <euclid/geometry>

#endif

namespace euclid::extra
{
  /**
   * @ingroup euclid_extra
   * @return the signed_area of a linear_ring or polygon
   */
  class signed_area : public euclid::operation::operation<signed_area, double>
  {
  public:
    using euclid::operation::operation<signed_area, double>::operation;
    template<class _system_>
    inline double operator()(const euclid::geometry::linear_ring<_system_>& _lr)
    {
      double sum = 0.0;
      for(typename _system_::point_circular_iterator it(_lr); it.full_circles() == 0; ++it)
      {
        euclid::simple::point p1 = *std::prev(it, 1);
        euclid::simple::point p2 = *it;
        euclid::simple::point p3 = *std::next(it, 1);

        sum += p2.x() * (p3.y() - p1.y());
      }
      return sum * 0.5;
    }
    template<class _system_>
    inline double operator()(const euclid::geometry::polygon<_system_>& _lr)
    {
      if(_lr.interior_rings_count() > 0)
      {
        // Not implemented if there are interior rings
        return NAN;
      }
      else
      {
        return signed_area(_lr.exterior_ring());
      }
    }
    template<class _T_>
    inline double operator()(const _T_&)
    {
      return NAN;
    }

    template<typename _system_>
    inline double operator()(const geometry::geometry<_system_>& _geometry)
    {
      return euclid::operation::dispatch<signed_area>()(_geometry);
    }
  };
  // END geometry

} // namespace euclid::extra
