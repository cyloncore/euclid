#ifndef EUCLID_EXTRA_HEADER

#include <euclid/geometry>

#endif

namespace euclid::extra
{
  struct moment_of_inertia
  {
    using return_type = moment_of_inertia;
    template<typename _T_>
    moment_of_inertia(const _T_& /*_lr*/)
    {
    }

    template<class _system_>
    moment_of_inertia(const euclid::geometry::linear_ring<_system_>& _lr) : m_x(0), m_y(0), m_xy(0)
    {
      for(typename _system_::point_circular_iterator it(_lr); it.full_circles() == 0; ++it)
      {
        euclid::simple::point p0 = *it;
        euclid::simple::point p1 = *std::next(it, 1);

        double dot = (p0.x() * p1.y() - p1.x() * p0.y());
        m_x += dot * (p0.x() * p0.x() + p0.x() * p1.x() + p1.x() * p1.x());
        m_y += dot * (p0.y() * p0.y() + p0.y() * p1.y() + p1.y() * p1.y());
        m_xy
          += dot * (p0.x() * p1.y() + 2 * p0.x() * p0.y() + 2 * p1.x() * p1.y() + p1.x() * p0.y());
      }
      m_x /= 12.0;
      m_y /= 12.0;
      m_xy /= 24.0;
    }
    template<class _system_>
    moment_of_inertia(const euclid::geometry::polygon<_system_>& _lr)
    {
      if(_lr.interior_rings_count() == 0)
      {
        *this = moment_of_inertia(_lr.exterior_ring());
      }
    }
    template<class _system_>
    moment_of_inertia(const euclid::geometry::geometry<_system_>& _geometry)
    {

      *this = operation::dispatch<euclid::extra::moment_of_inertia>(_geometry);
    }

    double x() const { return m_x; }
    double y() const { return m_y; }
    double xy() const { return m_xy; }
  private:
    double m_x = NAN, m_y = NAN, m_xy = NAN;
  };
} // namespace euclid::extra
