#ifndef EUCLID_EXTRA_HEADER

#include <euclid/geometry>
#include <random>

#endif

namespace euclid::extra
{
  /**
   * @ingroup euclid_extra
   * Generate rectangle of random center, size and rotation.
   *
   * @p _system_ the euclid system
   * @p _distribution_ a C++ random number distribution
   * @p _random_engine_ a C++ random number engine
   */
  template<class _system_, class _distribution_ = std::uniform_real_distribution<double>,
           class _random_engine_ = std::default_random_engine>
  struct random_rectangle
  {
    using polygon = typename _system_::polygon;
    random_rectangle() {}
    operator polygon()
    {
      const double l1 = 0.5 * _distribution_(m_min_length, m_max_length)(m_random);
      const double l2 = 0.5 * _distribution_(m_min_length, m_max_length)(m_random);
      const double r = _distribution_(m_min_rotation, m_max_rotation)(m_random);
      const double cx = _distribution_(m_min_center_x, m_max_center_x)(m_random);
      const double cy = _distribution_(m_min_center_y, m_max_center_y)(m_random);
      const double cr = std::cos(r);
      const double sr = std::sin(r);
      typename polygon::builder builder;

      auto add_point = [&builder, cx, cy, cr, sr](double dx, double dy)
      { builder.add_point(cx + cr * dx - sr * dy, cy + sr * dx + cr * dy); };

      add_point(-l1, -l2);
      add_point(-l1, l2);
      add_point(l1, l2);
      add_point(l1, -l2);

      return builder.create();
    }
    /**
     * Set the range for the x-coordinate of the center of the rectangle
     */
    random_rectangle& set_x_range(double _min, double _max)
    {
      m_min_center_x = _min;
      m_max_center_x = _max;
      return *this;
    }
    /**
     * Set the range for the y-coordinate of the center of the rectangle
     */
    random_rectangle& set_y_range(double _min, double _max)
    {
      m_min_center_y = _min;
      m_max_center_y = _max;
      return *this;
    }
    /**
     * Set the range for the length of the  rectangle
     */
    random_rectangle& set_length_range(double _min, double _max)
    {
      m_min_length = _min;
      m_max_length = _max;
      return *this;
    }
    /**
     * Set the range for the rotation of the rectangle
     */
    random_rectangle& set_rotation_range(double _min, double _max)
    {
      m_min_rotation = _min;
      m_max_rotation = _max;
      return *this;
    }
    /**
     * Set the random engine used for generating numbers
     */
    random_rectangle& set_random(const _random_engine_& _engine)
    {
      m_random = _engine;
      return *this;
    }
  private:
    double m_min_center_x = 0.0, m_min_center_y = 0.0, m_max_center_x = 0.0, m_max_center_y = 0.0;
    double m_min_length = 0.0, m_max_length = 1.0, m_min_rotation = 0.0, m_max_rotation = 2 * M_PI;
    _random_engine_ m_random = _random_engine_(std::random_device()());
  };
} // namespace euclid::extra
