#ifndef EUCLID_EXTRA_HEADER

#include <euclid/geometry>

#endif

namespace euclid::extra
{
  template<class _system_>
  inline bool is_convex(const euclid::geometry::linear_ring<_system_>& _lr)
  {
    for(typename _system_::point_circular_iterator it(_lr); it.full_circles() != 1; ++it)
    {
      euclid::simple::point p1 = *std::prev(it, 1);
      euclid::simple::point p2 = *it;
      euclid::simple::point p3 = *std::next(it, 1);
      const double crossproduct
        = (p2.x() - p1.x()) * (p3.y() - p2.y()) - (p2.y() - p1.y()) * (p3.x() - p2.x());
      if(crossproduct > 0)
      {
        return false;
      }
    }
    return true;
  }
} // namespace euclid::extra
