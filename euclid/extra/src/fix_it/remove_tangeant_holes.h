#ifndef EUCLID_EXTRA_HEADER

#include <euclid/geometry>
#include <euclid/operation>

#endif

namespace euclid::extra::fix_it
{
  static constexpr double remove_tangeant_holes_default_tolerance = 1e-2;
  /**
   * @ingroup euclid_extra
   *
   * This is an algorithm that remove_tangeant_holes removes holes that are tangeant to the boundary
   * or merge holes that share an edge
   */

  template<typename _system_>
  class remove_tangeant_holes_op
      : public euclid::operation::transform<_system_, remove_tangeant_holes_op<_system_>>
  {
    using linear_ring = geometry::linear_ring<_system_>;
    using polygon = geometry::polygon<_system_>;
    using multi_polygon = geometry::multi_polygon<_system_>;
    using multi_geometry = geometry::multi_geometry<_system_>;
    using geometry = geometry::geometry<_system_>;
  public:
    using euclid::operation::transform<_system_, remove_tangeant_holes_op<_system_>>::transform;

    // BEGIN polygon
    polygon operator()(const polygon& _polygon,
                       double _tolerance = remove_tangeant_holes_default_tolerance)
    {
      linear_ring exterior_ring = _polygon.exterior_ring();
      std::vector<linear_ring> interior_rings = _polygon.interior_rings();

      // Adjust tolerance to size of polygon
      euclid::simple::box enveloppe = euclid::ops::envelope(_polygon);
      double adj_tolerance = _tolerance * std::min(enveloppe.width(), enveloppe.height());

      // merge holes

      for(typename std::vector<linear_ring>::iterator it = interior_rings.begin();
          it != interior_rings.end(); ++it)
      {
        for(typename std::vector<linear_ring>::iterator it2 = std::next(it);
            it2 != interior_rings.end();)
        {
          linear_ring lr = *it;
          if(merge_rings(&lr, *it2, _tolerance, adj_tolerance))
          {
            it2 = interior_rings.erase(it2);
            *it = lr;
          }
          else
          {
            ++it2;
          }
        }
      }

      // merge remaining holes with exterior ring
      for(typename std::vector<linear_ring>::iterator it = interior_rings.begin();
          it != interior_rings.end();)
      {
        if(merge_rings(&exterior_ring, *it, _tolerance, adj_tolerance))
        {
          it = interior_rings.erase(it);
        }
        else
        {
          ++it;
        }
      }
      return polygon(exterior_ring, interior_rings);
    }
    // END polygon
    // BEGIN collection
    template<template<typename> class _geometry_t_>
    inline geometry
      operator()(const euclid::geometry::details::collection<_geometry_t_<_system_>>& _collection,
                 double _tolerance = remove_tangeant_holes_default_tolerance)
    {
      typename euclid::geometry::details::collection<_geometry_t_<_system_>>::builder builder;
      for(std::size_t i = 0; i < _collection.size(); ++i)
      {
        builder.add_element(remove_tangeant_holes_op(_collection.at(i), _tolerance));
      }
      return builder.create();
    }
    // END collection
    // BEGIN geometry
    inline geometry operator()(const geometry& _geometry,
                               double _tolerance = remove_tangeant_holes_default_tolerance)
    {
      return operation::dispatch<
        remove_tangeant_holes_op,
        operation::enable_for<euclid::geometry::polygon, euclid::geometry::multi_polygon,
                              euclid::geometry::multi_geometry>,
        geometry>(_geometry, _tolerance);
    }
    // END geometry
  private:
    bool merge_rings(linear_ring* _linear_ring_target, const linear_ring& _linear_ring_source,
                     double _tolerance, double _adj_tolerance)
    {
      std::vector<euclid::simple::segment> segments_1 = _linear_ring_target->segments();
      std::vector<euclid::simple::segment> segments_2 = _linear_ring_source.segments();
      for(auto it_s_1 = segments_1.begin(); it_s_1 != segments_1.end(); ++it_s_1)
      {
        for(auto it_s_2 = segments_2.begin(); it_s_2 != segments_2.end(); ++it_s_2)
        {
          if(euclid::simple::overlaps(*it_s_1, *it_s_2, _tolerance))
          {
            typename linear_ring::builder builder;

            // add the points from the first ring
            for(auto it = std::next(it_s_1); it != segments_1.end(); ++it)
            {
              builder.add_point(it->first());
            }
            for(auto it = segments_1.begin(); it != it_s_1; ++it)
            {
              builder.add_point(it->first());
            }
            builder.add_point(it_s_1->first());

            // add the points from the second

            namespace esi = euclid::simple::__internal__;

            euclid::simple::point u_s1 = esi::sub(it_s_1->second(), it_s_1->first());
            euclid::simple::point u_s2_f = esi::sub(it_s_2->first(), it_s_1->first());
            euclid::simple::point u_s2_s = esi::sub(it_s_2->second(), it_s_1->first());

            bool start_from_it_s_2_first = (esi::dot(u_s1, u_s2_f) < esi::dot(u_s1, u_s2_s));

            if(start_from_it_s_2_first)
            {
              if(euclid::simple::distance<1>(it_s_1->first(), it_s_2->first()) > _adj_tolerance)
              {
                builder.add_point(it_s_2->first());
              }
              if(std::prev(segments_2.end()) != it_s_2)
              {
                for(auto it = std::prev(it_s_2); it != std::prev(segments_2.begin()); --it)
                {
                  builder.add_point(it->first());
                }
                for(auto it = std::prev(segments_2.end()); it != std::next(it_s_2); --it)
                {
                  builder.add_point(it->first());
                }
              }
              else
              {
                for(auto it = std::prev(it_s_2); it != segments_2.begin(); --it)
                {
                  builder.add_point(it->first());
                }
              }
              if(euclid::simple::distance<1>(it_s_1->second(), it_s_2->second()) > _adj_tolerance)
              {
                builder.add_point(it_s_2->second());
              }
            }
            else
            {
              if(euclid::simple::distance<1>(it_s_1->first(), it_s_2->second()) > _adj_tolerance)
              {
                builder.add_point(it_s_2->second());
              }

              if(std::next(it_s_2) != segments_2.end())
              {
                for(auto it = std::next(it_s_2, 2); it != segments_2.end(); ++it)
                {
                  builder.add_point(it->first());
                }
                for(auto it = segments_2.begin(); it != it_s_2; ++it)
                {
                  builder.add_point(it->first());
                }
              }
              else
              {
                for(auto it = std::next(segments_2.begin()); it != it_s_2; ++it)
                {
                  builder.add_point(it->first());
                }
              }
              if(euclid::simple::distance<1>(it_s_1->second(), it_s_2->first()) > _adj_tolerance)
              {
                builder.add_point(it_s_2->first());
              }
            }
            *_linear_ring_target = builder.create();
            return true;
          }
        }
      }

      return false;
    }
  };
  template<typename _T_>
  inline euclid::geometry::geometry<typename _T_::system>
    remove_tangeant_holes(const _T_& _t,
                          double _tolerance = remove_tangeant_holes_default_tolerance)
  {
    return remove_tangeant_holes_op<typename _T_::system>(_t, _tolerance);
  }
} // namespace euclid::extra::fix_it
