#ifndef EUCLID_EXTRA_HEADER

#include <euclid/geometry>
#include <euclid/operation>

#endif

namespace euclid::extra::fix_it
{
  static constexpr double correct_self_intersection_default_tolerance = 1e-8;
  /**
   * @ingroup euclid_extra
   *
   * This is an algorithm that correct self intersecting linear_ring.
   *
   * It works by looping over all segments and detecting the one which intersects,
   */

  template<typename _system_>
  class correct_self_intersection_op
      : public euclid::operation::transform<_system_, correct_self_intersection_op<_system_>>
  {
    euclid::simple::point compute_point_fix(const euclid::simple::point& _point_to_fix,
                                            const euclid::simple::point& _other_point,
                                            double _fix_length, const euclid::simple::segment& _s)
    {
      namespace es__i__ = euclid::simple::__internal__;
      // Best would be to compute the symetric of _point_to_fix with respect to _s, but this leads
      // to numerical error. Instead we use the other segment point, to compute a fix direction

      // 1) Project the other point on the line
      euclid::simple::point a = es__i__::sub(_other_point, _s.first());
      euclid::simple::point b = es__i__::normalized(es__i__::sub(_s.second(), _s.first()));

      double dot_a_b = es__i__::dot(a, b);
      euclid::simple::point a_on_s = es__i__::mul(dot_a_b, b);

      // 2) Compute the direction to apply the fix
      euclid::simple::point fix_direction = es__i__::sub(a, a_on_s);

      // 3) Fix the point
      return es__i__::add(_point_to_fix, es__i__::mul(_fix_length, fix_direction));
    }
  public:
    using euclid::operation::transform<_system_, correct_self_intersection_op<_system_>>::transform;

    inline euclid::geometry::linear_ring<_system_>
      operator()(const euclid::geometry::linear_ring<_system_>& _linear_ring,
                 double _tol = correct_self_intersection_default_tolerance)
    {
      std::vector<euclid::simple::segment> segments = _linear_ring.segments();
      bool has_finished = false;
      while(has_finished == false)
      {
        has_finished = true;
        for(auto it_f = segments.begin(); it_f != std::prev(segments.end(), 2); ++it_f)
        {
          auto it_f2_end = (it_f == segments.begin()) ? std::prev(segments.end()) : segments.end();
          for(auto it_f2 = std::next(it_f, 2); it_f2 != it_f2_end;
              ++it_f2) // two conscecutive segments cannot self-intersect
          {
            euclid::simple::point ip;
            if(euclid::simple::intersection(*it_f, *it_f2, &ip, _tol))
            {
              double l1 = euclid::simple::distance<2>(it_f->first(), it_f->second());
              double l2 = euclid::simple::distance<2>(it_f2->first(), it_f2->second());

              // There two cases:
              // a) the ip is close to a segment extremety, and it is better to move the extremety
              // b) it is not, then we introduce two points and split the segments

              // a) is treated differently depending on which point it is close to
              if(euclid::simple::distance<2>(ip, it_f->first()) < 0.001 * l1)
              {
                std::vector<euclid::simple::segment> new_segments;
                euclid::simple::point p2_update
                  = compute_point_fix(it_f->first(), it_f->second(), 0.001 * l1, *it_f2);
                if(it_f == segments.begin())
                {
                  // 1) Add all the other segments.
                  auto it_end_prev = std::prev(segments.end());
                  new_segments.insert(new_segments.begin(), std::next(it_f), it_end_prev);

                  // 2) Fix the point and then add the new segments.
                  new_segments.push_back(euclid::simple::segment(it_end_prev->first(), p2_update));
                  new_segments.push_back(euclid::simple::segment(p2_update, it_f->second()));

                  // 3) Intersection is fixed, lets start over again.
                  has_finished = false;
                  std::swap(segments, new_segments);
                  goto correct_self_intersection_end_label;
                }
                else
                {
                  // 1) Add all the segments before the current one (if any).
                  auto it_f_prev = std::prev(it_f);
                  new_segments.insert(new_segments.begin(), segments.begin(),
                                      it_f_prev); // Add all the segments up to the previous one.

                  // 2) Fix the point and then add the new segments.
                  new_segments.push_back(euclid::simple::segment(it_f_prev->first(), p2_update));
                  new_segments.push_back(euclid::simple::segment(p2_update, it_f->second()));

                  // 3) Add all the remaining segments before the current one (if any).
                  new_segments.insert(new_segments.end(), std::next(it_f), segments.end());

                  // 4) Intersection is fixed, lets start over again.
                  has_finished = false;
                  std::swap(segments, new_segments);
                  goto correct_self_intersection_end_label;
                }
              }
              else if(euclid::simple::distance<2>(ip, it_f->second()) < 0.001 * l1)
              {
                std::vector<euclid::simple::segment> new_segments;
                // 1) Add all the segments before the current one (if any).
                new_segments.insert(new_segments.begin(), segments.begin(),
                                    it_f); // Add all the segments
                // 2) Fix the point and then add the new segments.
                euclid::simple::point p2_update
                  = compute_point_fix(it_f->second(), it_f->first(), 0.001 * l1, *it_f2);
                new_segments.push_back(euclid::simple::segment(it_f->first(), p2_update));
                auto it_f_next = std::next(it_f);
                new_segments.push_back(euclid::simple::segment(p2_update, it_f_next->second()));
                // 3) Add all the remaining segments before the current one (if any).
                new_segments.insert(new_segments.end(), std::next(it_f_next), segments.end());

                // 4) Intersection is fixed, lets start over again.
                has_finished = false;
                std::swap(segments, new_segments);
                goto correct_self_intersection_end_label;
              }
              else if(euclid::simple::distance<2>(ip, it_f2->first()) < 0.001 * l2)
              {
                std::vector<euclid::simple::segment> new_segments;
                // 1) Add all the segments before the current one
                new_segments.insert(new_segments.begin(), segments.begin(), std::prev(it_f2));
                // 2) Fix the point and then add the new segments.
                euclid::simple::point p2_update
                  = compute_point_fix(it_f2->first(), it_f2->second(), 0.001 * l2, *it_f);
                auto it_f2_prev = std::prev(it_f2);
                new_segments.push_back(euclid::simple::segment(it_f2_prev->first(), p2_update));
                new_segments.push_back(euclid::simple::segment(p2_update, it_f2->second()));
                // 3) Add all the remaining segments before the current one (if any).
                new_segments.insert(new_segments.end(), std::next(it_f2), segments.end());
                // 4) Intersection is fixed, lets start over again.
                has_finished = false;
                std::swap(segments, new_segments);
                goto correct_self_intersection_end_label;
              }
              else if(euclid::simple::distance<2>(ip, it_f2->second()) < 0.001 * l2)
              {
                std::vector<euclid::simple::segment> new_segments;
                euclid::simple::point p2_update
                  = compute_point_fix(it_f2->second(), it_f2->first(), 0.001 * l2, *it_f);
                if(std::next(it_f2) == segments.end())
                {
                  // 1) Add all the segments before the current one
                  new_segments.insert(new_segments.begin(), std::next(segments.begin()), it_f2);
                  // 2) Fix the point and then add the new segments.
                  new_segments.push_back(euclid::simple::segment(it_f2->first(), p2_update));
                  auto it_f2_next = segments.begin();
                  new_segments.push_back(euclid::simple::segment(p2_update, it_f2_next->second()));
                  // 3) Intersection is fixed, lets start over again.
                  has_finished = false;
                  std::swap(segments, new_segments);
                  goto correct_self_intersection_end_label;
                }
                else
                {
                  // 1) Add all the segments before the current one
                  new_segments.insert(new_segments.begin(), segments.begin(), it_f2);
                  // 2) Fix the point and then add the new segments.
                  new_segments.push_back(euclid::simple::segment(it_f2->first(), p2_update));
                  auto it_f2_next = std::next(it_f2);
                  new_segments.push_back(euclid::simple::segment(p2_update, it_f2_next->second()));
                  // 3) Add all the remaining segments before the current one (if any).
                  new_segments.insert(new_segments.end(), std::next(it_f2), segments.end());
                  // 4) Intersection is fixed, lets start over again.
                  has_finished = false;
                  std::swap(segments, new_segments);
                  goto correct_self_intersection_end_label;
                }
              }
              else
              { // b) We introduce two points and split the segments.
                // 1) Intersection has been detected, add all the segments before to the new list of
                // segment.
                std::vector<euclid::simple::segment> new_segments;
                namespace es__i__ = euclid::simple::__internal__;
                new_segments.insert(new_segments.begin(), segments.begin(), it_f);

                // 2) Compute a point that is slightly away from the intersection.
                euclid::simple::point ip1
                  = es__i__::add(ip, es__i__::mul(1.1 * _tol, es__i__::normalized(es__i__::add(
                                                                es__i__::sub(it_f2->first(), ip),
                                                                es__i__::sub(it_f->first(), ip)))));

                // 3) Create a new segment.
                new_segments.push_back(euclid::simple::segment(it_f->first(), ip1));
                new_segments.push_back(euclid::simple::segment(ip1, it_f2->first()));

                // 4) Add segments in between the intresections, but flipped so that the points are
                // in the correct order.
                for(auto it = std::prev(it_f2); it != it_f; --it)
                {
                  new_segments.push_back(euclid::simple::flip(*it));
                }

                // 5) Compute a point that is slightly away from the intersection, but in the other
                // direction than 2).
                euclid::simple::point ip2 = es__i__::add(
                  ip, es__i__::mul(1.1 * _tol, es__i__::normalized(
                                                 es__i__::add(es__i__::sub(it_f2->second(), ip),
                                                              es__i__::sub(it_f->second(), ip)))));

                // 6) Add new segments.
                new_segments.push_back(euclid::simple::segment(it_f->second(), ip2));
                new_segments.push_back(euclid::simple::segment(ip2, it_f2->second()));

                // 7) Add the remaining segments.
                new_segments.insert(new_segments.end(), std::next(it_f2), segments.end());

                // 8) Intersection is fixed, lets start over again.
                has_finished = false;
                std::swap(segments, new_segments);
                goto correct_self_intersection_end_label;
              }
            }
          }
        }
      correct_self_intersection_end_label:;
      }
      typename euclid::geometry::linear_ring<_system_>::builder builder;
      for(const euclid::simple::segment& seg : segments)
      {
        builder.add_point(seg.first());
      }
      return builder.create();
    }
    // BEGIN polygon
    geometry::polygon<_system_> operator()(const geometry::polygon<_system_>& _polygon,
                                           double _tolerance
                                           = correct_self_intersection_default_tolerance)
    {
      using linear_ring = geometry::linear_ring<_system_>;
      linear_ring ext = correct_self_intersection_op(_polygon.exterior_ring(), _tolerance);
      std::vector<linear_ring> ints;
      for(const linear_ring& lr : _polygon.interior_rings())
      {
        ints.push_back(correct_self_intersection_op(lr, _tolerance));
      }
      return geometry::polygon<_system_>(ext, ints);
    }
    // END polygon
    // BEGIN collection
    template<template<typename> class _geometry_t_>
    inline geometry::details::collection<_geometry_t_<_system_>>
      operator()(const geometry::details::collection<_geometry_t_<_system_>>& _collection,
                 double _tolerance = correct_self_intersection_default_tolerance)
    {
      typename geometry::details::collection<_geometry_t_<_system_>>::builder builder;
      for(std::size_t i = 0; i < _collection.size(); ++i)
      {
        builder.add_element(correct_self_intersection_op(_collection.at(i), _tolerance));
      }
      return builder.create();
    }
    // END collection
    inline typename _system_::geometry operator()(const geometry::geometry<_system_>& _geometry,
                                                  double _tolerance
                                                  = correct_self_intersection_default_tolerance)
    {
      return operation::dispatch<
        correct_self_intersection_op,
        operation::enable_for<euclid::geometry::polygon, euclid::geometry::linear_ring,
                              euclid::geometry::multi_polygon, euclid::geometry::multi_geometry>,
        geometry::geometry<_system_>>(_geometry, _tolerance);
    }
  };

  template<typename _T_>
  inline _T_ correct_self_intersection(const _T_& _t, double _tolerance
                                                      = correct_self_intersection_default_tolerance)
  {
    return correct_self_intersection_op<typename _T_::system>(_t, _tolerance);
  }
} // namespace euclid::extra::fix_it
