#ifndef EUCLID_EXTRA_HEADER

#include <euclid/geometry>
#include <euclid/operation>

#endif

namespace euclid::extra::fix_it
{
  static constexpr double split_default_tolerance = 1e-2;
  /**
   * @ingroup euclid_extra
   *
   * This is an algorithm that split a linear_ring or polygon if two segments are colinear
   */

  template<typename _system_>
  class split_op : public euclid::operation::operation<split_op<_system_>,
                                                       euclid::geometry::geometry<_system_>>
  {
    using linear_ring = geometry::linear_ring<_system_>;
    using polygon = geometry::polygon<_system_>;
    using multi_polygon = geometry::multi_polygon<_system_>;
    using multi_geometry = geometry::multi_geometry<_system_>;
    using geometry = geometry::geometry<_system_>;
  public:
    using euclid::operation::operation<split_op<_system_>,
                                       euclid::geometry::geometry<_system_>>::operation;

    // BEGIN polygon
    geometry operator()(const polygon& _polygon, double _tolerance = split_default_tolerance)
    {
      // split exterior and interiors
      std::vector<linear_ring> splited = split(_polygon.exterior_ring(), _tolerance);
      std::vector<linear_ring> splited_ints;
      for(const linear_ring& lr : _polygon.interior_rings())
      {
        std::vector<linear_ring> splited_ints_local = split(lr, _tolerance);
        splited_ints.insert(splited_ints.end(), splited_ints_local.begin(),
                            splited_ints_local.end());
      }

      // return result
      if(splited.size() == 1)
      {
        return polygon(*splited.begin(), splited_ints);
      }
      else
      {
        typename multi_polygon::builder mp_builder;

        for(const linear_ring& e_ring : splited)
        {
          std::vector<linear_ring> ints;

          for(auto it = splited_ints.begin(); it != splited_ints.end();)
          {
            if(euclid::ops::within(polygon(*it), polygon(e_ring)))
            {
              ints.push_back(*it);
              splited_ints.erase(it);
            }
            else
            {
              ++it;
            }
          }
          mp_builder.add_element(polygon(e_ring, ints));
        }
        euclid_assert(splited_ints.size() == 0);

        return mp_builder.create();
      }
    }
    // END polygon
    // BEGIN collection
    template<template<typename> class _geometry_t_>
    inline geometry
      operator()(const euclid::geometry::details::collection<_geometry_t_<_system_>>& _collection,
                 double _tolerance = split_default_tolerance)
    {
      typename euclid::geometry::details::collection<_geometry_t_<_system_>>::builder builder;
      for(std::size_t i = 0; i < _collection.size(); ++i)
      {
        geometry g = split_op(_collection.at(i), _tolerance);
        if(g.is_collection())
        {
          multi_geometry mg = g.template cast<multi_geometry>();
          for(int j = 0; j < mg.size(); ++j)
          {
            builder.add_element(mg.at(j).template cast<_geometry_t_<_system_>>());
          }
        }
        else
        {
          builder.add_element(g.template cast<_geometry_t_<_system_>>());
        }
      }
      return builder.create();
    }
    // END collection
    // BEGIN geometry
    inline geometry operator()(const geometry& _geometry,
                               double _tolerance = split_default_tolerance)
    {
      return operation::dispatch<
        split_op, operation::enable_for<euclid::geometry::polygon, euclid::geometry::multi_polygon,
                                        euclid::geometry::multi_geometry>>(_geometry, _tolerance);
    }
    // END geometry
  private:
    std::vector<linear_ring> split(const linear_ring& _linear_ring, double _tolerance)
    {
      std::vector<euclid::simple::segment> segments = _linear_ring.segments();
      for(auto it_f = segments.begin(); it_f != std::prev(segments.end(), 2); ++it_f)
      {
        auto it_f2_end = (it_f == segments.begin()) ? std::prev(segments.end()) : segments.end();
        for(auto it_f2 = std::next(it_f, 2); it_f2 != it_f2_end;
            ++it_f2) // two conscecutive segments cannot overlap
        {
          if(euclid::simple::overlaps(*it_f, *it_f2, _tolerance) and (it_f2 - it_f) >= 3
             and (it_f - segments.begin() + segments.end() - it_f2) >= 3)
          {
            std::vector<euclid::simple::segment> new_segments_first, new_segments_second;

            // 1) Overlaps has been detected, add all the segments before to the first linear_ring
            new_segments_first.insert(new_segments_first.end(), segments.begin(), std::next(it_f));

            // 2) add all the segments in between to the second linear_ring
            new_segments_second.insert(new_segments_second.end(), std::next(it_f),
                                       std::next(it_f2));

            // 3) add the remaining segments to the first linear ring
            new_segments_first.insert(new_segments_first.end(), std::next(it_f2), segments.end());

            // 4) create the new linear rings and try to split them more

            std::vector<linear_ring> result;
            for(const std::vector<euclid::simple::segment>& new_lr_s :
                {new_segments_first, new_segments_second})
            {
              typename linear_ring::builder builder;
              for(const euclid::simple::segment& seg : new_lr_s)
              {
                builder.add_point(seg.first());
              }
              std::vector<linear_ring> other_rings
                = {builder.create()}; //;split(builder.create(), _tol);
              result.insert(result.end(), other_rings.begin(), other_rings.end());
            }

            return result;
          }
        }
      }
      return {_linear_ring};
    }
#if 0
  correct_self_intersection_end_label:;
      }
      return builder.create();
    }
#endif
  };
  template<typename _T_>
  inline euclid::geometry::geometry<typename _T_::system>
    split(const _T_& _t, double _tolerance = split_default_tolerance)
  {
    return split_op<typename _T_::system>(_t, _tolerance);
  }
} // namespace euclid::extra::fix_it
