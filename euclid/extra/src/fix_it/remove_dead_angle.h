#ifndef EUCLID_EXTRA_HEADER

#include <euclid/geometry>
#include <euclid/operation>

#endif

namespace euclid::extra::fix_it
{
  static constexpr double remove_dead_angle_default_distance_tolerance = 1e-5;
  static constexpr double remove_dead_angle_default_angle_tolerance = 1.0 * M_PI / 180.0;
  /**
   * @ingroup euclid_extra
   *
   * This is an algorithm that remove points in a polygon where the angle between two consecutives
   * segments is equal to 0
   */

  template<typename _system_>
  class remove_dead_angle_op
      : public euclid::operation::transform<_system_, remove_dead_angle_op<_system_>>
  {
  public:
    using euclid::operation::transform<_system_, remove_dead_angle_op<_system_>>::transform;

    // BEGIN linear_ring
    inline typename _system_::linear_ring
      operator()(const geometry::linear_ring<_system_>& _ring,
                 double _distance_tolerance = remove_dead_angle_default_distance_tolerance,
                 double _angle_tolerance = remove_dead_angle_default_angle_tolerance)
    {
      using points_vector = std::vector<euclid::simple::point>;
      points_vector pts = _ring.points();

      pts.erase(std::prev(pts.end()));
      euclid::geometry::details::circular_iterator<points_vector::iterator> it(pts.begin(),
                                                                               pts.end());

      while(it.full_circles() == 0
            and pts.size() > 3) // 3 is the minimalal number of points for a valid linear ring
      {
        euclid::simple::point p1 = *std::prev(it, 1);
        euclid::simple::point p2 = *it;
        euclid::simple::point p3 = *std::next(it, 1);

        double angle = euclid::simple::__internal__::angle(p1, p2, p3);
        if(std::abs(angle) < _angle_tolerance)
        {
          points_vector::iterator new_it
            = std::prev(pts.erase(points_vector::iterator(it)),
                        1); // new_it points on p1, while erase points on p3
          if(simple::distance<1>(p1, p3)
               < _distance_tolerance * (simple::distance<1>(p2, p3) + simple::distance<1>(p1, p2))
             and pts.size() > 3)
          {
            points_vector::iterator to_remove = std::next(new_it, 1); // remove p3
            if(to_remove == pts.end())
            {
              pts.erase(pts.begin());
              new_it = std::prev(new_it);
            }
            else
            {
              pts.erase(to_remove);
            }
          }
          it = euclid::geometry::details::circular_iterator<points_vector::iterator>(pts.begin(),
                                                                                     pts.end());
          it = std::next(it, std::max<int>(0, new_it - pts.begin()));
        }
        else
        {
          ++it;
        }
      }
      return typename geometry::linear_ring<_system_>::builder().add_points(pts).create();
    }
    // END linear_ring
    // BEGIN polygon
    geometry::polygon<_system_>
      operator()(const geometry::polygon<_system_>& _polygon,
                 double _distance_tolerance = remove_dead_angle_default_distance_tolerance,
                 double _angle_tolerance = remove_dead_angle_default_angle_tolerance)
    {
      using linear_ring = geometry::linear_ring<_system_>;
      linear_ring ext
        = remove_dead_angle_op(_polygon.exterior_ring(), _distance_tolerance, _angle_tolerance);
      std::vector<linear_ring> ints;
      for(const linear_ring& lr : _polygon.interior_rings())
      {
        ints.push_back(remove_dead_angle_op(lr, _distance_tolerance, _angle_tolerance));
      }
      return geometry::polygon<_system_>(ext, ints);
    }
    // END polygon
    // BEGIN collection
    template<template<typename> class _geometry_t_>
    inline geometry::details::collection<_geometry_t_<_system_>>
      operator()(const geometry::details::collection<_geometry_t_<_system_>>& _collection,
                 double _distance_tolerance = remove_dead_angle_default_distance_tolerance,
                 double _angle_tolerance = remove_dead_angle_default_angle_tolerance)
    {
      typename geometry::details::collection<_geometry_t_<_system_>>::builder builder;
      for(std::size_t i = 0; i < _collection.size(); ++i)
      {
        builder.add_element(
          remove_dead_angle_op(_collection.at(i), _distance_tolerance, _angle_tolerance));
      }
      return builder.create();
    }
    // END collection
    inline typename _system_::geometry
      operator()(const geometry::geometry<_system_>& _geometry,
                 double _distance_tolerance = remove_dead_angle_default_distance_tolerance,
                 double _angle_tolerance = remove_dead_angle_default_angle_tolerance)
    {
      return operation::dispatch<
        remove_dead_angle_op,
        operation::enable_for<euclid::geometry::polygon, euclid::geometry::linear_ring,
                              euclid::geometry::multi_polygon, euclid::geometry::multi_geometry>,
        geometry::geometry<_system_>>(_geometry, _distance_tolerance, _angle_tolerance);
    }
  };

  /**
   * @ingroup euclid_extra
   *
   * This is an algorithm that remove points in a polygon where the angle between two consecutives
   * segments is equal to 0
   */
  template<typename _T_>
  inline _T_ remove_dead_angle(const _T_& _t,
                               double _distance_tolerance
                               = remove_dead_angle_default_distance_tolerance,
                               double _angle_tolerance = remove_dead_angle_default_angle_tolerance)
  {
    return remove_dead_angle_op<typename _T_::system>(_t, _distance_tolerance, _angle_tolerance);
  }
} // namespace euclid::extra::fix_it
