#include "tool.h"

#include <iostream>

int main(int _argc, char** _argv)
{
  if(_argc == 1)
  {
    std::cerr << "Missing tool name among: ";
    for(const std::string& tool : tool::all_tools_names())
    {
      std::cout << tool << " ";
    }
    std::cerr << std::endl;
  }
  std::vector<std::string> arguments;
  for(int i = 2; i < _argc; ++i)
  {
    arguments.push_back(_argv[i]);
  }
  return tool::run(_argv[1], arguments);
}
