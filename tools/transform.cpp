#include "tool.h"

#include <iostream>

#include <euclid/geos>
#include <euclid/io>
#include <euclid/proj>

class transform : public tool
{
public:
  transform() : tool("transform") {}
  int run(const std::vector<std::string>& _arguments) override
  {
    if(_arguments.size() < 3 or _arguments[0] == "-h" or _arguments[0] == "--help")
    {
      std::cout << "Usuage: pralin_tools transform source_cs destination_cs wkt" << std::endl;
      return -1;
    }
    else
    {
      euclid::geos::geometry geom = euclid::io::from_wkt<euclid::geos::geometry>(_arguments[2]);
      if(geom.is_null())
      {
        std::cerr << "Invalid geometry." << std::endl;
        return -1;
      }
      else
      {
        geom = euclid::proj::transform(geom, _arguments[0], _arguments[1], true);
        std::cout << euclid::io::to_wkt(geom) << std::endl;
        return 0;
      }
    }
  }
};

transform t;
