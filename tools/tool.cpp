#include "tool.h"

#include <iostream>
#include <map>

struct tool::data
{
  std::string name;

  static std::map<std::string, tool*> tools;
};

std::map<std::string, tool*> tool::data::tools;

tool::tool(const std::string& _name) : d(new data{_name}) { register_tool(this); }

tool::~tool() {}

int tool::run(const std::string& _name, const std::vector<std::string>& _arguments)
{
  std::map<std::string, tool*>::iterator it = data::tools.find(_name);
  if(it == data::tools.end())
  {
    std::cerr << "No tools named: " << _name << std::endl;
    return -1;
  }
  else
  {
    return it->second->run(_arguments);
  }
}

std::vector<std::string> tool::all_tools_names()
{
  std::vector<std::string> keys;
  for(const auto& [key, value] : data::tools)
  {
    keys.push_back(key);
  }
  return keys;
}

void tool::register_tool(tool* _tool) { data::tools[_tool->d->name] = _tool; }
