#include <string>
#include <vector>

class tool
{
public:
  tool(const std::string& _name);
  ~tool();
  virtual int run(const std::vector<std::string>& _arguments) = 0;
  static int run(const std::string& _name, const std::vector<std::string>& _arguments);
  static std::vector<std::string> all_tools_names();
private:
  static void register_tool(tool* _tool);
  struct data;
  data* const d;
};
